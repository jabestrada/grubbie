//
//  AppEventType.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


enum AppEventType : Int {
    case ExperiencePhotoAdded
    case ExperiencePhotoModified
    case ExperiencePhotoDeleted
    
    case RestaurantAdded
    case RestaurantModified
    case RestaurantDeleted
    
    case RestaurantExperienceAdded
    case RestaurantExperienceModified
    case RestaurantExperienceDeleted
    
    
    case RestaurantPhotoAdded
    case RestaurantPhotoModified
    case RestaurantPhotoDeleted
    
    case ShareSummaryAdded
    
    case ShareNotificationAccepted
    case ShareNotificationRejected
    case ShareNotificationNotFound
}