//
//  AppEventSubscriber.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

protocol AppEventSubscriber {
    func onEvent(eventType: AppEventType, eventTimeStamp: NSDate, userData: AnyObject?)
}
