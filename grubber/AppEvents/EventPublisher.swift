//
//  EventPublisher.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

class EventPublisher : AppEventPublisher {
    static private var _eventPublisher : AppEventPublisher?
    
    static func sharedAppEventPublisher() -> AppEventPublisher {
        if let _ = _eventPublisher {
            // Do nothing.
        }
        else {
            _eventPublisher = EventPublisher()
        }
        
        return _eventPublisher!
    }
    
    
    var events = [AppEventType : [AppEventSubscriber]]()
    
    func addSubscriber(subscriber: AppEventSubscriber, eventTypes: AppEventType...){
        for eventType in eventTypes {
//            print("addSubscriber: \(eventType)")
            addSubscriber(subscriber, eventType: eventType)
        }
    }
    
    private func addSubscriber(subscriber: AppEventSubscriber, eventType: AppEventType) {
        guard let _ = events[eventType] else {
            events[eventType] = [subscriber]
            return
        }
        // TODO: Check that subscriber is not yet in the list before adding it.
//        events[eventType]!.contains(subscriber)
        events[eventType]!.append(subscriber)
    }

    
    func emit(eventType: AppEventType, userData: AnyObject?){
        let eventTimestamp = NSDate()
        guard let _ = events[eventType] else {
            return
        }
        
        for sub in events[eventType]! {
            sub.onEvent(eventType, eventTimeStamp: eventTimestamp, userData: userData)
        }
        
    }
}