//
//  AppEventPublisher.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

protocol AppEventPublisher {
    var events : [AppEventType : [AppEventSubscriber]] { get }
    func addSubscriber(subscriber: AppEventSubscriber, eventTypes: AppEventType...)
    func emit(eventType: AppEventType, userData: AnyObject?)
    
}