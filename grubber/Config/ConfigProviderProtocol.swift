//
//  ConfigProviderProtocol.swift
//  UGetApp
//
//  Created by JABE on 9/28/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public protocol ConfigProviderProtocol {
    var nearbyEatsDistanceFromFilter : Double { get set }
    var nearbyEatsSearchTermFilter : String { get set }
    var restaurantNameFilter : String { get set }
    var photoCaptionFilter : String { get set }
    var townCityFilter : String { get set }
    var countryFilter : String { get set }
}


public extension ConfigProviderProtocol{
    var nearbyEatsDistanceFromFilterKey : String {
        get {
            return "NearbyEatsDistanceFromFilter"
        }
    }
    
    var nearbyEatsSearchTermFilterKey : String {
        get {
            return "NearbyEatsSearchTermFilterKey"
        }
    }
    
    var restaurantNameFilterKey : String {
        get {
            return "restaurantNameFilterKey"
        }
    }
    
    var photoCaptionFilterKey : String {
        get {
            return "photoCaptionFilterKey"
        }
    }
    
    var townCityFilterKey : String {
        get {
            return "townCityFilterKey"
        }
    }
    
    var countryFilterKey : String {
        get {
            return "countryFilterKey"
        }
    }
    
}