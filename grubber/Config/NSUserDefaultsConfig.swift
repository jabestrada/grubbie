//
//  AppConfig.swift
//  UGetApp
//
//  Created by JABE on 9/28/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

class NSUserDefaultsConfig : ConfigProviderProtocol {

    var nearbyEatsDistanceFromFilter : Double {
        get {
            return NSUserDefaults.standardUserDefaults().doubleForKey(nearbyEatsDistanceFromFilterKey)
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: nearbyEatsDistanceFromFilterKey)
        }
        
    }

    var nearbyEatsSearchTermFilter : String {
        get {
            if let searchTerm = NSUserDefaults.standardUserDefaults().stringForKey(nearbyEatsSearchTermFilterKey) {
                return searchTerm
            }
            return ""
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: nearbyEatsSearchTermFilterKey)
        }
    }
    
    var restaurantNameFilter : String {
        get {
            if let term = NSUserDefaults.standardUserDefaults().stringForKey(restaurantNameFilterKey) {
                return term
            }
            return ""
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: restaurantNameFilterKey)
        }
    
    }
    
    var photoCaptionFilter : String {
        get {
            if let term = NSUserDefaults.standardUserDefaults().stringForKey(photoCaptionFilterKey) {
                return term
            }
            return ""
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: photoCaptionFilterKey)
        }
        
    }
   
    var townCityFilter : String {
        get {
            if let term = NSUserDefaults.standardUserDefaults().stringForKey(townCityFilterKey) {
                return term
            }
            return ""
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: townCityFilterKey)
        }
        
    }
    
    var countryFilter : String {
        get {
            if let term = NSUserDefaults.standardUserDefaults().stringForKey(countryFilterKey) {
                return term
            }
            return ""
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: countryFilterKey)
        }
        
    }
    
    
}
