//
//  AppDelegate.swift
//  grubber
//
//  Created by JABE on 11/12/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit
import QuadratTouch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var configProvider = Factory.createConfigProvider()
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Parse.setApplicationId("b6YKfnwqQdxcvgb59rpqTSTOSDSXFs5DQGmdzZ3N", clientKey:"gbkZzx8Hg029gWnKmHQQpXORZ2ww6etM8WOY47BO")
//        PFTwitterUtils.initializeWithConsumerKey(“<Your Twitter Consumer Key>", consumerSecret:”<Your Twitter Consumer Secret>")
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions);
        
        setDefaultConfigValues()
        
        bootStrapFourSquare()
        
        return true
    }
    
    func setDefaultConfigValues() {
        let nearbyEatsDistanceFilter = self.configProvider.nearbyEatsDistanceFromFilter
        if nearbyEatsDistanceFilter == 0 {
            self.configProvider.nearbyEatsDistanceFromFilter = 10
        }
    }
    
    func bootStrapFourSquare() {
        let client = Client(clientID: "DKQQLTBTMSEXKE3YQ22NATU4I0TYM4AXQCGLR2HQY1Y41TYE",
            clientSecret:   "SSZL3EIQLSSGBKKHAIBSJ3UFRPFL4JPPNJ1VAMTW43WW0ZJZ",
            redirectURL:    "grubber://foursquare")
        var configuration = Configuration(client:client)
        configuration.mode = "foursquare"
        configuration.shouldControllNetworkActivityIndicator = true
        Session.setupSharedSessionWithConfiguration(configuration)
    }
    
    func application(application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: () -> Void) {
        print("handleEventsForBackgroundURLSession: \(identifier)")
    }
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }



    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

