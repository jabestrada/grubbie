//
//  Factory.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

//import Parse

class Factory: NSObject {
    class func createDataAdapter() -> DataAdapterProtocol {
        return ParseDataAdapter()
    }
    
    class func convert<T>(object: PFObject) -> T {
        switch object.parseClassName {
        case Restaurant.dataSourceEntityName:
            return Restaurant(pfRestaurantObject: object) as! T
        case RestaurantExperience.dataSourceEntityName:
            return RestaurantExperience(pfRestaurantExperienceObject: object) as! T
        case ExperiencePhoto.dataSourceEntityName:
            return ExperiencePhoto(pfExperiencePhotoObject: object) as! T
        case RestaurantPhoto.dataSourceEntityName:
            return RestaurantPhoto(pfRestaurantPhotoObject: object) as! T
        case AppUser.dataSourceEntityName:
            return AppUser(pfUserObject: object) as! T
        case ShareNotification.dataSourceEntityName:
            return ShareNotification(pfShareNotifObject: object) as! T
        case ShareSummary.dataSourceEntityName:
            return ShareSummary(pfShareSummaryObject: object) as! T
        case AdhocShare.dataSourceEntityName:
            return AdhocShare(pfAdhocShareObject: object) as! T
        case FbPostAudit.dataSourceEntityName:
            return FbPostAudit(pfFbPostAuditObject: object) as! T
        default:
            assertionFailure("Factory.convert: No converter defined for \(object.parseClassName)")
        }
        abort()
    }
    
    static func createError(code: AppErrorCode, description: String, reason: String, recovery: String = "") -> NSError {
        return
            NSError(domain: "Grubbie", code: code.rawValue,
                    userInfo: [NSLocalizedDescriptionKey: description,
                        NSLocalizedFailureReasonErrorKey: reason,
                        NSLocalizedRecoveryOptionsErrorKey: recovery])
    }
    
    static func createNetConnectivityChecker() -> NetConnectivityCheckerStrategy {
        return HttpRequestNetConnectivityChecker()
    }
    
    
    class func createConfigProvider() -> ConfigProviderProtocol {
        return NSUserDefaultsConfig()
    }
    
    class func createDateFormatter() -> NSDateFormatter {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy (E)"
        return dateFormatter
    }

    class func createDateFormatterWithTime() -> NSDateFormatter {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy (E) hh:mm a"
        return dateFormatter
    }
    
    class func createFBAdapter() -> FBAdapter {
//        return FakeFBAdapter()
                return ParseFacebookUtilsV4_FBAdapter()
        //        return ParseFacebookUtils_FBAdapter()
    }
    class func createFBParamsBuilder() -> FBParamsBuilder {
        return ParseFacebookUtilsV4_FBParamsBuilder()
    }
}
