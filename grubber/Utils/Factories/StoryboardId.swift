//
//  StoryboardId.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

enum StoryboardId : String {
    case PostToSocial = "PostToSocial"
    case RestaurantListView = "RestaurantListViewController"
    case AccountSettings = "AccountSettings"
    case PhotoEditor = "NewPhotoViewController"
    case ShareType = "ShareType"
    case GrubbiePhotoSearchView = "GrubbiePhotoSearch"
    case PhotoSearchSettingsView = "PhotoSearchSettingsView"
    case FullPhotoViewer = "FullPhotoViewer"
    case SharingViewController = "SharingViewController"
    case ShareAcceptRejectViewController = "ShareAcceptRejectViewController"
    case About = "AboutViewController"
}