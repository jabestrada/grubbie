//
//  UIFactory.swift
//  grubber
//
//  Created by JABE on 11/14/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

public class UIFactory {
    
    static func configureLoginTextField(textField: UITextField?) {
        textField?.textColor = UIColor.whiteColor()
        textField?.backgroundColor = UIColor(red: 12/255, green: 0/255, blue: 122/255, alpha: 0.5)
    }
    
    static func configureLoginButton(button: UIButton?){
        button?.setBackgroundImage(nil, forState: .Normal)
        button?.backgroundColor = UIColor.clearColor()
        button?.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
    
    static func createWelcomeBackgroundImageView() -> UIImageView {
        let backgroundImage = UIImageView(image: UIImage(named: MediaName.WelcomeBackgroundImage.rawValue))
        backgroundImage.contentMode = UIViewContentMode.ScaleAspectFill
        return backgroundImage

    }
    
    static func createWelcomeLabel() -> UILabel {
        let welcomeLabel = UILabel()
        welcomeLabel.text = "Grubbie"
        UIFactory.configureAppLabel(welcomeLabel)
        return welcomeLabel
    }

    static func configureAppLabel(label: UILabel) {
        label.textColor = UIColor(red: 12/255, green: 0/255, blue: 122/255, alpha: 1)
        label.font = UIFont(name: MediaName.WelcomeFontName.rawValue, size: 70)
        label.shadowColor = UIColor.lightGrayColor()
        label.shadowOffset = CGSizeMake(2, 2)
    }
    


    
    static func createReviewEditorController(parentViewController: UIViewController, restaurant: Restaurant, restaurantExperience: RestaurantExperience?, delegate: AppModelEditorDelegate?) {
        if let reviewEditorVc = parentViewController.storyboard?.instantiateViewControllerWithIdentifier("ReviewEditor") as? WriteReviewViewController {
            reviewEditorVc.restaurant = restaurant
            reviewEditorVc.restaurantExperience = restaurantExperience
            reviewEditorVc.delegate = delegate
            parentViewController.presentViewController(reviewEditorVc, animated: true, completion: nil)
        }
    }
    
    
    static func showStoryboard(parentViewController: UIViewController, storyboardId: StoryboardId, configureBlock: ((uiViewController: UIViewController) -> Void)?) {
        if let viewController = parentViewController.storyboard?.instantiateViewControllerWithIdentifier(storyboardId.rawValue) {
            if let configureBlock = configureBlock {
                configureBlock(uiViewController: viewController)
            }
            parentViewController.presentViewController(viewController, animated: true, completion: nil)
        }
    }
    
    
    static func createRestaurantEditorController(parentViewController: UIViewController, restaurant: Restaurant,  delegate: AppModelEditorDelegate?) {
        if let restaurantEditorVc = parentViewController.storyboard?.instantiateViewControllerWithIdentifier("RestaurantMainInfo") as? RestaurantMainDetailEditorController {
            restaurantEditorVc.restaurant = restaurant
            if let delegate = delegate {
                restaurantEditorVc.delegate = delegate
            }
            parentViewController.presentViewController(restaurantEditorVc, animated: false, completion: nil)
        }
    }

    static func createWelcomeScreenController(parentViewController: UIViewController) {
        if let welcomeVc = parentViewController.storyboard?.instantiateViewControllerWithIdentifier("WelcomeScreen") as? WelcomeViewController {
            parentViewController.presentViewController(welcomeVc, animated: true, completion: nil)
        }
    }
    
    static func createAudioRecordingController(parentViewController: UIViewController, audioData: NSData?, delegate: AudioRecordingDelegate?) {
        if let audioRecordingVc = parentViewController.storyboard?.instantiateViewControllerWithIdentifier("AudioRecordingView") as? AudioRecordingViewController {
            audioRecordingVc.audioData = audioData
            audioRecordingVc.delegate = delegate
            parentViewController.presentViewController(audioRecordingVc, animated: false, completion: nil)
        }
    }
    
    static func createGeoEditorController(parentViewController: UIViewController, location: CLLocation?, delegate: AppModelEditorDelegate?) {
        if let geoEditorVc = parentViewController.storyboard?.instantiateViewControllerWithIdentifier("GeoLocationEditor") as? GeoLocationEditor {
            geoEditorVc.delegate = delegate
            geoEditorVc.location = location
            parentViewController.presentViewController(geoEditorVc, animated: false, completion: nil)
        }
    }
    

    


}