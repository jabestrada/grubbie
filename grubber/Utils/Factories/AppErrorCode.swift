//
//  AppErrorCode.swift
//  Grubbie
//
//  Created by JABE on 12/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public enum AppErrorCode : Int {
    case ObjectNotFound = 1
    case InvalidOperation = 2
    
    func shortDescription() -> String {
        switch self {
        case .ObjectNotFound:
            return "Object not found"
        case .InvalidOperation:
            return "Invalid operation"
//        default:
//            return "Unknown error code \(self)"
        }
    }
}