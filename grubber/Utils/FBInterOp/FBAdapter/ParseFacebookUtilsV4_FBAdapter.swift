//
//  ParseFacebookUtilsV4_FBAdapter.swift
//  grubber
//
//  Created by JABE on 11/12/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

class ParseFacebookUtilsV4_FBAdapter {
    var fbParamsBuilder = Factory.createFBParamsBuilder()
    
    private func doPost(graphPath: String, fbParamsDictionary: [NSObject : AnyObject],
        completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
            let graphRequest = FBSDKGraphRequest(graphPath: graphPath, parameters: fbParamsDictionary, HTTPMethod: "POST")
            graphRequest.startWithCompletionHandler({
                (connection, result, error) -> Void in
                if let callback = completionBlock {
                    callback(result: result, error: error)
                }
            })
    }
    
    private func assertPublishPreReqs() {
        assertFBSDKAccessToken()
        
        guard FBSDKAccessToken.currentAccessToken().hasGranted(FBPermission.publish.rawValue) else {
            assertionFailure("ParseFacebookUtilsV4_FBAdapter.postPhoto(): Application should have been granted \(FBPermission.publish.rawValue) prior to calling this method.")
            return
        }
    }
    
    private func assertFBSDKAccessToken() {
        guard let _ = FBSDKAccessToken.currentAccessToken() else {
            assertionFailure("ParseFacebookUtilsV4_FBAdapter.postPhoto(): FBSDKAccessToken.currentAccessToken() is nil")
            return
        }
    }
}


// MARK: FBAdapter
extension ParseFacebookUtilsV4_FBAdapter : FBAdapter {
    func hasPermission(permission: String) -> Bool {
        assertFBSDKAccessToken()
        
        return FBSDKAccessToken.currentAccessToken().hasGranted(permission)
    }
    
    func hasPublishPermission() -> Bool {
              return FBSDKAccessToken.currentAccessToken().hasGranted(FBPermission.publish.rawValue)
    }
    
    func isUserLoggedThruFacebook() -> Bool {
        if let pfUser = PFUser.currentUser() {
            return PFFacebookUtils.isLinkedWithUser(pfUser)
        }
        else {
            return false
        }
    }
    
    func postPhoto(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        guard let _ = fbParams.photo else {
            assertionFailure("PFFBAdapter.postPhoto expects non-nil value for photoParams.photo")
            return
        }
        
        assertPublishPreReqs()
        
        let paramsDictionary = self.fbParamsBuilder.buildParams(fbParams)
//        paramsDictionary["place"] = "205524929471642"
//        doPost("me/photos", fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
        doPost(FbGraphPath.myPhotos.rawValue, fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
    }
    
    func postStatus(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        
        assertPublishPreReqs()
        
        let paramsDictionary = self.fbParamsBuilder.buildParams(fbParams)
//        paramsDictionary["place"] = "205524929471642"
        
//        doPost("me/feed", fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
           doPost(FbGraphPath.myFeed.rawValue, fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
    }
    
    func getProfilePicUrl(completion: (picUrl: String, error: NSError?) -> Void){
        var profPicParams = [NSObject : AnyObject]()
        profPicParams["type"] = "large"
        profPicParams["redirect"] = "false"
        let profPicGraphRequest = FBSDKGraphRequest(graphPath: FbGraphPath.myPicture.rawValue, parameters: profPicParams, HTTPMethod: "GET")
        profPicGraphRequest.startWithCompletionHandler { (connection, result, error) -> Void in
            if error == nil {
                if let picUrl = (result["data"] as? NSDictionary)?["url"] as? String {
                    completion(picUrl: picUrl, error: nil)
                }
                else {
                    completion(picUrl: "", error: nil)
                }
            }
            else {
                completion(picUrl: "", error: error)
            }
        }
    }

    func getFriendsUsingApp(completionBlock: (fbUsers: [FBUserData], error: NSError?) -> Void) {
        var paramsDictionary = [NSObject : AnyObject]()
        paramsDictionary["fields"] = "id,first_name,last_name"
        let graphRequest = FBSDKGraphRequest(graphPath: "me/friends?limit=99", parameters: paramsDictionary, HTTPMethod: "GET")
        graphRequest.startWithCompletionHandler({
            (connection, result, error) -> Void in
            var appUsers = [FBUserData]()
            if error == nil {
                if let results = result as? NSDictionary {
                    if let users = results["data"] as? NSArray {
                        for user in users {
                            if let fbUser = user as? NSDictionary {
                                
                                let fbUser = FBUserData(fbId: fbUser["id"] as! String, firstName: fbUser["first_name"] as! String, lastName: fbUser["last_name"] as! String, profilePicUrl: "")
                                appUsers.append(fbUser)
                            }
                        }
                    }
                }
            } else {
//                Util.showMessage(self, msg: "There seemed to be a problem reaching out to Facebook. Please try again later.", title: UserDialogTitle.Error.rawValue)
                completionBlock(fbUsers: [FBUserData](), error: nil)
                print("share Error: \(error)")
            }
            completionBlock(fbUsers: appUsers, error: error)
        })
    }
    
    func getFbUserData(includeProfilePic: Bool, completion: (userData: FBUserData?, error: NSError?) -> Void){
         var paramsDictionary = [NSObject : AnyObject]()
        paramsDictionary["fields"] = "\(FbFields.firstName.rawValue),\(FbFields.lastName.rawValue),\(FbFields.picture.rawValue)"
        let graphRequest = FBSDKGraphRequest(graphPath: FbGraphPath.me.rawValue, parameters: paramsDictionary, HTTPMethod: "GET")
        graphRequest.startWithCompletionHandler({
            (connection, result, error) -> Void in
            if error == nil {
                if let results = result as? NSDictionary {
                    let firstName = results[FbFields.firstName.rawValue] as! String
                    let lastName = results[FbFields.lastName.rawValue] as! String
                    let fbId = results[FbFields.id.rawValue] as! String
                    if includeProfilePic {
                        self.getProfilePicUrl({ (picUrl, error) -> Void in
                            if error == nil{
                                completion(userData: FBUserData(fbId: fbId, firstName: firstName, lastName: lastName, profilePicUrl: picUrl), error: nil)
                            } else {
                               completion(userData: FBUserData(fbId: fbId, firstName: firstName, lastName: lastName, profilePicUrl: ""), error: error)
                            }
                        })
                    } else {
                        completion(userData: FBUserData(fbId: fbId, firstName: firstName, lastName: lastName, profilePicUrl: ""), error: nil)
                    }
                }
                else {
                    completion(userData: nil, error: nil)
                }
            }
            else {
                completion(userData: nil, error: error)
            }
        })
    
    }
    
   
    func queryFbFriendsPermission(fromViewController : UIViewController, completionBlock: ((hasPublishPermissions: Bool, error: NSError?) -> Void)? )  {
        let fbLogin = FBSDKLoginManager()
//        fbLogin.logInWithReadPermissions(["user_friends"], fromViewController: fromViewController, handler: { (result, error) -> Void in
        fbLogin.logInWithReadPermissions([FBPermission.friends.rawValue], fromViewController: fromViewController, handler: { (result, error) -> Void in
                print("result \(result)")
                print("error: \(error)")
                if let completion = completionBlock {
                    completion(hasPublishPermissions: true, error: error)
                }
            })
    }
    
    func queryFbPublishPermission(fromViewController : UIViewController, completionBlock: ((hasPublishPermissions: Bool, error: NSError?) -> Void)? )  {
        let publishPermission = FBPermission.publish.rawValue
        if !self.hasPermission(publishPermission) {
            let fbLogin = FBSDKLoginManager()
            fbLogin.logInWithPublishPermissions([publishPermission], fromViewController: fromViewController, handler: { (result, error) -> Void in
                if let declinedPermissions = result.declinedPermissions {
                    if declinedPermissions.contains(publishPermission) {
                        Util.showMessage(fromViewController, msg: UserMessage.InsufficientAppPermissionToPostToFb.rawValue, title: UserDialogTitle.Info.rawValue)
                        if let completionBlock = completionBlock {
                            completionBlock(hasPublishPermissions: false, error: error)
                        }
                    }
                    else {
                        if let completionBlock = completionBlock {
                            completionBlock(hasPublishPermissions: true, error: NSError?.None)
                        }
                    }
                }
                else {
                    if let completionBlock = completionBlock {
                        completionBlock(hasPublishPermissions: true, error: NSError?.None)
                    }
                    
                }
            })
        }
        else {
            if let completionBlock = completionBlock {
                completionBlock(hasPublishPermissions: true, error: NSError?.None)
            }
            
        }
        
    }
    
    
}
