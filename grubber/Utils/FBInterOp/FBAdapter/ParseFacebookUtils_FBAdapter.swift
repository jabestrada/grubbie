//
//  ParseFacebookUtilsFBAdapter
//  grubber
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

// Source: http://m-farhan.com/2014/03/quick-facebook-post-ios-images-check-in-url/

public class ParseFacebookUtils_FBAdapter : FBAdapter {
    

    public func hasPermission(permission: String) -> Bool {
        assertionFailure("ParseFacebookUtilsV4_FBAdapter.hasPermission() is not implemented.")
        return false
    }
    
    public func getFriendsUsingApp(completionBlock: (fbUsers: [FBUserData], error: NSError?) -> Void) {
        assertionFailure("ParseFacebookUtils_FBAdapter.getFriendsUsingApp() is not implemented.")
    }
    

    public func getProfilePicUrl(completion: (picUrl: String, error: NSError?) -> Void) {
        assertionFailure("ParseFacebookUtilsV4_FBAdapter.getProfilePicUrl() is not implemented.")
    }
    public func getFbUserData(includeProfilePic: Bool, completion: (userData: FBUserData?, error: NSError?) -> Void) {
        assertionFailure("ParseFacebookUtilsV4_FBAdapter.getFbUserData() is not implemented.")
    }
    
    public func postPhoto(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        guard let photo = fbParams.photo else {
            assertionFailure("PFFBAdapter.postPhoto expects non-nil value for photoParams.photo")
            return
        }
        

        var paramsDictionary = [NSObject : AnyObject]()
        paramsDictionary["source"] = photo
        paramsDictionary["message"] = fbParams.message
        doPost("me/photos", fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
    }
    
    public func postStatus(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        if fbParams.message.isEmpty {
            assertionFailure("PFFBAdapter.postStatus expects non-nil value for photoParams.message")
            return
        }

        var paramsDictionary = [NSObject : AnyObject]()
        paramsDictionary["message"] = fbParams.message
        doPost("me/feed", fbParamsDictionary: paramsDictionary, completionBlock: completionBlock)
    }
    
    public func queryFbPublishPermission(fromViewController: UIViewController, completionBlock: ((hasPublishPermissions: Bool, error: NSError?) -> Void)?) {
        assertionFailure("ParseFacebookUtilsV4_FBAdapter.queryFbPublishPermission() is not implemented.")

    }
    private func doPost(graphPath: String, fbParamsDictionary: [NSObject : AnyObject], completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        // JABE, Nov. 11, 2015: FBSession class belongs to ParseFacebookUtils pod which is apparently no longer available
        // in ParseFacebookUtilsV4.
//        FBSession.openActiveSessionWithReadPermissions(["publish_actions"],
//            allowLoginUI: true) { (session, sessionState, error) -> Void in
//                let request = FBRequest(session: FBSession.activeSession(), graphPath: graphPath,
//                    parameters: fbParamsDictionary, HTTPMethod: "POST")
//                let newConnection = FBRequestConnection()
//                newConnection.addRequest(request, completionHandler: { (connection, result, error) -> Void in
//                    if let completionBlock = completionBlock {
//                        completionBlock(result: result, error: error)
//                    }
//                })
//                newConnection.start()
//        }

    }
    
    public func isUserLoggedThruFacebook() -> Bool {
       assertionFailure("ParseFacebookUtilsV4_FBAdapter.isUserLoggedThruFacebook() is not implemented.")
        return false
    }
}
