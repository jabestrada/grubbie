//
//  FBAdapter.swift
//  myEats
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public protocol FBAdapter {
    func postPhoto(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?)
    func postStatus(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?)
    func hasPermission(permission : String) -> Bool
    func queryFbPublishPermission(fromViewController : UIViewController, completionBlock: ((hasPublishPermissions: Bool, error: NSError?) -> Void)? )
    func isUserLoggedThruFacebook() -> Bool
    func getFbUserData(includeProfilePic: Bool, completion: (userData: FBUserData?, error: NSError?) -> Void)
    func getProfilePicUrl(completion: (picUrl: String, error: NSError?) -> Void)
    func getFriendsUsingApp(completionBlock: (fbUsers: [FBUserData], error: NSError?) -> Void) 
}

public enum FBPermission : String {
    case publish = "publish_actions"
    case friends = "user_friends"
}