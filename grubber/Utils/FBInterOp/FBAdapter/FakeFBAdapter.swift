//
//  FakeFBAdapter.swift
//  grubber
//
//  Created by JABE on 11/16/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class FakeFBAdapter : FBAdapter {
    public func hasPermission(permission: String) -> Bool {
        return true
    }
    
    public func queryFbPublishPermission(fromViewController: UIViewController, completionBlock: ((hasPublishPermissions: Bool, error: NSError?) -> Void)?) {
        if let completionBlock = completionBlock {
            completionBlock(hasPublishPermissions: false, error: nil)
        }
    }
    
    public func getFriendsUsingApp(completionBlock: (fbUsers: [FBUserData], error: NSError?) -> Void) {
        assertionFailure("FakeFBAdapter.getFriendsUsingApp() is not implemented.")
    }
    
    public func getProfilePicUrl(completion: (picUrl: String, error: NSError?) -> Void) {
        assertionFailure("FakeFBAdapter.getProfilePicUrl() is not implemented.")
    }
    
    public func getFbUserData(includeProfilePic: Bool, completion: (userData: FBUserData?, error: NSError?) -> Void) {
        assertionFailure("FakeFBAdapter.getFbUserData() is not implemented.")
    }
    
    
    public func postPhoto(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        print("Start FakeFBAdapter.postPhoto")
        ThreadingUtil.inBackgroundAfter({ () -> Void in
            print("End FakeFBAdapter.postPhoto")
            if let completionBlock = completionBlock {
                completionBlock(result: nil, error: nil)
            }
            }, afterDelay: 3)
    }
    
    public func postStatus(fbParams: FBParams, completionBlock: ((result: AnyObject?, error: NSError?) -> Void)?) {
        print("Start FakeFBAdapter.postStatus")
        ThreadingUtil.inBackgroundAfter({ () -> Void in
            print("End FakeFBAdapter.postStatus")
            if let completionBlock = completionBlock {
                completionBlock(result: nil, error: nil)
            }
            }, afterDelay: 3)
        
    }
    
    public func isUserLoggedThruFacebook() -> Bool {
        return true
    }
}
