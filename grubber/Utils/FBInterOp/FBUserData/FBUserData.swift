//
//  FBUserData.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

public struct FBUserData  {
    var fbId : String
    var fullName : String {
        get {
            return "\(firstName) \(lastName)"
        }
        
    }
    
    var firstName: String
    var lastName: String
    var profilePicUrl : String
}


public enum FbFields : String {
    case id = "id"
    case firstName = "first_name"
    case lastName = "last_name"
    case picture = "picture"
}

public enum FbGraphPath : String {
    case me = "me"
    case myFeed = "me/feed"
    case myPicture = "me/picture"
    case myPhotos = "me/photos"
}