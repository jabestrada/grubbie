//
//  FBParams.swift
//  grubber
//
//  Created by JABE on 11/18/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class FBParams {
    var photo: NSData?
    var message: String
    var privacy: PostPrivacy
    
    init(message: String, photo: NSData?, privacy: PostPrivacy?){
        if let photo = photo {
            self.photo = photo
        }
        self.message = message
        if let privacy = privacy {
            self.privacy = privacy
        } else {
            self.privacy = PostPrivacy.OnlyMe
        }
    }
    
    convenience init(message: String, photo: NSData?){
        self.init(message: message, photo: photo, privacy: PostPrivacy?.None)
    }
    
}


//SELF | ALL_FRIENDS | FRIENDS_OF_FRIENDS | EVERYONE | CUSTOM
public enum PostPrivacy : String {
    case OnlyMe = "SELF"
    case Friends = "ALL_FRIENDS"
    case Everyone = "EVERYONE"
    // vNext for Custom and Friends Of Friends (?)
//        case FriendsOfFriends = "FRIENDS_OF_FRIENDS"
    //    case Custom = "CUSTOM"
}
