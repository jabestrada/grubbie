//
//  FBParamsBuilder.swift
//  grubber
//
//  Created by JABE on 11/18/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


public protocol FBParamsBuilder {
    func buildParams(fbParams: FBParams) -> [NSObject : AnyObject]
}
