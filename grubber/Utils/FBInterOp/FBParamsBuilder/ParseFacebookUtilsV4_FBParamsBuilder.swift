//
//  ParseFacebookUtilsV4_FBParamsBuilder.swift
//  grubber
//
//  Created by JABE on 11/18/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class ParseFacebookUtilsV4_FBParamsBuilder : FBParamsBuilder {
    public func buildParams(fbParams: FBParams) -> [NSObject : AnyObject] {
        var paramsDictionary = [NSObject : AnyObject]()
        if fbParams.photo != nil {
            paramsDictionary["source"] = fbParams.photo
        }
        paramsDictionary["message"] = fbParams.message
        paramsDictionary["privacy"] = "{'value':'\(fbParams.privacy.rawValue)'}"

        // TODO: Build privacy
        // SELF | ALL_FRIENDS | FRIENDS_OF_FRIENDS | EVERYONE | CUSTOM
        //                paramsDictionary["privacy"] = "{'value':'SELF'}"
        return paramsDictionary
    }
}
