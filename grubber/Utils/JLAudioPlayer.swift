//
//  JLAudioPlayer.swift
//  grubber
//
//  Created by JABE on 11/22/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import AVFoundation

class JLAudioPlayer : NSObject, AVAudioPlayerDelegate {
    
    var audioPlayer : AVAudioPlayer?
    var completionBlock : ((player: AVAudioPlayer, successfully: Bool) -> Void)?
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully: Bool) {
        
        if let completionBlock = self.completionBlock {
            completionBlock(player: self.audioPlayer!, successfully: successfully)
        }
    }
    
    
    func play(audioData : NSData, completionBlock: ((player: AVAudioPlayer, successfully: Bool) -> Void)?) {
        self.completionBlock = completionBlock

        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker)
        } catch let e as NSError {
            print("Error AVAudioSession.sharedInstance().overrideOutputAudioPort: \(e.localizedDescription)")
        }
        
        do {
            try audioPlayer = AVAudioPlayer(data: audioData)
            audioPlayer?.delegate = self
            audioPlayer?.play()
            print("Play started...")
        }
        catch let e as NSError {
            print("Error on audioPlayer.play: \(e.localizedDescription)")
        }
    }
    
    func stop(){
        self.audioPlayer?.stop()
    }
    
    
}
