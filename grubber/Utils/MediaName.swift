//
//  MediaName.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum MediaName : String {
    case PhotoEditorStopButtonIcon = "Stop Filled-32.png"
    case PhotoEditorPlayButtonIcon = "High Volume Filled-32.png"
    case StopRecordingIcon = "Stop Filled-100.png"
    case StartRecordingIcon = "Microphone Filled-100.png"
    case PlayRecordingIcon = "Play Filled-100.png"
    case WelcomeBackgroundImage = "Welcome_bg600by400_80"
    case WelcomeFontName = "GillSans-Bold"
    case RecordingTempFileName = "grubbie.caf"
    case TableViewCellCheckedImage = "Checked Checkbox 2-32"
    case TableViewCellUncheckedImage = "Unchecked Checkbox-32"
    case NoAudio = "No Audio-32.png"
}