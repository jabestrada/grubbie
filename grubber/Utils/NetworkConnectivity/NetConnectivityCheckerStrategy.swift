
//
//  NetConnectivityCheckerStrategy.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


import Foundation


protocol NetConnectivityCheckerStrategy {
    func isNetworkAvailable() -> Bool
}

