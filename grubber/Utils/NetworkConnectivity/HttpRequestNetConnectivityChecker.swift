//
//  HttpRequestNetConnectivityChecker.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class HttpRequestNetConnectivityChecker : NetConnectivityCheckerStrategy {
    func isNetworkAvailable() -> Bool {
        let url = NSURL(string: "https://google.com/")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "HEAD"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        var response: NSURLResponse?
        do {
            let _ = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response) as NSData?
            if let httpResponse = response as? NSHTTPURLResponse {
                return httpResponse.statusCode == 200
            }
            return false
        }
        catch {
            return false
        }
    }
}
