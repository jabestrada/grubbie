//
//  Util.swift
//  UGetApp
//
//  Created by JABE on 9/25/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit


public class Util {
    
        
    
    class func closeController(controller: UIViewController){
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

    class func showMessage(parentViewController: UIViewController, msg: String, title: String, completionBlock: ((UIAlertAction) -> Void)?){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: completionBlock))
        parentViewController.presentViewController(alert, animated: true, completion: nil)

    }

    
    static func getConfirmation(parentVc: UIViewController, message: String, title: String, yesCaption: String, noCaption: String, onYesDelegate: (UIAlertAction) -> Void){
        
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .ActionSheet)
        let cancelAction = UIAlertAction(title: noCaption, style: .Cancel, handler: nil)
        actionSheetController.addAction(cancelAction)
        
        let yesAction = UIAlertAction(title: yesCaption, style: UIAlertActionStyle.Destructive) { action -> Void in
            onYesDelegate(action)
        }
        actionSheetController.addAction(yesAction)
        parentVc.presentViewController(actionSheetController, animated: true, completion: nil)
        
    }
    
    class func showMessage(parentViewController: UIViewController, msg: String, title: String){
        Util.showMessage(parentViewController, msg: msg, title: title, completionBlock: nil)
    }
    
    static func canonicalize(input: String) -> String {
        return input.uppercaseString
    }
    static func dial(parentViewController: UIViewController, rawNumber: String) {
        let cleanNumber = rawNumber.stringByReplacingOccurrencesOfString(" ", withString: "")
        let number = "tel://\(cleanNumber)"
        if let url = NSURL(string: number) {
            UIApplication.sharedApplication().openURL(url)
            Util.showMessage(parentViewController, msg: "Number \(rawNumber) was sent to your phone's dialer. If no call was initiated, the number may not be reachable through this device.", title: UserDialogTitle.Info.rawValue)
            
        } else {
            Util.showMessage(parentViewController, msg: "Phone number \(number) cannot be reached.", title: UserDialogTitle.Info.rawValue)
        }
    }
    
    class func getCurrentUserId() -> String {
        if let userId = PFUser.currentUser()?.objectId {
            return userId
        }
        return "fallbackUserId"
    }
    
    class func setNoResultsViewMessage(view: UIView, message: String){
        var hasLabel = false
        for subView in view.subviews {
            if let label = subView as? UILabel {
                hasLabel = true
                label.text = message
            }
        }
        
        if !hasLabel {            
            let label = UILabel(frame: view.frame)
            label.numberOfLines = 0
            label.font = UIFont(name: "HelveticaNeue", size: 20.0)
            label.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
            label.textAlignment = .Center
            label.text = message
            view.addSubview(label)
        }
    }
    
    
    class func roundTo(value: Double, decimalPlaces: Int) -> Double {
        let decimalValue = pow(10.0, Double(decimalPlaces))
        return round(value * decimalValue) / decimalValue
    }
    

    
}
