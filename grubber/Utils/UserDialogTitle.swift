//
//  UserDialogTitle.swift
//  grubber
//
//  Created by JABE on 11/16/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


public enum UserDialogTitle : String {
    case Success = "Success"
    case Error = "Oops!"
    case Info = "Info"
}