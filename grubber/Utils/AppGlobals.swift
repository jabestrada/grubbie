//
//  AppGlobals.swift
//  myEats
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class AppGlobals {
    private static var sharedLocation : CLLocation?
    private static var sharedPlacemarks: [CLPlacemark]?
    private static var sharedLocator = JLLocator()
    
    static var appHomeTabViewController : AppHomeTabViewController!
    
    static func sharedLocation(completionBlock: (location: CLLocation) -> Void){
        if let _ = sharedLocation {
            print("Using cached sharedLocation")
            completionBlock(location: sharedLocation!)
        }
        else {
            print("Fetching new sharedLocation")
            sharedLocator.getCurrentLocation { (location) -> Void in
                sharedLocation = location
                completionBlock(location: location)
            }
        }
    }
    
    static func sharedPlacemark(completionBlock: (placemarks: [CLPlacemark]?, error: NSError?) -> Void){
        if let _ = sharedPlacemarks {
            print("Using cached sharedPlacemarks")
            completionBlock(placemarks: sharedPlacemarks, error: nil)
        }
        else {
            print("Fetching new sharedPlacemarks")
            sharedLocator.getCurrentPlacemarks({ (placemarks, error) -> Void in
                sharedPlacemarks = placemarks
                completionBlock(placemarks: placemarks, error: error)
            })
        }
    }
   

}