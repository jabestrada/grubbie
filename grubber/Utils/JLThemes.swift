//
//  JLThemes.swift
//  grubber
//
//  Created by JABE on 11/17/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


public enum WidgetTagCode : Int {
    case restaurantLabel = 10000
    case textView = 10001
    case textViewReadOnly = 10002
    case backgroundView = 10003
    case containerView = 10004
    case imageView = 10005
}


public class JLThemes {
    static var roundCornerRadius = CGFloat(15.0)
    
    static func apply(widgets: UIView...){
        for widget in widgets {
            if let tagCode = WidgetTagCode(rawValue: widget.tag) {
                switch tagCode{
                case .restaurantLabel:
                    formatRestaurantLabel(widget as? UILabel)
                    break;
                case .textView:
                    formatTextView(widget as? UITextView)
                    break;
                case .textViewReadOnly:
                    formatReadOnlyTextView(widget as? UITextView)
                    break;
                case .backgroundView:
                    formatBackgroundView(widget)
                    break;
                case .containerView:
                    formatContainerView(widget)
                    break;
                case .imageView:
                    formatImageView(widget as? UIImageView)
                    break;
                }
            } else {
                assertionFailure("Widget Tag \(widget.tag) was passed but does not have a handler.")
            }
        }
    }

    static func formatBackgroundView(view: UIView?){
        guard let _ = view else {
            assertionFailure("Widget's tag code was \(WidgetTagCode.backgroundView) but it isn't a UIView")
            return
        }
        
//        view.backgroundColor = UIColor.lightGrayColor()
    }
    
    
    static func formatRestaurantLabel(label: UILabel?){
        guard let label = label else {
            assertionFailure("Widget's tag code was \(WidgetTagCode.restaurantLabel) but it isn't a UILabel")
            return
        }
        
        label.textAlignment = NSTextAlignment.Center
        label.numberOfLines = 0
        label.backgroundColor = UIColor.blackColor()
        label.textColor = UIColor.cyanColor()
        label.font = UIFont(name: "GillSans", size: 19.0)

    }
    
    static func formatContainerView(view: UIView){
        view.layer.cornerRadius = roundCornerRadius
    }

    static func formatImageView(imageView: UIImageView?){
        guard let imageView = imageView else {
            assertionFailure("Widget's tag code was \(WidgetTagCode.imageView) but it isn't a UIImageView")
            return
        }
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = roundCornerRadius
    }
    
    static func formatTextView(textView: UITextView?){
        guard let textView = textView else {
            assertionFailure("Widget's tag code was \(WidgetTagCode.textView) but it isn't a UITextView")
            return
        }
        
        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = roundCornerRadius
    }

    
    static func formatReadOnlyTextView(textView: UITextView?){
        guard let textView = textView else {
            assertionFailure("Widget's tag code was \(WidgetTagCode.textViewReadOnly) but it isn't a UITextView")
            return
        }
        formatTextView(textView)
        
        textView.layer.borderWidth = 0
        textView.textColor = UIColor.whiteColor()
        textView.backgroundColor = UIColor.lightGrayColor()
    }
}
