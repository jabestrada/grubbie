//
//  UIViewControllerl+HideKeyboardOnTapExtension.swift
//  grubber
//
//  Created by JABE on 11/17/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension UIViewController {
    func hideKeyboardOnTap(){
        self.view.userInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard:"))
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    func hideKeyboard(sender: AnyObject){
        self.view.endEditing(true)
    }
}