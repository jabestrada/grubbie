//
//  JLActivityIndicator.swift
//  grubber
//
//  Created by JABE on 11/14/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

public class JLActivityIndicator {
    var parentView : UIView
    var onTopOfView: UIView
    var activityIndicator : UIActivityIndicatorView
    var activityOverlay : UIView?
    
    init(parentView: UIView, onTopOfView: UIView, startAnimating: Bool = true, drawOverlayOverParentView: Bool = true) {
        self.parentView = parentView
        self.onTopOfView = onTopOfView
        
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = parentView.center
        
        activityIndicator.transform = CGAffineTransformMakeScale(1.5, 1.5)
        parentView.insertSubview(activityIndicator, aboveSubview: onTopOfView)

        if startAnimating {
            activityIndicator.startAnimating()
        }
        if drawOverlayOverParentView {
            let activityOverlay = UIView(frame: parentView.frame)
            activityOverlay.backgroundColor = UIColor.lightGrayColor()
            activityOverlay.alpha = 0.3
            self.activityOverlay = activityOverlay
            parentView.addSubview(activityOverlay)
            if !startAnimating {
                activityOverlay.hidden = true
            }
        }
    }
    
    public func startAnimation() {
        self.activityIndicator.startAnimating()
        if let activityOverlay = self.activityOverlay {
            activityOverlay.hidden = false
        }
    }
    
    public func stopAnimation() {
        self.activityIndicator.stopAnimating()
        if let activityOverlay = self.activityOverlay {
            activityOverlay.hidden = true
        }
    }
    
}
