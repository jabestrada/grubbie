//
//  JLGeoData.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


class JLGeoData: NSObject {
    
    var country : String?
    var street : String?
    var townCity: String?
    var location: CLLocation?

    
    init(placemarks: [CLPlacemark]?) {
        if let placemarks = placemarks {
            if placemarks.count > 0 {
                let firstPlacemark = placemarks[0]
                self.location = firstPlacemark.location
                if let addressDictionary = firstPlacemark.addressDictionary {
                    self.country = addressDictionary["Country"] as? String
//                    self.street =  addressDictionary["Thoroughfare"] as? String
//                    self.townCity =  addressDictionary["Locality"] as? String
                    self.street =  addressDictionary["Street"] as? String
                    self.townCity =  addressDictionary["City"] as? String
                }
            }
        }
    }
}
