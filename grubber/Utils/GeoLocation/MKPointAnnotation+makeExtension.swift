//
//  MKPointAnnotation+makeExtension.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import MapKit

extension MKPointAnnotation {
    static func make(coordinate2D : CLLocationCoordinate2D, title: String, subTitle: String?) -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate2D
        annotation.title = title
        if let subTitle = subTitle {
            annotation.subtitle = subTitle
        }
        return annotation
    }
}
