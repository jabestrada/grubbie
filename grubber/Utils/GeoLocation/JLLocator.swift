//
//  JLLocator.swift
//  myEats
//
//  Created by JABE on 11/7/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class JLLocator : NSObject {
    var locationManager = CLLocationManager()
    var location : CLLocation?
    var completionCallback : ((location: CLLocation) -> Void)?
    
    override init() {
        super.init()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.delegate = self
    }
    
    func getCurrentLocation(completionBlock: (location: CLLocation) -> Void){
        self.completionCallback = completionBlock
     
        let status = CLLocationManager.authorizationStatus()
        if status == .NotDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        } else if status == CLAuthorizationStatus.AuthorizedWhenInUse || status == CLAuthorizationStatus.AuthorizedAlways {
                self.locationManager.startUpdatingLocation()
        } else {
            // TODO: Throw error here...
            print("showNoPermissionsAlert().")
        }

    }
    
    func getGeoDataFromLocation(location: CLLocation, completionBlock: (geoData: JLGeoData?, error: NSError?) -> Void) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if error == nil {
                completionBlock(geoData: JLGeoData(placemarks: placemarks), error: error)
            }
            else {
                print("getGeoDataFromLocation error: \(error?.localizedDescription). Reason: \(error?.localizedFailureReason)")
                completionBlock(geoData: JLGeoData?.None, error: error)
            }
        })
    }
    
    
    func getCurrentPlacemarks(completionBlock: (placemarks: [CLPlacemark]?, error: NSError?) -> Void) {
        if let _ = self.location {
            getPlaceMarks(completionBlock)
        }
        else {
            self.getCurrentLocation({ (location) -> Void in
                self.getCurrentPlacemarks({ (placemarks, error) -> Void in
                    self.getPlaceMarks(completionBlock)
                })
            })
        }
    }
    
    private func getPlaceMarks(completionBlock: (placemarks: [CLPlacemark]?, error: NSError?) -> Void) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(self.location!, completionHandler: {(placemarks, error) in
            if error != nil && placemarks == nil {
                print("JLLocator.getPlacemarks error: \(error?.localizedDescription) Reason: \(error?.localizedFailureReason)")
            }
            completionBlock(placemarks: placemarks, error: error)
        })

    }
}

// MARK: CLLocationManagerDelegate
extension JLLocator: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .Denied || status == .Restricted {
            print("showNoPermissionsAlert()")
        } else {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        // Process error.
        // kCLErrorDomain. Not localized.
        print("Error: \(error)")
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        guard let location = self.locationManager.location else {
            return
        }
        
        if self.location == newLocation {
            return
        }

        if let completionCallback = self.completionCallback {
            completionCallback(location: location)
        }
        self.location = location
        locationManager.stopUpdatingLocation()
    }
}