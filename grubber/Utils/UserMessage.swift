//
//  UserMessage.swift
//  grubber
//
//  Created by JABE on 11/14/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum UserMessage : String {
    case AppName = "Grubbie"
    case NoNearbyRestaurantSearchResult = "There are no restaurants\nthat match the given criteria."
    case NoRestaurantAtAll = "No restaurants\non your Grubs list.\n\nYou need to write reviews\nto build your Grubs list."
    case NoReviewForThisRestaurant = "You have no reviews yet\nfor this restaurant."
    case NoReviewPhoto = "There are no photos\nfor this review."
    case ErrorPostingToFacebook = "There was an error posting to Facebook. Please re-try later."
    case ReviewSucessfullyPostedToFacebook = "Review was successfully posted to Facebook."
    case ReviewPhotoSucessfullyPostedToFacebook = "Review photo was successfully posted to Facebook."
    case ErrorInsufficientPublishToFbPermission = "The application was not granted sufficient permissions to publish to Facebook."
    case GenericSuccessfullyPostedToFb = "Successfully posted to Facebook"
    case InsufficientAppPermissionToPostToFb = "Grubber cannot post to Facebook because you declined the required permissions."
    case NoGalleryPhotosForThisRestaurant = "There are no gallery photos\nfor this restaurant.\n\nTap on + button\nto add photos to the gallery.\n\nOr open reviews\nto access review photos."
    case StartRecordingInstruction = "Tap on microphone icon\nto start recording"
    case StopRecordingInstruction = "Recording in progress.\nTap Stop icon to end."
    case PlayingRecording = "Playing recording..."
    case RestaurantNameRequired = "Please enter restaurant name."
    case YourCurrentLocation = "You are here"
    case ManualRestaurantEntry = "I'll enter restaurant details myself"
    case SelectNearbyRestaurants = "Select from nearby restaurants"
    case SelectFromGrubsList = "Select from My Grubs list"
    case NoInternet = "Grubbie requires Internet connectivity"
    case GrubbieShare = "Grubbie share"
    case GrubbieShareDetailText = "Share one or more of your past reviews logged via Grubbie. Recipients can save your share(s) to their Grubs list."
    case AdhocShare = "Ad hoc share"
    case AdhocShareDetailText = "Share any photo from your device. However, you can share only 1 photo at a time and recipients cannot save your ad hoc share to their Grubs list."
    case NoPlacemarksError = "There seems to be a problem processing location data. Please try again later."
}


