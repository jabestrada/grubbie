//
//  RestaurantExperience+ParseInit.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



public extension RestaurantExperience{
    convenience init(pfRestaurantExperienceObject: PFObject){
        self.init()
        id = pfRestaurantExperienceObject.objectId!
        restaurantId = pfRestaurantExperienceObject[RestaurantExperienceFields.restaurantId.rawValue] as! String
        experienceDate = pfRestaurantExperienceObject[RestaurantExperienceFields.experienceDate.rawValue] as! NSDate
        rating = pfRestaurantExperienceObject[RestaurantExperienceFields.rating.rawValue] as! Int
        comments = pfRestaurantExperienceObject[RestaurantExperienceFields.comments.rawValue] as! String
    }
}
