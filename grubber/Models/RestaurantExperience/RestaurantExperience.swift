//
//  RestaurantExperience.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class RestaurantExperience: NSObject {
    var id: String = ""
    var restaurantId : String = ""
    var experienceDate = NSDate()
    var rating : Int = 0
    var comments : String = "" {
        didSet{
            commentsCanonical = Util.canonicalize(comments)
        }
    }
    var commentsCanonical = ""
    
    
    func isNew() -> Bool {
        return id == ""
    }
    
    static func getById(experienceId: String) -> RestaurantExperience? {
        return firstOrDefault(Predicate(lhs: RestaurantExperienceFields.id.rawValue, op: .Equals, rhs: experienceId), sortDescriptors: nil)
    }
    
    class func insert(newValuesAssignmentBlock: (RestaurantExperience) -> Void) -> RestaurantExperience {
        let insertObjectValuesAssignmentBlock = {(pfRestaurantExperience: PFObject) -> Void
            in
            let restaurantExperience = RestaurantExperience()
            newValuesAssignmentBlock(restaurantExperience)
            
            pfRestaurantExperience.setValue(NSUUID().UUIDString, forKey: RestaurantExperienceFields.id.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.restaurantId, forKey: RestaurantExperienceFields.restaurantId.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.experienceDate, forKey: RestaurantExperienceFields.experienceDate.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.rating, forKey: RestaurantExperienceFields.rating.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.comments, forKey: RestaurantExperienceFields.comments.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.commentsCanonical, forKey: RestaurantExperienceFields.commentsCanonical.rawValue)
        }
        
        let pfNewRestoExperience =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return RestaurantExperience(pfRestaurantExperienceObject: pfNewRestoExperience)
    }
    
    static func update(restaurantExperienceId: String, updateValuesAssignmentBlock: (RestaurantExperience) -> Void) -> Int {
        let updateObjectValuesAssignmentBlock = {(pfRestaurantExperience: PFObject) -> Void
            in
            let restaurantExperience = RestaurantExperience()
            updateValuesAssignmentBlock(restaurantExperience)
            pfRestaurantExperience.setValue(restaurantExperience.experienceDate, forKey: RestaurantExperienceFields.experienceDate.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.rating, forKey: RestaurantExperienceFields.rating.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.comments, forKey: RestaurantExperienceFields.comments.rawValue)
            pfRestaurantExperience.setValue(restaurantExperience.commentsCanonical, forKey: RestaurantExperienceFields.commentsCanonical.rawValue)
        }
        return dataAdapter.update(self.dataSourceEntityName, predicate: Predicate(lhs: RestaurantExperienceFields.id.rawValue, op: .Equals, rhs: restaurantExperienceId), updateOnlyOne: true, valuesAssignmentBlock: updateObjectValuesAssignmentBlock)

    }
    
    // JABE, 11/8/2015: For some reason, using class intead of static causes this method to be called twice when convenience initializer in 
    // RestaurantExperience+ParseInit is called. In which case, restaurantId becomes a bad address and causes a crash.
    static func getLatestReviewOfRestaurant(restaurantId: String) -> RestaurantExperience? {
        let restaurantPredicate = Predicate(lhs: RestaurantExperienceFields.restaurantId.rawValue, op: PredicateOperator.Equals, rhs: restaurantId)
        var sortDescriptors = [NSSortDescriptor]()
        sortDescriptors.append(NSSortDescriptor(key: RestaurantExperienceFields.experienceDate.rawValue, ascending: false))
        if let restoExperience =  firstOrDefault(restaurantPredicate, sortDescriptors: sortDescriptors) {
            return restoExperience
        }
        return RestaurantExperience?.None
    }
    
    static func remove(reviewId: String, completionBlock: ((success: Bool, error: NSError?) -> Void)? ) {
        // JABE, Dec. 1, 2015: Can't use ParseDataAdapter here because synchronous delete (PFObject.delete) causes build error (delete not available in Swift).
        // Without callbacks, there's no way to determine delete completion and I'm not willing to extend the DataAdapter protocol to support callbacks at this time.
        
        let photoQuery = PFQuery(className: ExperiencePhoto.dataSourceEntityName)
        photoQuery.whereKey(ExperiencePhotoFields.experienceId.rawValue, equalTo: reviewId)
        photoQuery.findObjectsInBackgroundWithBlock { (photos, error) -> Void in
            if error == nil {
                let deleteGroup = dispatch_group_create()
                if let photos = photos {
                    for photo in photos {
                        dispatch_group_enter(deleteGroup)
                        photo.deleteInBackgroundWithBlock({ (success, error) -> Void in
                            dispatch_group_leave(deleteGroup)
                        })

                    }
                }
                dispatch_group_notify(deleteGroup, dispatch_get_main_queue(), { () -> Void in
                    let pfReview = PFObject(withoutDataWithClassName: self.dataSourceEntityName, objectId: reviewId)
                    pfReview.deleteInBackgroundWithBlock { (success, error) -> Void in
                        if let completionBlock = completionBlock {
                            completionBlock(success: success, error: error)
                        }
                    }
                })
            }
        }
    }
    
    class func getAllReviews(restaurantId : String) -> [RestaurantExperience]? {
        let restaurantPredicate = Predicate(lhs: RestaurantExperienceFields.restaurantId.rawValue, op: PredicateOperator.Equals, rhs: restaurantId)
        var sortDescriptors = [NSSortDescriptor]()
        sortDescriptors.append(NSSortDescriptor(key: RestaurantExperienceFields.experienceDate.rawValue, ascending: false))
        return all(restaurantPredicate, limit: Int?.None, sortDescriptors: sortDescriptors)
    }
    
}
