//
//  RestaurantExperience+UniQProtocol.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension RestaurantExperience : UniQProtocol {
    public typealias T = RestaurantExperience
    public static var dataSourceEntityName: String {
        get {
            return "RestaurantExperience"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
