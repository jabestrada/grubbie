//
//  RestaurantExperienceFields.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum RestaurantExperienceFields : String {
    case id = "id"
    case objectId = "objectId"
    case restaurantId = "restaurantId"
    case experienceDate = "experienceDate"
    case rating = "rating"
    case comments = "comments"
    case commentsCanonical = "commentsCanonical"
}
