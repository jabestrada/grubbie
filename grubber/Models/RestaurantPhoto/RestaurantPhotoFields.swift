//
//  RestaurantPhotoFields.swift
//  grubber
//
//  Created by JABE on 11/19/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum RestaurantPhotoFields : String {
    case id = "id"
    case restaurantId = "restaurantId"
    case comments = "comments"
    case commentsCanonical = "commentsCanonical"
    case photo = "photo"
    case dateTaken = "dateTaken"
    case sortOrder = "sortOrder"
    case audio = "audio"
 
    case shareNotifRefId = "shareNotifRefId"
    case sharedBy = "sharedBy"
    case origSharedBy = "origSharedBy"
    case sourceRestaurantPhotoId = "sourceRestaurantPhotoId"
}
