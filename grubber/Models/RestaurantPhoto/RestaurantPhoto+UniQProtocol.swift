//
//  RestaurantPhoto+UniQProtocol.swift
//  grubber
//
//  Created by JABE on 11/19/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension RestaurantPhoto : UniQProtocol  {
    public typealias T = RestaurantPhoto
    public static var dataSourceEntityName: String {
        get {
            return "RestaurantPhoto"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
