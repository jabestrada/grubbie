//
//  RestaurantPhoto+ParseInit.swift
//  grubber
//
//  Created by JABE on 11/19/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public extension RestaurantPhoto {
    convenience init(pfRestaurantPhotoObject: PFObject){
        self.init()
        id = pfRestaurantPhotoObject.objectId!
        restaurantId = pfRestaurantPhotoObject[RestaurantPhotoFields.restaurantId.rawValue] as! String
        comments = pfRestaurantPhotoObject[RestaurantPhotoFields.comments.rawValue] as! String
        dateTaken = pfRestaurantPhotoObject[RestaurantPhotoFields.dateTaken.rawValue] as! NSDate
        sortOrder = pfRestaurantPhotoObject[RestaurantPhotoFields.sortOrder.rawValue] as! Int
        photoPfFile = pfRestaurantPhotoObject[RestaurantPhotoFields.photo.rawValue] as? PFFile
        
        
        if let shareNotifRefId = pfRestaurantPhotoObject[RestaurantPhotoFields.shareNotifRefId.rawValue] as? String {
            self.shareNotifRefId = shareNotifRefId
        }
        else {
            self.shareNotifRefId = ""
        }
        if let sourceRestaurantPhotoId = pfRestaurantPhotoObject[RestaurantPhotoFields.sourceRestaurantPhotoId.rawValue] as? String{
            self.sourceRestaurantPhotoId = sourceRestaurantPhotoId
        }
        else {
            self.sourceRestaurantPhotoId = ""
        }
        if let sharedBy = pfRestaurantPhotoObject[RestaurantPhotoFields.sharedBy.rawValue] as? String {
            self.sharedBy = sharedBy
        }
        else {
            self.sharedBy = ""
        }
        if let origSharedBy = pfRestaurantPhotoObject[RestaurantPhotoFields.origSharedBy.rawValue] as? String {
            self.origSharedBy = origSharedBy
        }
        else {
            self.origSharedBy = ""
        }
//        let imageFile = pfRestaurantPhotoObject[RestaurantPhotoFields.photo.rawValue] as! PFFile
//        do {
//            let data = try imageFile.getData()
//            photo = UIImage(data: data)
//        }
//        catch let e as NSError {
//            print("RestaurantPhoto.init: Error retrieving image file. Details: \(e.localizedDescription)")
//        }
        if let audioFile = pfRestaurantPhotoObject[RestaurantPhotoFields.audio.rawValue] as? PFFile {
            do {
                let audioData = try audioFile.getData()
                audio = audioData
            }
            catch let e as NSError {
                print("RestaurantPhoto.init: Error retrieving audio file. Details: \(e.localizedDescription)")
            }
        }
    }

}
