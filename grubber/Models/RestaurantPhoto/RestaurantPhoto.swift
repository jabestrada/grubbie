 //
//  RestaurantPhoto.swift
//  grubber
//
//  Created by JABE on 11/19/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

 //: PhotoSource
public class RestaurantPhoto : NSObject
{
    var id = ""
    var restaurantId = ""
    
    var comments = "" {
        didSet {
            commentsCanonical = Util.canonicalize(comments)
        }
    }
    var commentsCanonical = ""
    
    var photo: UIImage!
    var dateTaken = NSDate()
    var sortOrder = 100000
    var audio = NSData()
    var photoPfFile : PFFile?
    var photoCompressionQuality = CGFloat(0.5)
    
    // Share-related fields
    var shareNotifRefId = ""
    var sourceRestaurantPhotoId = ""
    var sharedBy : String = ""
    var origSharedBy : String = ""
    
    func isNew() -> Bool {
        return id == ""
    }
    
    static func copyWithNotif(source : RestaurantPhoto, sourcePhotoType: SharePhotoType, target: RestaurantPhoto, restaurantId: String, shareNotif: ShareNotification) {
        target.restaurantId = restaurantId
        target.comments = source.comments
        target.photo = source.photo
        target.audio = source.audio
        target.sharedBy = shareNotif.sharedBy
        target.shareNotifRefId = shareNotif.id
        if sourcePhotoType == SharePhotoType.ExperiencePhoto {
            target.sourceRestaurantPhotoId = "expPhoto:\(source.id)"
        }
        else {
            target.sourceRestaurantPhotoId = "resPhoto:\(source.id)"
        }
        if source.origSharedBy.isEmpty {
            target.origSharedBy = shareNotif.sharedBy
        } else {
            target.origSharedBy = source.origSharedBy
        }
        // Maintain photo compression quality because this is a share; otherwise, photo quality degrades as the same photo is shared by sharee(s).
        target.photoCompressionQuality = CGFloat(1.0)
        
    }
    
    static func copyWithNotif(source : ExperiencePhoto, sourcePhotoType: SharePhotoType, target: RestaurantPhoto, restaurantId: String, shareNotif: ShareNotification) {
        target.restaurantId = restaurantId
        target.comments = source.comments
        target.photo = source.photo
        target.audio = source.audio
        target.sharedBy = shareNotif.sharedBy
        target.shareNotifRefId = shareNotif.id
        if sourcePhotoType == SharePhotoType.ExperiencePhoto {
            target.sourceRestaurantPhotoId = "expPhoto:\(source.id)"
        }
        else {
            target.sourceRestaurantPhotoId = "resPhoto:\(source.id)"
        }
        if source.origSharedBy.isEmpty {
            target.origSharedBy = shareNotif.sharedBy
        } else {
            target.origSharedBy = source.origSharedBy
        }
        // Maintain photo compression quality because this is a share; otherwise, photo quality degrades as the same photo is shared by sharee(s).
        target.photoCompressionQuality = CGFloat(1.0)
    
    }
    
    static func insertFromShareNotification(shareNotif: ShareNotification, completion: ((success: Bool, newRestaurantPhoto: RestaurantPhoto?, error: NSError?) -> Void)?) {
        guard shareNotif.shareType == SharePhotoType.RestaurantPhoto else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.InvalidOperation, description: "Cannot accept notification id \(shareNotif.id)", reason: "Share Notification's share type [\(shareNotif.shareType)] is not of type RestaurantPhoto"))
            return
        }
        
        guard let sharerPhoto = getById(shareNotif.shareRefId) else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.ObjectNotFound, description: "Cannot accept notification id \(shareNotif.id)", reason: "RestaurantPhoto object [\(shareNotif.shareRefId)] not found"))
            return
        }
        
        guard let sharerRestaurant = Restaurant.getById(sharerPhoto.restaurantId) else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.ObjectNotFound, description: "Cannot accept notification id \(shareNotif.id)", reason: "Restaurant object [\(sharerPhoto.restaurantId)] not found"))
            return
        }
        
        // Create photo's restaurant for user if it doesn't exist yet.
        var shareeRestaurant : Restaurant! = Restaurant.getByName(sharerRestaurant.name, userId: Util.getCurrentUserId(), exceptId: String?.None)
        if shareeRestaurant == nil {
            shareeRestaurant = Restaurant.insert({ (restaurant) -> Void in
                Restaurant.copyWithNotif(sharerRestaurant, target: restaurant, shareNotif: shareNotif)
            })
            print("shareeRestaurant: \(shareeRestaurant.id)")
        }
        
        sharerPhoto.getPhotoInBackground { (image, error) -> Void in
            let newRestaurantPhoto = RestaurantPhoto.insert { (restaurantPhoto) -> Void in
                RestaurantPhoto.copyWithNotif(sharerPhoto, sourcePhotoType: SharePhotoType.RestaurantPhoto, target: restaurantPhoto, restaurantId: shareeRestaurant.id, shareNotif: shareNotif)
//                restaurantPhoto.restaurantId = shareeRestaurant.id
//                restaurantPhoto.comments = sharerPhoto.comments
//                restaurantPhoto.dateTaken = sharerPhoto.dateTaken
//                restaurantPhoto.sortOrder = sharerPhoto.sortOrder
//                restaurantPhoto.photo = sharerPhoto.photo
//                restaurantPhoto.audio = sharerPhoto.audio
//                restaurantPhoto.sharedBy = shareNotif.sharedBy
//                restaurantPhoto.shareNotifRefId = shareNotif.id
//                restaurantPhoto.sourceRestaurantPhotoId = sharerPhoto.id
//                if sharerPhoto.origSharedBy.isEmpty {
//                    restaurantPhoto.origSharedBy = shareNotif.sharedBy
//                } else {
//                    restaurantPhoto.origSharedBy = sharerPhoto.origSharedBy
//                }
//                // Maintain photo compression quality because this is a share; otherwise, photo quality degrades as the same photo is shared by sharee(s)..
//                restaurantPhoto.photoCompressionQuality = CGFloat(1.0)
            }
            print("RestaurantPhoto created \(newRestaurantPhoto.id)")
            completion?(success: error == nil, newRestaurantPhoto: newRestaurantPhoto, error: error)
        }

    }
    
    
    static func getById(restaurantPhotoId: String) -> RestaurantPhoto? {
        return RestaurantPhoto.firstOrDefault(Predicate(lhs: RestaurantPhotoFields.id.rawValue, op: .Equals, rhs: restaurantPhotoId), sortDescriptors: nil)
    }

    static func findPhotos(settings: PhotoSearchSettings, completion: (photos: [PhotoSource]?, error: NSError?) -> Void) {
        let userRestaurantsQuery = PFQuery(className: Restaurant.dataSourceEntityName)
        userRestaurantsQuery.whereKey("userId", equalTo: Util.getCurrentUserId())
        if !settings.country.isEmpty {
            userRestaurantsQuery.whereKey("countryCanonical", containsString: Util.canonicalize(settings.country))
        }
        if !settings.townCity.isEmpty {
            userRestaurantsQuery.whereKey("townCityCanonical", containsString: Util.canonicalize(settings.townCity))
        }
        if !settings.restaurantName.isEmpty {
            userRestaurantsQuery.whereKey("nameCanonical", containsString: Util.canonicalize(settings.restaurantName))
        }
        
        let restoPhotosQuery = PFQuery(className: RestaurantPhoto.dataSourceEntityName)
        restoPhotosQuery.whereKey("restaurantId", matchesKey: "objectId", inQuery: userRestaurantsQuery)
        if !settings.photoCaption.isEmpty {
            restoPhotosQuery.whereKey("commentsCanonical", containsString: Util.canonicalize(settings.photoCaption))
        }
        
        restoPhotosQuery.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            if error == nil {
                var restoPhotos = [RestaurantPhoto]()
                if let results = results {
                    for result in results{
                        restoPhotos.append(RestaurantPhoto(pfRestaurantPhotoObject: result))
                    }
                }
                completion(photos: restoPhotos, error: error)
            }
            else {
                completion(photos: [PhotoSource]?.None, error: error)
                print(error?.localizedDescription)
            }
        }
    }
    
    static func getLatestPic(restaurantId: String, completion: (photo: PhotoSource?, error: NSError?) -> Void) {
        let sortByDate = NSSortDescriptor(key: RestaurantPhotoFields.dateTaken.rawValue, ascending: false)
        let restoPredicate = Predicate(lhs: RestaurantPhotoFields.restaurantId.rawValue, op: .Equals, rhs: restaurantId)
        
        let latestRestoPhoto = firstOrDefault(restoPredicate, sortDescriptors: [sortByDate])
        if let latestRestoPhoto = latestRestoPhoto {
            latestRestoPhoto.getPhotoInBackground({ (photo, error) -> Void in
                if error == nil {
                    latestRestoPhoto.photo = photo
                    completion(photo: latestRestoPhoto, error: nil)
                } else {
                    completion(photo: nil, error: error)
                }
            })
        }
        else {
            completion(photo: nil, error: nil)
        }
    }
    
    
    class func getPhotosOfRestaurant(restaurantId: String) ->   [RestaurantPhoto] {
        return RestaurantPhoto.all(Predicate(lhs: RestaurantPhotoFields.restaurantId.rawValue, op: .Equals, rhs: restaurantId), limit: nil, sortDescriptors: nil)
    }
    
    class func insert(newValuesAssignmentBlock: (RestaurantPhoto) -> Void) -> RestaurantPhoto {
        let insertObjectValuesAssignmentBlock = {(pfRestaurantPhoto: PFObject) -> Void
            in
            let restaurantPhoto = RestaurantPhoto()
            newValuesAssignmentBlock(restaurantPhoto)
            
            pfRestaurantPhoto.setValue(NSUUID().UUIDString, forKey: RestaurantPhotoFields.id.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.restaurantId, forKey: RestaurantPhotoFields.restaurantId.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.comments, forKey: RestaurantPhotoFields.comments.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.commentsCanonical, forKey: RestaurantPhotoFields.commentsCanonical.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.dateTaken, forKey: RestaurantPhotoFields.dateTaken.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.sortOrder, forKey: RestaurantPhotoFields.sortOrder.rawValue)
            
            pfRestaurantPhoto.setValue(restaurantPhoto.shareNotifRefId, forKey: RestaurantPhotoFields.shareNotifRefId.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.sharedBy, forKey: RestaurantPhotoFields.sharedBy.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.origSharedBy, forKey: RestaurantPhotoFields.origSharedBy.rawValue)
            pfRestaurantPhoto.setValue(restaurantPhoto.sourceRestaurantPhotoId, forKey: RestaurantPhotoFields.sourceRestaurantPhotoId.rawValue)
            let photoRawData = UIImageJPEGRepresentation(restaurantPhoto.photo, restaurantPhoto.photoCompressionQuality)!
            print("Photo size: \(photoRawData.length)")
            let photoData = PFFile(data: NSData(data: photoRawData))!
            pfRestaurantPhoto.setObject(photoData, forKey: RestaurantPhotoFields.photo.rawValue)

            if restaurantPhoto.audio.length > 0 {
                let audioData = PFFile(data: restaurantPhoto.audio)
               pfRestaurantPhoto.setObject(audioData!, forKey: RestaurantPhotoFields.audio.rawValue)
            }
        }
        
        let pfRestaurantPhoto =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return RestaurantPhoto(pfRestaurantPhotoObject: pfRestaurantPhoto)
    }

    static func update(restaurantPhotoId: String, updatePhoto: Bool, updateValuesAssignmentBlock: (RestaurantPhoto) -> Void) -> Int {
        let updateObjectValuesAssignmentBlock = {(pfRestaurantPhoto: PFObject) -> Void
            in
            let restoPhoto = RestaurantPhoto()
            updateValuesAssignmentBlock(restoPhoto)
            
            pfRestaurantPhoto.setValue(restoPhoto.restaurantId, forKey: RestaurantPhotoFields.restaurantId.rawValue)
            pfRestaurantPhoto.setValue(restoPhoto.comments, forKey: RestaurantPhotoFields.comments.rawValue)
            pfRestaurantPhoto.setValue(restoPhoto.commentsCanonical, forKey: RestaurantPhotoFields.commentsCanonical.rawValue)
            if updatePhoto {
                let photoRawData = UIImageJPEGRepresentation(restoPhoto.photo, restoPhoto.photoCompressionQuality)!
                print("Photo size: \(photoRawData.length)")
                let photoData = PFFile(data: NSData(data: photoRawData))!
                pfRestaurantPhoto.setObject(photoData, forKey: RestaurantPhotoFields.photo.rawValue)
            }
            if restoPhoto.audio.length > 0 {
                let audioData = PFFile(data: restoPhoto.audio)
                pfRestaurantPhoto.setObject(audioData!, forKey: RestaurantPhotoFields.audio.rawValue)
            }
        }
        return dataAdapter.update(self.dataSourceEntityName, predicate: Predicate(lhs: RestaurantPhotoFields.id.rawValue, op: .Equals, rhs: restaurantPhotoId), updateOnlyOne: true, valuesAssignmentBlock: updateObjectValuesAssignmentBlock)
    }

    static func remove(restaurantPhotoId: String, completionBlock: ((success: Bool, error: NSError?) -> Void)? ) {
        // JABE, Dec. 1, 2015: Can't use ParseDataAdapter here because synchronous delete (PFObject.delete) causes build error (delete not available in Swift).
        // Without callbacks, there's no way to determine delete completion and I'm not willing to extend the DataAdapter protocol to support callbacks at this time.
        let restaurantPhoto = PFObject(withoutDataWithClassName: RestaurantPhoto.dataSourceEntityName, objectId: restaurantPhotoId)
        restaurantPhoto.deleteInBackgroundWithBlock { (success, error) -> Void in
            if let completion = completionBlock {
                completion(success: error == nil, error: error)
            }
        }

    }
    
}

