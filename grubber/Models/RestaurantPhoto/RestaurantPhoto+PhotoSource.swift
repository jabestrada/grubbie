//
//  RestaurantPhoto+PhotoSource.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

// MARK: PhotoSource
extension RestaurantPhoto : PhotoSource {
    func getId() -> String {
        return self.id
    }
    
    func getRestaurantName() -> String {
        if let restaurant = Restaurant.getById(self.restaurantId) {
            return restaurant.name
        }
        return ""
    }
    
    func getComments() -> String {
        return self.comments
    }
    
    func getPhotoType() -> PhotoSourceType {
        return .RestaurantPhoto
    }
    
    func getPhoto() -> UIImage {
        return self.photo
    }
    func getPhotoPfFile() -> PFFile? {
        return self.photoPfFile
    }
    
    func getPhotoSourceInfoInBackground(completion: (photoSourceInfo: PhotoSourceInfo?, error: NSError?) -> Void) {
        var restaurant : Restaurant?
        ThreadingUtil.inBackground({
            restaurant = Restaurant.firstOrDefault(Predicate(lhs: RestaurantFields.id.rawValue, op: .Equals, rhs: self.restaurantId), sortDescriptors: nil)
            }) {
                if let restaurant = restaurant {
                    completion(photoSourceInfo: PhotoSourceInfo(restaurantName: restaurant.name, townCity: restaurant.townCity, country: restaurant.country) , error: nil)
                } else {
                    completion(photoSourceInfo: nil ,
                        error: Factory.createError(.ObjectNotFound,
                            description: "Cannot resolve location info for Restaurant Photo [\(self.getId())]",
                            reason: "Restaurant id [\(self.restaurantId)] not found"))
                }
        }
    }
    
    
    
    func getPhotoInBackground(completion: (image: UIImage?, error: NSError?) -> Void) {
        if let imageFile = self.photoPfFile {
            imageFile.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil {
                    self.photo = UIImage(data: data!)
                    completion(image: self.photo, error: error)
                } else {
                    completion(image: nil, error: error)
                }
            })
            
        }
    }
    
    func getAudio() -> NSData? {
        if self.audio.length > 0 {
            return self.audio
        }
        return NSData?.None
    }
}