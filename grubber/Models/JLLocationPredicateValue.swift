//
//  JLLocation.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class JLLocationPredicateValue {
    var latitude : Double
    var longitude : Double
    var withinMiles : Double

    init(latitude: Double, longitude: Double, withinMiles: Double){
        self.latitude = latitude
        self.longitude = longitude
        self.withinMiles = withinMiles
    }
}
