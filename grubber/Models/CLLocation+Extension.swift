//
//  CLLocation+Extension.swift
//  myEats
//
//  Created by JABE on 11/5/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


import QuadratTouch

// MARK: CLLocation extension
extension CLLocation {
    func parameters() -> Parameters {
        let ll      = "\(self.coordinate.latitude),\(self.coordinate.longitude)"
        let llAcc   = "\(self.horizontalAccuracy)"
        let alt     = "\(self.altitude)"
        let altAcc  = "\(self.verticalAccuracy)"
        let parameters = [
            Parameter.ll:ll,
            Parameter.llAcc:llAcc,
            Parameter.alt:alt,
            Parameter.altAcc:altAcc
        ]
        return parameters
    }
    
    func textForDistanceFrom(otherLocation: CLLocation) -> String {
        var distanceFromCurrentLocation = self.distanceFromLocation(otherLocation)
        var distanceUnitText =  distanceFromCurrentLocation > 1 ? "meters" : "meter"
        if distanceFromCurrentLocation > 1000 {
            distanceFromCurrentLocation = Util.roundTo(distanceFromCurrentLocation / 1000, decimalPlaces: 2)
            distanceUnitText =  distanceFromCurrentLocation > 1 ? "kilometers" : "kilometer"
            return "\(distanceFromCurrentLocation) \(distanceUnitText) away"
        }
        else {
            if distanceFromCurrentLocation == 0 {
                return  "Right where you are now"
            }
            else {
                let formatter = NSNumberFormatter()
                formatter.maximumFractionDigits = 0
                let distanceFromCurrentLocation = round(distanceFromCurrentLocation)
                
                return "\(formatter.stringFromNumber(distanceFromCurrentLocation)!) \(distanceUnitText) away"
            }
        }
    }
}
