//
//  ExperiencePhotoFields.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum ExperiencePhotoFields : String {
    case id = "id"
    case experienceId = "experienceId"
    case comments = "comments"
    case commentsCanonical = "commentsCanonical"
    case photo = "photo"
    case audio = "audio"
    
    case shareNotifRefId = "shareNotifRefId"
    case sourceExperiencePhotoId = "sourceExperiencePhotoId"
    case sharedBy = "sharedBy"
    case origSharedBy = "origSharedBy"
}