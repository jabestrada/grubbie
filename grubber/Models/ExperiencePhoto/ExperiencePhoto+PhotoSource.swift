//
//  ExperiencePhoto+PhotoSource.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

extension ExperiencePhoto : PhotoSource {
    func getId() -> String {
        return self.id
    }
    
    func getRestaurantName() -> String{
        if let theExperience = RestaurantExperience.firstOrDefault(Predicate(lhs: RestaurantExperienceFields.id.rawValue, op: .Equals, rhs: self.experienceId), sortDescriptors: nil) {
            if let restaurant = Restaurant.firstOrDefault(Predicate(lhs: RestaurantFields.id.rawValue, op: .Equals, rhs: theExperience.restaurantId), sortDescriptors: nil){
                return restaurant.name
                
            }
            else {
                return ""
            }
            
        } else {
            return ""
        }
    }
    
    func getPhotoType() -> PhotoSourceType {
        return .ExperiencePhoto
    }
    
    func getComments() -> String {
        return self.comments
    }
    
    func getPhoto() -> UIImage {
        return self.photo
    }
    
    
    func getPhotoSourceInfoInBackground(completion: (photoSourceInfo: PhotoSourceInfo?, error: NSError?) -> Void) {
        var theExperience : RestaurantExperience?
        var restaurant : Restaurant?
        ThreadingUtil.inBackground({
            theExperience = RestaurantExperience.firstOrDefault(Predicate(lhs: RestaurantExperienceFields.id.rawValue, op: .Equals, rhs: self.experienceId), sortDescriptors: nil)
            if theExperience != RestaurantExperience?.None {
                restaurant = Restaurant.firstOrDefault(Predicate(lhs: RestaurantFields.id.rawValue, op: .Equals, rhs: theExperience!.restaurantId), sortDescriptors: nil)
            }
            
            }) {
                if let experience = theExperience {
                    if let restaurant = restaurant {
                        completion(photoSourceInfo: PhotoSourceInfo(restaurantName: restaurant.name, townCity: restaurant.townCity, country: restaurant.country) , error: nil)
                    } else {
                        completion(photoSourceInfo: nil ,
                            error: Factory.createError(.ObjectNotFound,
                                description: "Cannot resolve location info for experience photo [\(self.getId())]",
                                reason: "Restaurant id [\(experience.restaurantId)] not found"))
                    }
                } else {
                    completion(photoSourceInfo: nil ,
                        error: Factory.createError(.ObjectNotFound,
                            description: "Cannot resolve location info for experience photo [\(self.getId())]",
                            reason: "RestaurantExperience id [\(self.experienceId)] not found"))
                }
        }
    }
    
    
    
    func getPhotoPfFile() -> PFFile? {
        return self.photoPfFile
    }
    
    func getPhotoInBackground(completion: (image: UIImage?, error: NSError?) -> Void) {
        if let imageFile = self.photoPfFile {
            imageFile.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil {
                    self.photo = UIImage(data: data!)
                    completion(image: self.photo, error: error)
                } else {
                    completion(image: nil, error: error)
                }
            })
            
        }
    }
    
    func getAudio() -> NSData? {
        if self.audio.length > 0 {
            return self.audio
        }
        return NSData?.None
    }
}