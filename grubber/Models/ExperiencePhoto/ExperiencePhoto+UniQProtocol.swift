//
//  ExperiencePhoto+UniQProtocol.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



extension ExperiencePhoto : UniQProtocol  {
    public typealias T = ExperiencePhoto
    public static var dataSourceEntityName: String {
        get {
            return "ExperiencePhoto"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
