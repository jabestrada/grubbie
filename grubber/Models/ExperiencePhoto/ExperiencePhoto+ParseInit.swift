//
//  ExperiencePhoto+ParseInit.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


public extension ExperiencePhoto {
    convenience init(pfExperiencePhotoObject: PFObject){
        self.init()
        id = pfExperiencePhotoObject.objectId!
        experienceId = pfExperiencePhotoObject[ExperiencePhotoFields.experienceId.rawValue] as! String
        comments = pfExperiencePhotoObject[ExperiencePhotoFields.comments.rawValue] as! String
        let imageFile = pfExperiencePhotoObject[ExperiencePhotoFields.photo.rawValue] as! PFFile
        self.photoPfFile = imageFile
        
        if let shareNotifRefId = pfExperiencePhotoObject[ExperiencePhotoFields.shareNotifRefId.rawValue] as? String {
            self.shareNotifRefId = shareNotifRefId
        }
        else {
            self.shareNotifRefId = ""
        }
        if let sourceExperiencePhotoId = pfExperiencePhotoObject[ExperiencePhotoFields.sourceExperiencePhotoId.rawValue] as? String {
            self.sourceExperiencePhotoId = sourceExperiencePhotoId
        }
        else {
            self.sourceExperiencePhotoId = ""
        }
        if let sharedBy = pfExperiencePhotoObject[ExperiencePhotoFields.sharedBy.rawValue] as? String {
            self.sharedBy = sharedBy
        }
        else {
            self.sharedBy = ""
        }
        if let origSharedBy = pfExperiencePhotoObject[ExperiencePhotoFields.origSharedBy.rawValue] as? String {
            self.origSharedBy = origSharedBy
        }
        else {
            self.origSharedBy = ""
        }
        
        if let audioFile = pfExperiencePhotoObject[RestaurantPhotoFields.audio.rawValue] as? PFFile {
            do {
                let audioData = try audioFile.getData()
                audio = audioData
            }
            catch let e as NSError {
                print("ExperiencePhoto: Error retrieving audio file. Details: \(e.localizedDescription)")
            }
        }
    }
}
