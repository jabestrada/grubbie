//
//  ExperiencePhoto.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


public class ExperiencePhoto: NSObject {
    var id = ""
    var experienceId = ""
    
    var comments = "" {
        didSet {
            commentsCanonical = Util.canonicalize(comments)
        }
    }
    var commentsCanonical = ""
    
    var photo : UIImage!
    var audio = NSData()
    var photoPfFile : PFFile?
    var photoCompressionQuality = CGFloat(0.5)
    
    // Share-related fields
    var shareNotifRefId = ""
    var sourceExperiencePhotoId = ""
    var sharedBy : String = ""
    var origSharedBy : String = ""
    
    
    func isNew() -> Bool {
        return id == ""
    } 
    
    
    static func insertFromShareNotification(shareNotif: ShareNotification, completion: ((success: Bool, newRestaurantPhoto: RestaurantPhoto?, error: NSError?) -> Void)?) {
        guard shareNotif.shareType == SharePhotoType.ExperiencePhoto else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.InvalidOperation, description: "Cannot accept notification id \(shareNotif.id)", reason: "Share Notification's share type [\(shareNotif.shareType)] is not of type ExperiencePhoto"))
            return
        }
        
        guard let sharerPhoto = ExperiencePhoto.getById(shareNotif.shareRefId) else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.ObjectNotFound, description: "Cannot accept notification id \(shareNotif.id)", reason: "ExperiencePhoto object [\(shareNotif.shareRefId)] not found"))
            return
        }
        
        guard let sharerReview = RestaurantExperience.getById(sharerPhoto.experienceId) else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.ObjectNotFound, description: "Cannot accept notification id \(shareNotif.id)", reason: "RestaurantExperience object [\(sharerPhoto.experienceId)] not found"))
            return
        }
        
        guard let sharerRestaurant = Restaurant.getById(sharerReview.restaurantId) else {
            completion?(success: false, newRestaurantPhoto: nil,
                error: Factory.createError(.ObjectNotFound, description: "Cannot accept notification id \(shareNotif.id)", reason: "Restaurant object [\(sharerReview.restaurantId)] not found"))
            return
        }
        
        // Create photo's restaurant for user if it doesn't exist yet.
        var shareeRestaurant : Restaurant! = Restaurant.getByName(sharerRestaurant.name, userId: Util.getCurrentUserId(), exceptId: String?.None)
        if shareeRestaurant == nil {
            shareeRestaurant = Restaurant.insert({ (restaurant) -> Void in
                Restaurant.copyWithNotif(sharerRestaurant, target: restaurant, shareNotif: shareNotif)
            })
            print("shareeRestaurant: \(shareeRestaurant.id)")
        }
        
        sharerPhoto.getPhotoInBackground { (image, error) -> Void in
            var newRestaurantPhoto : RestaurantPhoto?
            if image != nil {
                 newRestaurantPhoto = RestaurantPhoto.insert { (restaurantPhoto) -> Void in
                      RestaurantPhoto.copyWithNotif(sharerPhoto, sourcePhotoType: SharePhotoType.ExperiencePhoto, target: restaurantPhoto, restaurantId: shareeRestaurant.id, shareNotif: shareNotif)
                }
                if let _ = newRestaurantPhoto {
                    print("RestaurantPhoto created \(newRestaurantPhoto!.id)")
                }
            }
            completion?(success: error == nil, newRestaurantPhoto: newRestaurantPhoto, error: error)
        }
    }
    
    
    static func getById(experiencePhotoId: String) -> ExperiencePhoto? {
        return ExperiencePhoto.firstOrDefault(Predicate(lhs: ExperiencePhotoFields.id.rawValue, op: .Equals, rhs: experiencePhotoId), sortDescriptors: nil)
    }
    
    static func findPhotos(settings: PhotoSearchSettings, completion: (photos: [PhotoSource]?, error: NSError?) -> Void) {
        let userRestaurantsQuery = PFQuery(className: Restaurant.dataSourceEntityName)
        userRestaurantsQuery.whereKey("userId", equalTo: Util.getCurrentUserId())
        if !settings.country.isEmpty {
            userRestaurantsQuery.whereKey("countryCanonical", containsString: Util.canonicalize(settings.country))
        }
        if !settings.townCity.isEmpty {
            userRestaurantsQuery.whereKey("townCityCanonical", containsString: Util.canonicalize(settings.townCity))
        }
        if !settings.restaurantName.isEmpty {
            userRestaurantsQuery.whereKey("nameCanonical", containsString: Util.canonicalize(settings.restaurantName))
        }
        
        let restoReviews = PFQuery(className: RestaurantExperience.dataSourceEntityName)
        restoReviews.whereKey("restaurantId", matchesKey: "objectId", inQuery: userRestaurantsQuery)
        restoReviews.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            if error == nil {
                if let results = results {
                    var experienceIds = [String]()
                    for result in results {
                        experienceIds.append(result.objectId!)
                    }
                    let restoPhotos = PFQuery(className: ExperiencePhoto.dataSourceEntityName)
                    restoPhotos.whereKey("experienceId", containedIn: experienceIds)
                    if !settings.photoCaption.isEmpty {
                        restoPhotos.whereKey("commentsCanonical", containsString: Util.canonicalize(settings.photoCaption) )
                    }
                    restoPhotos.findObjectsInBackgroundWithBlock { (results, error) -> Void in
                        if error == nil {
                            var experiencePhotos = [ExperiencePhoto]()
                            if let results = results {
                                for result in results {
                                    experiencePhotos.append(ExperiencePhoto(pfExperiencePhotoObject: result))
                                }
                            }
                            completion(photos: experiencePhotos, error: error)
                        }
                        else {
                            completion(photos: [PhotoSource]?.None, error: error)
                        }
                    }
                } else {
                    completion(photos: [PhotoSource]?.None, error: error)
                }
            }
            else {
                completion(photos: [PhotoSource]?.None, error: error)
            }
        }
    }
    
    
    class func insert(newValuesAssignmentBlock: (ExperiencePhoto) -> Void) -> ExperiencePhoto {
        let insertObjectValuesAssignmentBlock = {(pfExperiencePhoto: PFObject) -> Void
            in
            let experiencePhoto = ExperiencePhoto()
            newValuesAssignmentBlock(experiencePhoto)
            
            pfExperiencePhoto.setValue(NSUUID().UUIDString, forKey: ExperiencePhotoFields.id.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.experienceId, forKey: ExperiencePhotoFields.experienceId.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.comments, forKey: ExperiencePhotoFields.comments.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.commentsCanonical, forKey: ExperiencePhotoFields.commentsCanonical.rawValue)
            let photoRawData = UIImageJPEGRepresentation(experiencePhoto.photo, experiencePhoto.photoCompressionQuality)!
            print("Photo size: \(photoRawData.length)")
            let photoData = PFFile(data: NSData(data: photoRawData))!
            pfExperiencePhoto.setObject(photoData, forKey: ExperiencePhotoFields.photo.rawValue)
            
            pfExperiencePhoto.setValue(experiencePhoto.shareNotifRefId, forKey: ExperiencePhotoFields.shareNotifRefId.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.sharedBy, forKey: ExperiencePhotoFields.sharedBy.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.origSharedBy, forKey: ExperiencePhotoFields.origSharedBy.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.sourceExperiencePhotoId, forKey: ExperiencePhotoFields.sourceExperiencePhotoId.rawValue)
            
            
            if experiencePhoto.audio.length > 0 {
                let audioData = PFFile(data: experiencePhoto.audio)
                pfExperiencePhoto.setObject(audioData!, forKey: ExperiencePhotoFields.audio.rawValue)
            }
        }
        
        let pfExperiencePhoto =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return ExperiencePhoto(pfExperiencePhotoObject: pfExperiencePhoto)
    }
    
    static func remove(experiencePhotoId: String, completionBlock: ((success: Bool, error: NSError?) -> Void)? ) {
        // JABE, Dec. 1, 2015: Can't use ParseDataAdapter here because synchronous delete (PFObject.delete) causes build error (delete not available in Swift).
        // Without callbacks, there's no way to determine delete completion and I'm not willing to extend the DataAdapter protocol to support callbacks at this time.
        let experiencePhoto = PFObject(withoutDataWithClassName: ExperiencePhoto.dataSourceEntityName, objectId: experiencePhotoId)
        experiencePhoto.deleteInBackgroundWithBlock { (success, error) -> Void in
            if let completion = completionBlock {
                completion(success: error == nil, error: error)
            }
        }
    }
    
    
    static func update(experiencePhotoId: String, updatePhoto: Bool, updateValuesAssignmentBlock: (ExperiencePhoto) -> Void) -> Int {
        let updateObjectValuesAssignmentBlock = {(pfExperiencePhoto: PFObject) -> Void
            in
            let experiencePhoto = ExperiencePhoto()
            updateValuesAssignmentBlock(experiencePhoto)
            
            pfExperiencePhoto.setValue(experiencePhoto.experienceId, forKey: ExperiencePhotoFields.experienceId.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.comments, forKey: ExperiencePhotoFields.comments.rawValue)
            pfExperiencePhoto.setValue(experiencePhoto.commentsCanonical, forKey: ExperiencePhotoFields.commentsCanonical.rawValue)
            if updatePhoto {
                let photoRawData = UIImageJPEGRepresentation(experiencePhoto.photo, 0.5)!
                print("Photo size: \(photoRawData.length)")
                let photoData = PFFile(data: NSData(data: photoRawData))!
                pfExperiencePhoto.setObject(photoData, forKey: ExperiencePhotoFields.photo.rawValue)
            }
            else {
                print("Photo field was not updated")
            }
            if experiencePhoto.audio.length > 0 {
                let audioData = PFFile(data: experiencePhoto.audio)
                pfExperiencePhoto.setObject(audioData!, forKey: ExperiencePhotoFields.audio.rawValue)
            }
        }
        return dataAdapter.update(self.dataSourceEntityName, predicate: Predicate(lhs: ExperiencePhotoFields.id.rawValue, op: .Equals, rhs: experiencePhotoId), updateOnlyOne: true, valuesAssignmentBlock: updateObjectValuesAssignmentBlock)
    }

    
    static func getReviewPhotos(experienceId: String) -> [ExperiencePhoto] {
        return ExperiencePhoto.all(Predicate(lhs: ExperiencePhotoFields.experienceId.rawValue, op: PredicateOperator.Equals, rhs: experienceId), limit: Int?.None, sortDescriptors: [NSSortDescriptor]?.None)
    }
    
    
//    static func getLatestPic(restaurantId: String) -> PhotoSource? {    
    static func getLatestPic(restaurantId: String, completion: (photo: PhotoSource?, error: NSError?) -> Void) {
    
        let restoReviewQuery = PFQuery(className: RestaurantExperience.dataSourceEntityName)
        restoReviewQuery.whereKey(RestaurantExperienceFields.restaurantId.rawValue, equalTo: restaurantId)
        
        let restoPhotoQuery = PFQuery(className: ExperiencePhoto.dataSourceEntityName)
        restoPhotoQuery.whereKey(ExperiencePhotoFields.experienceId.rawValue, matchesKey: RestaurantExperienceFields.objectId.rawValue, inQuery: restoReviewQuery)
        restoPhotoQuery.getFirstObjectInBackgroundWithBlock { (result, error) -> Void in
            if error == nil {
                let experiencePhoto = ExperiencePhoto(pfExperiencePhotoObject: result!)
                experiencePhoto.getPhotoInBackground({ (image, error) -> Void in
                    if error == nil {
                        experiencePhoto.photo = image!
                        completion(photo: experiencePhoto, error: error)
                    } else {
                        completion(photo: PhotoSource?.None, error: error)
                    }
                })
            } else {
                completion(photo: PhotoSource?.None, error: error)
            }
        }

    }
    
    
}

