//
//  AppModelEditorDelegate.swift
//  myEats
//
//  Created by JABE on 11/5/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

protocol AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController,  instance: T)
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T)
}
