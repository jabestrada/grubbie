//
//  Search.swift
//  Grubbie
//
//  Created by JABE on 12/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

class GrubbieSearch {
    var foundPhotos = [PhotoSource]()
    func findPhotos(settings: PhotoSearchSettings, completion: (photos: [PhotoSource]?, error: NSError?) -> Void ){
        var theError : NSError?
        self.foundPhotos.removeAll()
        ThreadingUtil.inBackground({ () -> Void in
            RestaurantPhoto.findPhotos(settings) { (photos, error) -> Void in
                theError = error
                if error == nil {
                    if let photos = photos {
                        self.foundPhotos.appendContentsOf(photos)
                    }
                }
                ExperiencePhoto.findPhotos(settings) { (photos, error) -> Void in
                    theError = error
                    if error == nil {
                        if let photos = photos {
                            self.foundPhotos.appendContentsOf(photos)
                        }
                    }
                    completion(photos: self.foundPhotos, error: theError)
                }
            }
            }) { () -> Void in
//                completion(photos: self.foundPhotos, error: theError)
        }
    }
}

