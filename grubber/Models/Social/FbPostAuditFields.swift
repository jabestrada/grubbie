//
//  FbPostAuditFields.swift
//  Grubbie
//
//  Created by Julius Estrada on 21/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public enum FbPostAuditFields : String {
    case id = "id"
    case fbRefId = "fbRefId"
    case appRefId = "appRefId"
    case objectType = "objectType"
    case postedBy = "postedBy"
    case postedOn = "postedOn"
}