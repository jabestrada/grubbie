//
//  FbPostAudit+UniQProtocol.swift
//  Grubbie
//
//  Created by Julius Estrada on 21/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

extension FbPostAudit : UniQProtocol  {
    public typealias T = FbPostAudit
    public static var dataSourceEntityName: String {
        get {
            return "FbPostAudit"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}