//
//  FbPostAudit+ParseInit.swift
//  Grubbie
//
//  Created by Julius Estrada on 21/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public extension FbPostAudit {
    convenience init(pfFbPostAuditObject: PFObject){
        self.init()
        id = pfFbPostAuditObject.objectId!
        fbRefId = pfFbPostAuditObject[FbPostAuditFields.fbRefId.rawValue] as! String
        appRefId = pfFbPostAuditObject[FbPostAuditFields.appRefId.rawValue] as! String
        objectType = pfFbPostAuditObject[FbPostAuditFields.objectType.rawValue] as! String
        postedBy = pfFbPostAuditObject[FbPostAuditFields.postedBy.rawValue] as! String
        postedOn = pfFbPostAuditObject[FbPostAuditFields.postedOn.rawValue] as! NSDate
    }
}