//
//  FbPostAudit.swift
//  Grubbie
//
//  Created by Julius Estrada on 21/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public class FbPostAudit {
    var id = ""
    var fbRefId = ""
    var appRefId = ""
    var objectType = ""
    var postedBy = ""
    var postedOn = NSDate()
    
    static func insert(newValuesAssignmentBlock: (FbPostAudit) -> Void) -> FbPostAudit {
        let insertObjectValuesAssignmentBlock = {(pfFbPostAudit: PFObject) -> Void
            in
            let fbPostAudit = FbPostAudit()
            newValuesAssignmentBlock(fbPostAudit)
            
            pfFbPostAudit.setValue(NSUUID().UUIDString, forKey: FbPostAuditFields.id.rawValue)
            pfFbPostAudit.setValue(fbPostAudit.fbRefId, forKey: FbPostAuditFields.fbRefId.rawValue)
            pfFbPostAudit.setValue(fbPostAudit.appRefId, forKey: FbPostAuditFields.appRefId.rawValue)
            pfFbPostAudit.setValue(fbPostAudit.objectType, forKey: FbPostAuditFields.objectType.rawValue)
            pfFbPostAudit.setValue(fbPostAudit.postedBy, forKey: FbPostAuditFields.postedBy.rawValue)
            pfFbPostAudit.setValue(fbPostAudit.postedOn, forKey: FbPostAuditFields.postedOn.rawValue)
     
        }
        
        let newPfFbPostAudit =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return FbPostAudit(pfFbPostAuditObject: newPfFbPostAudit)
    }
    
}