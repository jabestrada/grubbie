//
//  SocialPost.swift
//  grubber
//
//  Created by JABE on 11/16/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class SocialPost{
    var photo : UIImage?
    var caption: String?
    
    var refId: String
    var objectType: String
    // TODO: Add refId and objectType here so that these info can be saved after posting.
    
    init(refId: String, objectType: String, photo: UIImage?, caption: String?){
        self.refId = refId
        self.objectType = objectType
        self.photo = photo
        self.caption = caption
    }

//    convenience init(){
//        self.init photo: UIImage?.None, caption: String?.None)
//    }
//
//    convenience init(photo: UIImage){
//        self.init(photo: photo, caption: String?.None)
//    }
//
//    convenience init(caption: String){
//        self.init(photo: UIImage?.None, caption: caption)
//    }
}
