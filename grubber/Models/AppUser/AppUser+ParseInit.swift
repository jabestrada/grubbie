//
//  AppUser+ParseInit.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public extension AppUser{
    convenience init(pfUserObject: PFObject){
        self.init()
        id = pfUserObject.objectId!
        username = pfUserObject[AppUserFields.username.rawValue] as! String
        if let fbFirstName = pfUserObject[AppUserFields.fbFirstName.rawValue] as? String {
            self.fbFirstName = fbFirstName
        } else {
            fbFirstName = ""
        }
        if let fbLastName = pfUserObject[AppUserFields.fbLastName.rawValue] as? String {
            self.fbLastName = fbLastName
        } else {
            fbLastName = ""
        }
        if let fbId = pfUserObject[AppUserFields.fbId.rawValue] as? String {
            self.fbId = fbId
        }
        else {
            self.fbId = ""
        }
        if let profPicFile = pfUserObject[AppUserFields.profPic.rawValue] as? PFFile {
            self.profPicFile = profPicFile
        }
    }
}