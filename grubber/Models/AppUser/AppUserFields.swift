//
//  AppUserFields.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

enum AppUserFields : String {
    case id = "id"
    case username = "username"
    case authData = "authData"
    case fbId = "fbId"
    case fbFirstName = "fbFirstName"
    case fbLastName = "fbLastName"
    case profPic = "profilePic"
}