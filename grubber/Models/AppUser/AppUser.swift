//
//  AppUser.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public class AppUser {
    var id = ""
    var fbFirstName = ""
    var fbId = ""
    var fbLastName = ""
    var username = ""
    var profPicFile : PFFile?
    
    static func getAppAccount(userName: String) -> AppUser? {
         return firstOrDefault(Predicate(lhs: AppUserFields.username.rawValue, op: .Equals, rhs: userName), sortDescriptors: nil)
    }
    
    static func getUserByFbId(rawFbId: String) -> AppUser? {
         return firstOrDefault(Predicate(lhs: AppUserFields.fbId.rawValue, op: .Equals, rhs: rawFbId), sortDescriptors: nil)
    }
    
    static func getUserById(id: String) -> AppUser? {
         return firstOrDefault(Predicate(lhs: AppUserFields.id.rawValue, op: .Equals, rhs: id), sortDescriptors: nil)
    }
    
    static func remove(userId: String, completion: ((success: Bool, error: NSError?) -> Void)?){
        Restaurant.getRestaurantsOfUser(userId, contains: String?.None, locationPredicateValue: JLLocationPredicateValue?.None) { (restaurants) -> Void in

            let deleteUserDataGroup = dispatch_group_create()
            
            for restaurant in restaurants {
                dispatch_group_enter(deleteUserDataGroup)
                Restaurant.remove(restaurant.id, completionBlock: { (success, error) -> Void in
                    dispatch_group_leave(deleteUserDataGroup)
                })
            }
            
            ShareNotification.getShareNotificationsOfUser(userId, completion: { (success, shareNotifications, error) -> Void in
                for notification in shareNotifications {
                    if notification.shareType == SharePhotoType.AdHoc {
                        dispatch_group_enter(deleteUserDataGroup)
                        AdhocShare.remove(notification.shareRefId, completion: { (success, error) -> Void in
                            dispatch_group_leave(deleteUserDataGroup)
                        })
                    }
                }
            })
            
            dispatch_group_notify(deleteUserDataGroup, dispatch_get_main_queue(), { () -> Void in
                PFUser.currentUser()?.deleteInBackgroundWithBlock({ (success, error) -> Void in
                    if let completion = completion {
                        completion(success: true, error: nil)
                    }
                })
            })
        }
    }
    
    static func getProfPicImage(fbId: String) -> UIImage? {
        if let user = AppUser.getUserByFbId(fbId) {
            if let picFile = user.profPicFile {
                do {
                    let imageData = try picFile.getData()
                    return UIImage(data: imageData)
                }
                catch let e as NSError {
                    print(e)
                }
            }
        }
        return UIImage?.None
    }
    
    static func updateProfPic(picImage: UIImage, completionBlock: ((success: Bool, error: NSError?) -> Void)?){
        guard let currentUser = PFUser.currentUser() else {
            return
        }
        let photoData = UIImageJPEGRepresentation(picImage, 1.0)!
        let pfPhoto = PFFile(data: NSData(data: photoData))!
        currentUser.setObject(pfPhoto, forKey: AppUserFields.profPic.rawValue)
        currentUser.saveInBackgroundWithBlock { (success, error) -> Void in
            if let completion = completionBlock {
                completion(success: success, error: error)
            }
        }
        
    }
    
}