//
//  AppUser+UniQProtocol.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension AppUser : UniQProtocol {
    public typealias T = AppUser
    public static var dataSourceEntityName: String {
        get {
            return "_User"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
