//
//  Restaurant+ParseInit.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public extension Restaurant{
    convenience init(pfRestaurantObject: PFObject){
        self.init()
        id = pfRestaurantObject.objectId!        
        userId = pfRestaurantObject[RestaurantFields.userId.rawValue] as! String
        name = pfRestaurantObject[RestaurantFields.name.rawValue] as! String
        address = pfRestaurantObject[RestaurantFields.address.rawValue] as! String
        phone = pfRestaurantObject[RestaurantFields.phone.rawValue] as! String
        email = pfRestaurantObject[RestaurantFields.email.rawValue] as! String
        if let townCity = pfRestaurantObject[RestaurantFields.townCity.rawValue] as? String {
            self.townCity = townCity
        } else {
            self.townCity = ""
        }
        country = pfRestaurantObject[RestaurantFields.country.rawValue] as! String
        
        let geoPoint = pfRestaurantObject[RestaurantFields.location.rawValue] as! PFGeoPoint
        lat = geoPoint.latitude
        lng = geoPoint.longitude
        
        // Share-related fields
        if let shareNotifRefId = pfRestaurantObject[RestaurantFields.shareNotifRefId.rawValue] as? String {
            self.shareNotifRefId = shareNotifRefId
        }
        else {
            self.shareNotifRefId = ""
        }
        if let sourceRestaurantId = pfRestaurantObject[RestaurantFields.sourceRestaurantId.rawValue] as? String {
            self.sourceRestaurantId = sourceRestaurantId
        } else {
            self.sourceRestaurantId = ""
        }
        if let sharedBy = pfRestaurantObject[RestaurantFields.sharedBy.rawValue] as? String {
            self.sharedBy = sharedBy
        }
        else {
            self.sharedBy = ""
        }
        if let origSharedBy = pfRestaurantObject[RestaurantFields.origSharedBy.rawValue] as? String {
            self.origSharedBy = origSharedBy
        } else {
            self.origSharedBy = ""
        }
    }
}
