//
//  Restaurant+UniQProtocol.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension Restaurant : UniQProtocol {
    public typealias T = Restaurant
    public static var dataSourceEntityName: String {
        get {
            return "Restaurant"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
