//
//  RestaurantFields.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum RestaurantFields : String {
    case id = "id"
    case name = "name"
    case nameCanonical = "nameCanonical"
    case userId = "userId"
    case location = "location"
    case phone = "phone"
    case email = "email"
    case address = "address"
    case townCity = "townCity"
    case townCityCanonical = "townCityCanonical"
    case country = "country"
    case countryCanonical = "countryCanonical"
    
    case shareNotifRefId = "shareNotifRefId"
    case sourceRestaurantId = "sourceRestaurantId"
    case sharedBy = "sharedBy"
    case origSharedBy = "origSharedBy"
}
