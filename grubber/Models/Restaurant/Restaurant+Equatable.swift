//
//  Restaurant+Equatable.swift
//  myEats
//
//  Created by JABE on 11/4/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


extension Restaurant : Equatable {}

public func ==(lhs: Restaurant, rhs: Restaurant) -> Bool {
    return lhs.name == rhs.name
}

