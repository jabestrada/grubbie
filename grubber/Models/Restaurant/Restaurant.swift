//
//  Restaurant.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import QuadratTouch

public enum RestaurantSourceType : Int {
    case myEatsList = 0
    case otherRestosList = 1
}

public class Restaurant {
    var id: String = ""
    var userId : String = ""
    var name: String = "" {
        didSet {
            nameCanonical = Util.canonicalize(name)
        }
    }
    var nameCanonical : String = ""
    
    var address: String = ""
    
    var country: String = "" {
        didSet {
            countryCanonical = Util.canonicalize(country)
        }
    }
    var countryCanonical = ""
    
    var townCity : String = "" {
        didSet {
            townCityCanonical = Util.canonicalize(townCity)
        }
    }
    var townCityCanonical = ""
    
    var phone: String = ""
    var email: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    // Share-related fields
    var shareNotifRefId = ""
    var sourceRestaurantId = ""
    var sharedBy : String = ""
    var origSharedBy : String = ""
    
    func isNew() -> Bool {
        return id == ""
    }
    

    static func copyWithNotif(source: Restaurant, target: Restaurant, shareNotif: ShareNotification){
        target.name = source.name
        target.phone = source.phone
        target.email = source.email
        target.address = source.address
        target.townCity = source.townCity
        target.country = source.country
        target.lat = source.lat
        target.lng = source.lng
        target.sourceRestaurantId = source.id
        
        target.shareNotifRefId = shareNotif.id
        target.sharedBy = shareNotif.sharedBy
        if source.origSharedBy.isEmpty {
            target.origSharedBy = shareNotif.sharedBy
        } else {
            target.origSharedBy = source.origSharedBy
        }

    }
    
    static func getProfilePic(restaurantId : String, completion: (photo: PhotoSource?, error: NSError?) -> Void ) {
        RestaurantPhoto.getLatestPic(restaurantId) { (photo, error) -> Void in
            if let photo = photo {
                completion(photo: photo, error: error)
            } else {
                ExperiencePhoto.getLatestPic(restaurantId, completion: { (photo, error) -> Void in
                    completion(photo: photo, error: error)
                })
            }
        }
    }
    
    
    static func sortByDistance(location: CLLocation, restaurant1: Restaurant, restaurant2 : Restaurant) -> Bool {
        return location.distanceFromLocation(CLLocation(latitude: restaurant1.lat, longitude: restaurant1.lng)) <
            location.distanceFromLocation(CLLocation(latitude: restaurant2.lat, longitude: restaurant2.lng))
    }
    
    class func getById(id: String) -> Restaurant? {
        return firstOrDefault(Predicate(lhs: "id", op: .Equals, rhs: id), sortDescriptors: [NSSortDescriptor]?.None)
    }
    


    static func getByName(restaurantName: String) -> Restaurant? {
        // Validation for INSERT
        return Restaurant.getByName(restaurantName, exceptId: String?.None)
    }

    static func getByName(restaurantName: String, exceptId: String?) -> Restaurant? {
        return getByName(restaurantName, userId: Util.getCurrentUserId(), exceptId: exceptId)
    }

    
    static func getByName(restaurantName: String, userId: String, exceptId: String?) -> Restaurant? {
        if let _ = exceptId {
            assertionFailure("Invalid operation. Feature not yet currently supported.")
        }
        // Validation for UPDATE
        let namePredicate = Predicate(lhs: RestaurantFields.nameCanonical.rawValue, op: .Equals, rhs: Util.canonicalize(restaurantName))
        let userPredicate = Predicate(lhs: RestaurantFields.userId.rawValue, op: .Equals, rhs: userId)
        let predicateGroup = PredicateGroup(predicate1: namePredicate, predicate2: userPredicate)
        if let exceptId = exceptId {
            // JABE, Dec. 12 2015: .NotEquals doesn't currently work for Parse id fields, thus the assertion above.
            let restoPredicate = Predicate(lhs: RestaurantFields.id.rawValue, op: .NotEquals, rhs: exceptId)
            predicateGroup.andWith(PredicateGroup(predicate1: restoPredicate))
        }
        return firstOrDefault(predicateGroup, sortDescriptors: nil)
    }
    
    
    static func getNearbyRestaurants(location: CLLocation, nameFilter: String?, completionBlock: (restaurants: [Restaurant]) -> Void){
        let session = Session.sharedSession()
        var filter = "Food Restaurant"
        if let nameFilter = nameFilter {
            if !nameFilter.isEmpty {
                filter = nameFilter
            }
        }
        var parameters = [Parameter.query: filter]
        parameters += location.parameters()
        let searchTask = session.venues.search(parameters) {
            (result) -> Void in
            var restaurants = [Restaurant]()
            if let response = result.response {
                if let venues = response["venues"] as! [JSONParameters]? {
                    for venue in venues {
                        let restaurant = Restaurant(fourSquareVenue: venue)
                        if !restaurants.contains(restaurant){
                            restaurants.append(restaurant)
                        }
                    }
                    completionBlock(restaurants: restaurants)
                } else {
                    completionBlock(restaurants: restaurants)
                }
            } else {
                completionBlock(restaurants: restaurants)
            }
        }
        searchTask.start()
    }
    
    
    class func insert(newValuesAssignmentBlock: (Restaurant) -> Void) -> Restaurant {
        let insertRestoValuesAssignmentBlock = {(pfRestaurant: PFObject) -> Void
            in
                let restaurant = Restaurant()
                newValuesAssignmentBlock(restaurant)
                
                pfRestaurant.setValue(NSUUID().UUIDString, forKey: RestaurantFields.id.rawValue)
                pfRestaurant.setValue(Util.getCurrentUserId(), forKey: RestaurantFields.userId.rawValue)
                pfRestaurant.setValue(restaurant.name, forKey: RestaurantFields.name.rawValue)
                pfRestaurant.setValue(restaurant.nameCanonical, forKey: RestaurantFields.nameCanonical.rawValue)
                pfRestaurant.setValue(restaurant.phone, forKey: RestaurantFields.phone.rawValue)
                pfRestaurant.setValue(restaurant.email, forKey: RestaurantFields.email.rawValue)
                pfRestaurant.setValue(restaurant.address, forKey: RestaurantFields.address.rawValue)
                pfRestaurant.setValue(restaurant.country, forKey: RestaurantFields.country.rawValue)
                pfRestaurant.setValue(restaurant.countryCanonical, forKey: RestaurantFields.countryCanonical.rawValue)
                pfRestaurant.setValue(restaurant.townCity, forKey: RestaurantFields.townCity.rawValue)
                pfRestaurant.setValue(restaurant.townCityCanonical, forKey: RestaurantFields.townCityCanonical.rawValue)
                let locationGeoPoint = PFGeoPoint(latitude: restaurant.lat, longitude: restaurant.lng)
                pfRestaurant.setValue(locationGeoPoint, forKey: RestaurantFields.location.rawValue)
            
                pfRestaurant.setValue(restaurant.shareNotifRefId, forKey: RestaurantFields.shareNotifRefId.rawValue)
                pfRestaurant.setValue(restaurant.sourceRestaurantId, forKey: RestaurantFields.sourceRestaurantId.rawValue)
                pfRestaurant.setValue(restaurant.sharedBy, forKey: RestaurantFields.sharedBy.rawValue)
                pfRestaurant.setValue(restaurant.origSharedBy, forKey: RestaurantFields.origSharedBy.rawValue)
            }
        let pfNewResto =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertRestoValuesAssignmentBlock)
        return Restaurant(pfRestaurantObject: pfNewResto)
    }
    

    static func update(restaurantId: String, updateValuesAssignmentBlock: (Restaurant) -> Void) -> Int {
        let updateRestoValuesAssignmentBlock = {(pfRestaurant: PFObject) -> Void
            in
            let restaurant = Restaurant()
            updateValuesAssignmentBlock(restaurant)
            
            pfRestaurant.setValue(restaurant.name, forKey: RestaurantFields.name.rawValue)
            pfRestaurant.setValue(restaurant.nameCanonical, forKey: RestaurantFields.nameCanonical.rawValue)
            pfRestaurant.setValue(restaurant.phone, forKey: RestaurantFields.phone.rawValue)
            pfRestaurant.setValue(restaurant.email, forKey: RestaurantFields.email.rawValue)
            pfRestaurant.setValue(restaurant.address, forKey: RestaurantFields.address.rawValue)
            pfRestaurant.setValue(restaurant.townCity, forKey: RestaurantFields.townCity.rawValue)
            pfRestaurant.setValue(restaurant.townCityCanonical, forKey: RestaurantFields.townCityCanonical.rawValue)
            pfRestaurant.setValue(restaurant.country, forKey: RestaurantFields.country.rawValue)
            pfRestaurant.setValue(restaurant.countryCanonical, forKey: RestaurantFields.countryCanonical.rawValue)
            
            let locationGeoPoint = PFGeoPoint(latitude: restaurant.lat, longitude: restaurant.lng)
            pfRestaurant.setValue(locationGeoPoint, forKey: RestaurantFields.location.rawValue)
        }
        
        return dataAdapter.update(self.dataSourceEntityName, predicate: Predicate(lhs: RestaurantFields.id.rawValue, op: .Equals, rhs: restaurantId), updateOnlyOne: true, valuesAssignmentBlock: updateRestoValuesAssignmentBlock)
    }
    
    
    static func remove(restaurantId: String, completionBlock: ((success: Bool, error: NSError?) -> Void)? ) {
        
        ThreadingUtil.inBackground({ () -> Void in
            let deleteGroup = dispatch_group_create()
            if let reviews = RestaurantExperience.getAllReviews(restaurantId) {
                for review in reviews {
                    dispatch_group_enter(deleteGroup)
                    RestaurantExperience.remove(review.id, completionBlock: { (success, error) -> Void in
                        dispatch_group_leave(deleteGroup)
                    })
                }
            }
            
            let restaurantPhotos = RestaurantPhoto.getPhotosOfRestaurant(restaurantId)
            for photo in restaurantPhotos {
                dispatch_group_enter(deleteGroup)
                RestaurantPhoto.remove(photo.id, completionBlock: { (success, error) -> Void in
                    dispatch_group_leave(deleteGroup)
                })
            }
            
            dispatch_group_notify(deleteGroup, dispatch_get_main_queue(), { () -> Void in
                let restaurant = PFObject(withoutDataWithClassName: Restaurant.dataSourceEntityName, objectId: restaurantId)
                restaurant.deleteInBackgroundWithBlock({ (success, error) -> Void in
                    if let completion = completionBlock {
                        completion(success: success, error: error)
                    }
                })
            
            })
            
            })
            {
              // Do nothing.
        }
    }
    
    
    class func doesUserHaveRestaurant(userId: String) -> Bool {
        if let _ = Restaurant.firstOrDefault(createUserPredicate(userId), sortDescriptors: [NSSortDescriptor]?.None){
            return true
        }
        return false
    }

    class func doesUserHaveRestaurant() -> Bool {
        return doesUserHaveRestaurant(Util.getCurrentUserId())
    }
    
    
    class func createUserPredicate(userId : String) -> Predicate {
        return Predicate(lhs: RestaurantFields.userId.rawValue, op: PredicateOperator.Equals, rhs: userId)
    }
    
    // class
    static func getRestaurantsOfUser(userId: String?, contains: String?, locationPredicateValue: JLLocationPredicateValue?,
            completionBlock: (restaurants: [Restaurant]) -> Void ) {
                
        let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        dispatch_async(backgroundQueue, {
            let theUserId = userId == String?.None ? Util.getCurrentUserId() :  userId!
            let userPredicate = Predicate(lhs: RestaurantFields.userId.rawValue, op: .Equals, rhs: theUserId)
            var locationPredicate : Predicate?
            if let locationPredicateValue = locationPredicateValue {
                locationPredicate = Predicate(lhs: RestaurantFields.location.rawValue, op: PredicateOperator.NearGeo, rhs: locationPredicateValue)
            }
            let userAndLocationPredicateGroup = PredicateGroup(predicate1: userPredicate, predicate2: locationPredicate)
            if let contains = contains {
                if !contains.isEmpty {
                    let restaurantNamePredicate = Predicate(lhs: RestaurantFields.nameCanonical.rawValue, op: .Contains, rhs: Util.canonicalize(contains))
                    userAndLocationPredicateGroup.andWith(PredicateGroup(predicate1: restaurantNamePredicate))
                }
            }
            let restos = self.all(userAndLocationPredicateGroup, limit: nil, sortDescriptors: [NSSortDescriptor]?.None)
            
            dispatch_async(dispatch_get_main_queue(), {
                completionBlock(restaurants: restos)
            })
            
        })
    }
}


// MARK: FourSquare initializer
public typealias JLFourSquareVenue = [String: AnyObject]

public extension Restaurant{
     convenience init(fourSquareVenue: JLFourSquareVenue){
        self.init()
        userId = Util.getCurrentUserId()
        name = fourSquareVenue["name"] as! String

        if let venueLoc = fourSquareVenue["location"] as! JSONParameters? {
            if let address = venueLoc["address"] as? String {
                self.address = address
            }
            if let country = venueLoc["country"] as? String {
                self.country = country
            }
            if let lat  = venueLoc["lat"] as? Double {
                self.lat = lat
            }
            if let lng = venueLoc["lng"] as? Double {
                self.lng = lng
            }
        }

        if let contact = fourSquareVenue["contact"] as! JSONParameters?{
            if let formattedPhone = contact["formattedPhone"] as? String{
                phone = formattedPhone
            }
            else {
                if let phone = contact["phone"] as? String{
                    self.phone = phone
                }
            }
        }

    }
}





















