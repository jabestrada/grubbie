//
//  ShareSummaryFields.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


enum ShareSummaryFields : String {
    case id = "id"
    case sharedBy = "sharedBy"
    case photoCount = "photoCount"
    case recipientCount = "recipientCount"
    case startProcessDate = "startProcessDate"
    case endProcessDate = "endProcessDate"
}
