//
//  ShareSummary+UniQProtocol.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension ShareSummary : UniQProtocol {
    public typealias T = ShareSummary
    public static var dataSourceEntityName: String {
        get {
            return "ShareSummary"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}