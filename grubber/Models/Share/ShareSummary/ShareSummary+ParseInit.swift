//
//  ShareSummary+ParseInit.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

 extension ShareSummary {
    convenience init(pfShareSummaryObject: PFObject){
        self.init()
        id = pfShareSummaryObject.objectId!
        photoCount = pfShareSummaryObject[ShareSummaryFields.photoCount.rawValue] as! Int
        recipientCount = pfShareSummaryObject[ShareSummaryFields.recipientCount.rawValue] as! Int
        sharedBy = pfShareSummaryObject[ShareSummaryFields.sharedBy.rawValue] as! String
        startProcessDate = pfShareSummaryObject[ShareSummaryFields.startProcessDate.rawValue] as? NSDate
        endProcessDate = pfShareSummaryObject[ShareSummaryFields.endProcessDate.rawValue] as? NSDate
    }
}