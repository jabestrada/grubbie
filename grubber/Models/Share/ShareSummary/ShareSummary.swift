//
//  ShareSummary.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public class ShareSummary {
    var id = ""
    var sharedBy = ""
    var photoCount = 0
    var recipientCount = 0
    var startProcessDate : NSDate?
    var endProcessDate : NSDate?
    
    static func insert(newValuesAssignmentBlock: (ShareSummary) -> Void) -> ShareSummary {
        let insertObjectValuesAssignmentBlock = {(pfShareSummary: PFObject) -> Void
            in
            let shareSummary = ShareSummary()
            newValuesAssignmentBlock(shareSummary)
            pfShareSummary.setValue(NSUUID().UUIDString, forKey: ShareSummaryFields.id.rawValue)
            pfShareSummary.setValue(shareSummary.sharedBy, forKey: ShareSummaryFields.sharedBy.rawValue)
            pfShareSummary.setValue(shareSummary.startProcessDate, forKey: ShareSummaryFields.startProcessDate.rawValue)
            pfShareSummary.setValue(shareSummary.photoCount, forKey: ShareSummaryFields.photoCount.rawValue)
            pfShareSummary.setValue(shareSummary.recipientCount, forKey: ShareSummaryFields.recipientCount.rawValue)
        }
        
        let pfNewShareSummary =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return ShareSummary(pfShareSummaryObject: pfNewShareSummary)
    }
    
    static func onFinish(shareSummaryId: String, completion: (((success: Bool, error: NSError?) -> Void)?)) {
        let pfShareSummary = PFObject(withoutDataWithClassName: self.dataSourceEntityName, objectId: shareSummaryId)
        pfShareSummary.setValue(NSDate(), forKey: ShareSummaryFields.endProcessDate.rawValue)
        pfShareSummary.saveInBackgroundWithBlock { (success, error) -> Void in
            if let completion = completion {
                completion(success: success, error: error)
            }
        }
    }
    
    static func getShareSummariesOfUser(userId: String, completion: (shareSummaries: [ShareSummary]?, error: NSError?) -> Void ){
        let userPredicate = Predicate(lhs: ShareSummaryFields.sharedBy.rawValue, op: .Equals, rhs: userId)
        var shareSummaries = [ShareSummary]()
        ThreadingUtil.inBackground({
                shareSummaries =  self.all(userPredicate, limit: nil, sortDescriptors: [NSSortDescriptor(key: ShareSummaryFields.startProcessDate.rawValue, ascending: false)])
            }) {
                completion(shareSummaries: shareSummaries, error: nil)
        }

    }
    
}
