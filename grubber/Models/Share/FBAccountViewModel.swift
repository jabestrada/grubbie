//
//  FbAccountViewModel.swift
//  Grubbie
//
//  Created by JABE on 12/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


struct FBAccountViewModel {
    var fbAccount : AppUser
    var isSelected: Bool = false
    
    init(fbAccount: AppUser){
        self.fbAccount = fbAccount
    }
}

enum ShareType : Int {
    case Unknown = -1
    case GrubbieShare = 1
    case AdhocShare
}


class ShareSessionInfo {
    var sharedBy : String
    var fbRecipients : [AppUser]
    var grubbieRecipients : [AppUser]
//    var photos : [PhotoSource]
    var photos: [SharePhoto]
    var shareType : ShareType
    
    init(){
        sharedBy = Util.getCurrentUserId()
        fbRecipients = [AppUser]()
        grubbieRecipients = [AppUser]()
//        photos = [PhotoSource]()
        photos = [SharePhoto]()
        shareType = .Unknown
    }
    
}