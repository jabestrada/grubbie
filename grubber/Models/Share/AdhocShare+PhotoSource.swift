//
//  AdhocShare+PhotoSource.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

extension AdhocShare : PhotoSource {
    func isNew() -> Bool {
        return self.id == ""
    }
    
    func getRestaurantName() -> String {
        return ""
    }
    
    func getId() -> String {
        return self.id
    }
    func getComments() -> String {
        return self.comments
    }
    
    func getPhotoType() -> PhotoSourceType {
        return .Adhoc
    }
    
    func getPhoto() -> UIImage {
        return self.photo
    }
    func getPhotoPfFile() -> PFFile? {
        return self.photoPfFile
    }
    
    func getPhotoSourceInfoInBackground(completion: (photoSourceInfo: PhotoSourceInfo?, error: NSError?) -> Void) {
        completion(photoSourceInfo: PhotoSourceInfo(restaurantName: "Ad hoc", townCity: "Ad hoc", country: "Adhoc"), error: nil)

    }
    
    func getPhotoInBackground(completion: (image: UIImage?, error: NSError?) -> Void) {
        if let imageFile = self.photoPfFile {
            imageFile.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil {
                    self.photo = UIImage(data: data!)
                    completion(image: self.photo, error: error)
                } else {
                    completion(image: nil, error: error)
                }
            })
            
        }
    }
    
    func getAudio() -> NSData? {
        if self.audio.length > 0 {
            return self.audio
        }
        return NSData?.None
    }
}