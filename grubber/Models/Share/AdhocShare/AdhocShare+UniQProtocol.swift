//
//  AdhocShare+UniQProtocol.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//


extension AdhocShare : UniQProtocol {
    public typealias T = AdhocShare
    public static var dataSourceEntityName: String {
        get {
            return "AdhocShare"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}
