//
//  AdhocShare+ParseInit.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

extension AdhocShare {
    convenience init(pfAdhocShareObject: PFObject){
        self.init()
        id = pfAdhocShareObject.objectId!
        shareSummaryId = pfAdhocShareObject[AdhocShareFields.shareSummaryId.rawValue] as! String
        comments = pfAdhocShareObject[AdhocShareFields.comments.rawValue] as! String
        photoPfFile = pfAdhocShareObject[AdhocShareFields.photo.rawValue] as? PFFile
        if let audioFile = pfAdhocShareObject[AdhocShareFields.audio.rawValue] as? PFFile {
            do {
                audio = try audioFile.getData()
            }
            catch let e as NSError {
                print("AdhocShare: Error retrieving audio file. Details: \(e.localizedDescription)")
            }
        }
    }
}