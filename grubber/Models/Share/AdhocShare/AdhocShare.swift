//
//  AdhocShare.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//



public class AdhocShare : NSObject {
    var id = ""
    var shareSummaryId = ""
    var comments = "" {
        didSet{
            commentsCanonical = Util.canonicalize(comments)
        }
    }
    var commentsCanonical = ""
    var photo : UIImage!
    var photoPfFile : PFFile?
    var audio = NSData()
    var photoCompressionQuality = CGFloat(0.5)
    
    
    
    static func getById(adhocShareId: String) -> AdhocShare? {
        return AdhocShare.firstOrDefault(Predicate(lhs: AdhocShareFields.id.rawValue, op: .Equals, rhs: adhocShareId), sortDescriptors: nil)
    }

    static func remove(adhocShareId: String, completion: ((success: Bool, error: NSError?) -> Void)?) {
        let pfShare = PFObject(withoutDataWithClassName: AdhocShare.dataSourceEntityName, objectId: adhocShareId)
        pfShare.deleteInBackgroundWithBlock { (success, error) -> Void in
            if let completion = completion {
                completion(success: success, error: error)
            }
        }
    }
    
    static func insert(newValuesAssignmentBlock: (AdhocShare) -> Void) -> AdhocShare {
        let insertObjectValuesAssignmentBlock = {(pfAdhocShare: PFObject) -> Void
            in
            let adhocShare = AdhocShare()
            newValuesAssignmentBlock(adhocShare)
            
            pfAdhocShare.setValue(NSUUID().UUIDString, forKey: AdhocShareFields.id.rawValue)
            
            pfAdhocShare.setValue(adhocShare.shareSummaryId, forKey: AdhocShareFields.shareSummaryId.rawValue)
            pfAdhocShare.setValue(adhocShare.comments, forKey: AdhocShareFields.comments.rawValue)
            pfAdhocShare.setValue(adhocShare.commentsCanonical, forKey: AdhocShareFields.commentsCanonical.rawValue)
            
            let photoRawData = UIImageJPEGRepresentation(adhocShare.photo, adhocShare.photoCompressionQuality)!
            print("Photo size: \(photoRawData.length)")
            let photoData = PFFile(data: NSData(data: photoRawData))!
            
            pfAdhocShare.setObject(photoData, forKey: AdhocShareFields.photo.rawValue)
            
            if adhocShare.audio.length > 0 {
                let audioData = PFFile(data: adhocShare.audio)
                pfAdhocShare.setObject(audioData!, forKey: AdhocShareFields.audio.rawValue)
            }
        }
        
        
        let pfNewAdhocShare =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return AdhocShare(pfAdhocShareObject: pfNewAdhocShare)
    }
}
