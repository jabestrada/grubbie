//
//  AdhocShareFields.swift
//  Grubbie
//
//  Created by Julius Estrada on 18/12/2015.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

public enum AdhocShareFields : String {
    case id = "id"
    case shareSummaryId = "shareSummaryId"
    case comments = "comments"
    case commentsCanonical = "commentsCanonical"
    case photo = "photo"
    case audio = "audio"
}