//
//  ShareNotification+ParseInit.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public extension ShareNotification {
    convenience init(pfShareNotifObject: PFObject){
        self.init()
        id = pfShareNotifObject.objectId!
        shareSummaryRefId = pfShareNotifObject[ShareNotificationFields.shareSummaryRefId.rawValue] as! String
        sharedBy = pfShareNotifObject[ShareNotificationFields.sharedBy.rawValue] as! String
        sharedTo = pfShareNotifObject[ShareNotificationFields.sharedTo.rawValue] as! String
        shareType = SharePhotoType(rawValue: pfShareNotifObject[ShareNotificationFields.shareType.rawValue] as! Int)!
        shareRefId = pfShareNotifObject[ShareNotificationFields.shareRefId.rawValue] as! String
        status =  ShareStatus(rawValue: pfShareNotifObject[ShareNotificationFields.status.rawValue] as! Int)!
        sentDate = pfShareNotifObject[ShareNotificationFields.sentDate.rawValue] as! NSDate
        viewedDate = pfShareNotifObject[ShareNotificationFields.viewedDate.rawValue] as? NSDate
        acceptedRejectedDate = pfShareNotifObject[ShareNotificationFields.acceptedRejectedDate.rawValue] as? NSDate
        if let failReason = pfShareNotifObject[ShareNotificationFields.failReason.rawValue] as? String {
            self.failReason = failReason
        }
        else {
            self.failReason = ""
        }
    }
}
