//
//  ShareNotificationFields.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



enum ShareNotificationFields : String {
    case id = "id"
    case shareSummaryRefId = "shareSummaryRefId"
    case sharedBy = "sharedBy"
    case sharedTo = "sharedTo"
    case shareType = "shareType"
    case shareRefId = "shareRefId"
    case status = "status"
    case sentDate = "sentDate"
    case viewedDate = "viewedDate"
    case acceptedRejectedDate = "acceptedRejectedDate"
    case failReason = "failReason"
}
