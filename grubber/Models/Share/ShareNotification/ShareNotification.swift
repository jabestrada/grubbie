//
//  ShareNotification.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

enum SharePhotoType : Int {
    case Unknown = 0
    case RestaurantPhoto = 1
    case ExperiencePhoto = 2
    case AdHoc = 3
    
}

enum ShareStatus : Int {
    case Sent = 0
    case Viewed
    case Accepted
    case Rejected
    case Failed
}

class ShareNotificationViewModel {
    var sharerFirstName : String = ""
    var sharerLastName: String = ""
    var photoSourceInfo : PhotoSourceInfo?
    var comments : String = ""
    var audioData : NSData?
    var photoData : UIImage?
}


public class ShareNotification  {
    var id = ""
    var sharedBy = ""
    var sharedTo = ""
    var shareType = SharePhotoType.Unknown
    var shareRefId = ""
    var shareSummaryRefId = ""
    var status = ShareStatus.Sent
    var sentDate = NSDate()
    var viewedDate : NSDate?
    var acceptedRejectedDate : NSDate?
    var failReason = ""
    
    
    
    static func reject(shareNotification: ShareNotification, completion: ((success: Bool, error: NSError?) -> Void)?){
        ShareNotification.setStatus(shareNotification.id, status: .Rejected, statusText: "", completion: { (success, error) -> Void in
            if let completion = completion {
                completion(success: success, error: error)
            }
        })


    }
    
    static func accept(shareNotification: ShareNotification, completion: ((success: Bool, newPhoto: PhotoSource?, error: NSError?) -> Void)?){
                switch shareNotification.shareType {
                case .ExperiencePhoto:
                    ExperiencePhoto.insertFromShareNotification(shareNotification, completion: { (success, newRestaurantPhoto, error) -> Void in
                        if success {
                            ShareNotification.setStatus(shareNotification.id, status: .Accepted, statusText: "", completion: { (success, error) -> Void in
                                completion?(success: success, newPhoto: newRestaurantPhoto, error: error)
                            })
                        } else {
                            completion?(success: success, newPhoto: newRestaurantPhoto, error: error)
                        }
                    })
                    break;
                case .RestaurantPhoto:
                    RestaurantPhoto.insertFromShareNotification(shareNotification, completion: { (success, newPhoto, error) -> Void in
                        if success {
                            ShareNotification.setStatus(shareNotification.id, status: .Accepted, statusText: "", completion: { (success, error) -> Void in
                                completion?(success: success, newPhoto: newPhoto, error: error)
                            })
                        } else {
                            completion?(success: success, newPhoto: newPhoto, error: error)
                        }
                    })
                    break;
                default:
                    assertionFailure("ShareNotification share type of \(shareNotification.shareType) not yet supported")
                    break;
                }
        
    }
    
    
    static func setStatus(shareNotifId: String, status: ShareStatus, statusText: String, completion: (success: Bool, error: NSError?) -> Void){
        dataAdapter.update(self.dataSourceEntityName, predicate: Predicate(lhs: ShareNotificationFields.id.rawValue, op: .Equals, rhs: shareNotifId), updateOnlyOne: true) { (shareNotif : PFObject) -> Void in
            shareNotif.setValue(status.rawValue, forKey: ShareNotificationFields.status.rawValue)
            if status == ShareStatus.Accepted || status == ShareStatus.Rejected {
                shareNotif.setValue(NSDate(), forKey: ShareNotificationFields.acceptedRejectedDate.rawValue)
            }
            if status == ShareStatus.Failed {
                shareNotif.setValue(statusText, forKey: ShareNotificationFields.failReason.rawValue)
            }
        }
        completion(success: true, error: nil)
    }
    
    
    static func insert(newValuesAssignmentBlock: (ShareNotification) -> Void) -> ShareNotification {
        let insertObjectValuesAssignmentBlock = {(pfShareNotif: PFObject) -> Void
            in
            let shareNotif = ShareNotification()
            newValuesAssignmentBlock(shareNotif)
            pfShareNotif.setValue(NSUUID().UUIDString, forKey: ShareNotificationFields.id.rawValue)
            pfShareNotif.setValue(shareNotif.sharedBy, forKey: ShareNotificationFields.sharedBy.rawValue)
            pfShareNotif.setValue(shareNotif.shareSummaryRefId, forKey: ShareNotificationFields.shareSummaryRefId.rawValue)
            pfShareNotif.setValue(shareNotif.sharedTo, forKey: ShareNotificationFields.sharedTo.rawValue)
            pfShareNotif.setValue(shareNotif.shareType.rawValue, forKey: ShareNotificationFields.shareType.rawValue)
            pfShareNotif.setValue(shareNotif.shareRefId, forKey: ShareNotificationFields.shareRefId.rawValue)
            pfShareNotif.setValue(ShareStatus.Sent.rawValue, forKey: ShareNotificationFields.status.rawValue)
            pfShareNotif.setValue(NSDate(), forKey: ShareNotificationFields.sentDate.rawValue)
            pfShareNotif.setValue("", forKey: ShareNotificationFields.failReason.rawValue)
        }
        
        let pfShareNotification =  dataAdapter.insert(self.dataSourceEntityName, valuesAssignmentBlock: insertObjectValuesAssignmentBlock)
        return ShareNotification(pfShareNotifObject: pfShareNotification)
    }
    
    static func getShareNotificationsOfUser(userId: String, completion: ((success: Bool, shareNotifications: [ShareNotification], error: NSError?) -> Void)?){
        let notifications = self.all(Predicate(lhs: ShareNotificationFields.sharedBy.rawValue, op: .Equals, rhs: userId), limit: nil, sortDescriptors: nil)
        if let completion = completion {
            completion(success: true, shareNotifications: notifications, error: nil)
        }
    }
    
    
    static func getShareNotificationsForUser(userId: String, completion: (notifications: [ShareNotification], error: NSError?) -> Void){
        var notifs = [ShareNotification]()
        ThreadingUtil.inBackground({ () -> Void in
            let predicate = Predicate(lhs: ShareNotificationFields.sharedTo.rawValue, op: .Equals, rhs: userId)
            let statusPredicate = Predicate(lhs: ShareNotificationFields.status.rawValue, op: PredicateOperator.LessThan, rhs:ShareStatus.Accepted.rawValue)
            let predicateGroup = PredicateGroup(predicate1: predicate, predicate2: statusPredicate)
            notifs = all(predicateGroup, limit: nil, sortDescriptors: [NSSortDescriptor(key: ShareNotificationFields.sentDate.rawValue, ascending: false)])
            }) { () -> Void in
                completion(notifications: notifs, error: nil)
        }
    
    }
    

    
    static func getShareNotifViewModel(notif: ShareNotification, completion: (shareNotifViewModel: ShareNotificationViewModel?, error: NSError?) -> Void){
        let shareNotifViewModel = ShareNotificationViewModel()
        var user : AppUser?
        ThreadingUtil.inBackground({
            user = AppUser.getUserById(notif.sharedBy)
            var photo : PhotoSource?
            if notif.shareType == SharePhotoType.ExperiencePhoto {
                photo = ExperiencePhoto.getById(notif.shareRefId)
            } else if notif.shareType == SharePhotoType.RestaurantPhoto {
                photo = RestaurantPhoto.getById(notif.shareRefId)
            } else if notif.shareType == SharePhotoType.AdHoc {
                photo = AdhocShare.getById(notif.shareRefId)
            }
            if let photo = photo {
                shareNotifViewModel.audioData =  photo.getAudio()
                shareNotifViewModel.comments = photo.getComments()
                photo.getPhotoInBackground({ (image, error) -> Void in
                    shareNotifViewModel.photoData = image
                    if let user = user {
                        if user.fbFirstName.isEmpty {
                            shareNotifViewModel.sharerFirstName = user.username
                        }
                        else {
                            shareNotifViewModel.sharerFirstName = user.fbFirstName
                        }
                        if !user.fbLastName.isEmpty {
                            shareNotifViewModel.sharerLastName = user.fbLastName
                        }
                    }
                    photo.getPhotoSourceInfoInBackground({ (photoSourceInfo, error) -> Void in
                        shareNotifViewModel.photoSourceInfo = photoSourceInfo
                        completion(shareNotifViewModel: shareNotifViewModel, error: nil)
                    })
                })
            } else {
                // Photo not found; might have been deleted by sharer.
                let failReason = "Cannot resolve referenced photo \(notif.shareRefId) of type \(notif.shareType)"
                ShareNotification.setStatus(notif.id, status: .Failed, statusText: failReason, completion: { (success, error) -> Void in
                    completion(shareNotifViewModel: ShareNotificationViewModel?.None, error: nil)
                })
            }
            
            
            }) {
                // ...
        }
    }
}
