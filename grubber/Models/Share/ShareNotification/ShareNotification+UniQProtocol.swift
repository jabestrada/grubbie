//
//  ShareNotification+UniQProtocol.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

extension ShareNotification : UniQProtocol {
    public typealias T = ShareNotification
    public static var dataSourceEntityName: String {
        get {
            return "ShareNotification"
        }
    }
    
    public static var dataAdapter : DataAdapterProtocol {
        get{
            return Factory.createDataAdapter()
        }
    }
}