//
//  RestaurantListingCell.swift
//  grubber
//
//  Created by JABE on 11/18/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation


protocol RestaurantListingCellDelegate {
    func didPressPhoneNumberButton(sender: AnyObject, rawPhoneNumber: String)
}


class RestaurantListingCell : UITableViewCell {


    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var restaurantNameLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var phoneButton: UIButton!
    
    var delegate : RestaurantListingCellDelegate?
    

    func configureCell(restaurant: Restaurant, location: CLLocation?, delegate: RestaurantListingCellDelegate){
        self.restaurantNameLabel?.text = restaurant.name

//        self.textLabel?.text = restaurant.name
        self.delegate = delegate
        
        if let location = location {
            self.distanceLabel?.text = location.textForDistanceFrom(CLLocation(latitude: restaurant.lat, longitude: restaurant.lng))
        }
        if restaurant.phone.isEmpty {
            phoneNumberLabel?.hidden = true
            phoneButton?.hidden = true
        } else {
            phoneNumberLabel?.hidden = false
            phoneButton?.hidden = false
            phoneNumberLabel.text = restaurant.phone
        }
//        phoneNumberButton.setTitle(restaurant.phone, forState: .Normal)
//        phoneNumberButton.sizeToFit()
    }

    @IBAction func phoneButtonPressed(sender: AnyObject) {
        guard let delegate = self.delegate,
                rawNumber = phoneNumberLabel.text else {
            return
        }
        delegate.didPressPhoneNumberButton(sender, rawPhoneNumber: rawNumber)
    }
    
//    @IBAction func phoneNumberButtonPressed(sender: AnyObject) {
//        guard let delegate = self.delegate,
//                button = sender as? UIButton,
//                rawNumber = button.titleLabel?.text else {
//            return
//        }
//        delegate.didPressPhoneNumberButton(sender, rawPhoneNumber: rawNumber)
//    }
    
}