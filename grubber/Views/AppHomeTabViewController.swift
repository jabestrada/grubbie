//
//  AppHomeTabViewController.swift
//  myEats
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

 class AppHomeTabViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        AppGlobals.appHomeTabViewController = self
        if !Restaurant.doesUserHaveRestaurant() {
            if let _ = self.viewControllers {
                for view in self.viewControllers! {
                    if let writeReviewVc = view as? ReviewHomeViewController {
                        self.selectedViewController = writeReviewVc
                    }
                }
            }
        }
        
    }
    
    func activateGrubsList(){
        self.selectedIndex = 0
    }
    
    @IBAction func AppHomeTabViewController_unwindToLogin(segue: UIStoryboardSegue){
        Util.closeController(self)
    }
    
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
    
    }
    
    
    func activateWriteReviewTab() {
        if let _ = self.viewControllers {
            for view in self.viewControllers! {
                if let writeReviewVc = view as? ReviewHomeViewController {
                    self.selectedViewController = writeReviewVc
                }
            }
        }

    }
}

