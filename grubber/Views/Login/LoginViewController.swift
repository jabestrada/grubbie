//
//  ViewController.swift
//  grubber
//
//  Created by JABE on 11/12/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, PFLogInViewControllerDelegate {

    @IBOutlet var appLabel: UILabel!
    
    @IBOutlet var retryButton: UIButton!
    var activityIndicator : JLActivityIndicator!
    var netChecker = Factory.createNetConnectivityChecker()
    var fbAdapter = Factory.createFBAdapter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIFactory.configureAppLabel(appLabel)
        retryButton.hidden = true
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: true, drawOverlayOverParentView: false)
        doLogin()
    }
    
    func checkNetConnectivity() -> Bool {
        if !netChecker.isNetworkAvailable() {
            activityIndicator.stopAnimation()
            ThreadingUtil.inBackgroundAfter({ () -> Void in
                Util.showMessage(self, msg: UserMessage.NoInternet.rawValue, title: "", completionBlock: nil)
                }, afterDelay: 0.3)
            retryButton.hidden = false
            return false
        }
        return true
    }

    @IBAction func retryButtonPressed(sender: AnyObject) {
        onRetry()

    }
    
    func onRetry(){
        retryButton.enabled = false
        activityIndicator.startAnimation()
        doLogin()
        retryButton.enabled = true
        activityIndicator.stopAnimation()
    }
    
    @IBAction func logout(segue: UIStoryboardSegue){
        PFUser.logOut()
        onRetry()
    }
    
    
    func doLogin() {
        if !checkNetConnectivity() {
            return
        }
        
        var isAuthenticated = false
        ThreadingUtil.inBackground({
            isAuthenticated = PFUser.currentUser() != nil
            }) {
                if isAuthenticated {
                    self.retryButton.enabled = false
                    self.onPostAuthentication()
                } else {
                    let loginViewController = AppPFLoginViewController()
                    loginViewController.delegate = self
                    //            loginViewController.fields = .UsernameAndPassword | .LogInButton | .PasswordForgotten | .SignUpButton | .Facebook | .Twitter

                    loginViewController.facebookPermissions = [FBPermission.friends.rawValue]
                    
                    loginViewController.fields = [ .UsernameAndPassword, .LogInButton, .PasswordForgotten, .SignUpButton, .Facebook]
                    loginViewController.emailAsUsername = true

                    loginViewController.signUpController = AppPFSignupViewController()
                    loginViewController.signUpController?.emailAsUsername = true
//                    loginViewController.signUpController?.delegate = self
                    self.presentViewController(loginViewController, animated: false, completion: nil)
                }
                
        }
    }
    
    
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
        ThreadingUtil.inBackgroundAfter({ () -> Void in
            self.onPostAuthentication()
            }, afterDelay: 0.3)
        
    }
    
    
    
    func onPostAuthentication() {
        if fbAdapter.isUserLoggedThruFacebook(){
            let currentPfUser = PFUser.currentUser()!
            if let fbId = currentPfUser.objectForKey(AppUserFields.fbId.rawValue) as? String {
                if fbId.isEmpty {
                    self.saveFbDataAndSegueToHome()
                }
                else {
                    self.segueToHome()
                }
            } else {
                self.saveFbDataAndSegueToHome()
            }
        } else {
            self.segueToHome()
        }
    }
    

    func saveFbDataAndSegueToHome(){
        guard let currentPfUser = PFUser.currentUser() else {
            return
        }
        
        self.fbAdapter.getFbUserData(false, completion: { (userData, error) -> Void in
            if error == nil {
                if let fbData = userData {
                    currentPfUser.setObject(fbData.fbId, forKey: AppUserFields.fbId.rawValue)
                    currentPfUser.setObject(fbData.firstName, forKey: AppUserFields.fbFirstName.rawValue)
                    currentPfUser.setObject(fbData.lastName, forKey: AppUserFields.fbLastName.rawValue)
                    currentPfUser.saveInBackgroundWithBlock({ (success, error) -> Void in
                        if error != nil {
                            print("Failed to save FbId: \(error)")
                        } else {
                            self.segueToHome()
                        }
                    })
                }
            } else {
                print("Failed to retrieve and save FbId: \(error)")
            }
        })
    }
    
    
    func segueToHome() {
        if Restaurant.doesUserHaveRestaurant() {
            ThreadingUtil.inBackgroundAfter({ () -> Void in
                self.performSegueWithIdentifier("segueToHome", sender: nil)
                }, afterDelay: 0.3)
        }
        else {
            UIFactory.createWelcomeScreenController(self)
        }
    }
}

