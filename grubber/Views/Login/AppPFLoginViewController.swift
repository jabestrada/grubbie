//
//  AppPFLoginViewController.swift
//  Grubbie
//
//  Created by JABE on 11/27/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class AppPFLoginViewController : PFLogInViewController {
    var backgroundImage : UIImageView!
    var viewsToAnimate: [UIView!]!;
    var viewsFinalYPosition : [CGFloat]!;
    var hasAnimated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundImage = UIFactory.createWelcomeBackgroundImageView()
        self.logInView!.insertSubview(self.backgroundImage, atIndex: 0)
        
        let logo = UIFactory.createWelcomeLabel()
        logInView!.logo = logo
       
        UIFactory.configureLoginButton(logInView?.logInButton)
        logInView?.passwordForgottenButton?.setTitleColor(UIColor.whiteColor(), forState: .Normal)

        UIFactory.configureLoginTextField(logInView?.usernameField)
        UIFactory.configureLoginTextField(logInView?.passwordField)
        
        customizeButton(logInView?.facebookButton!)
        customizeButton(logInView?.signUpButton!)
        
//         viewsToAnimate = [self.logInView?.usernameField, self.logInView?.passwordField, self.logInView?.logInButton, self.logInView?.passwordForgottenButton, self.logInView?.facebookButton, self.logInView?.twitterButton, self.logInView?.signUpButton, self.logInView?.logo]
         viewsToAnimate = [self.logInView?.usernameField, self.logInView?.passwordField, self.logInView?.logInButton, self.logInView?.passwordForgottenButton, self.logInView?.facebookButton, self.logInView?.signUpButton, self.logInView?.logo]
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let loginView = self.logInView else {
            return
        }
        backgroundImage.frame = CGRectMake(0, 0, loginView.frame.width, loginView.frame.height)
        loginView.logo!.sizeToFit()
        let logoFrame = loginView.logo!.frame
        loginView.logo!.frame = CGRectMake(logoFrame.origin.x,
            loginView.usernameField!.frame.origin.y - logoFrame.height - 16,
            loginView.frame.width,
            logoFrame.height)
        
        if !hasAnimated {
            viewsFinalYPosition = [CGFloat]();
            for viewToAnimate in viewsToAnimate {
                let currentFrame = viewToAnimate.frame
                viewsFinalYPosition.append(currentFrame.origin.y)
                viewToAnimate.frame = CGRectMake(currentFrame.origin.x, self.view.frame.height + currentFrame.origin.y, currentFrame.width, currentFrame.height)
            }
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
       super.viewDidAppear(animated)
        if viewsFinalYPosition.count == self.viewsToAnimate.count {
            UIView.animateWithDuration(1, delay: 0.0, options: .CurveEaseInOut,  animations: { () -> Void in
                for viewToAnimate in self.viewsToAnimate {
                    let currentFrame = viewToAnimate.frame
                    viewToAnimate.frame = CGRectMake(currentFrame.origin.x, self.viewsFinalYPosition.removeAtIndex(0), currentFrame.width, currentFrame.height)
                }
                }, completion: {(completed) in
                    self.hasAnimated = true
                })
            
        }
    }
    
    func customizeButton(button: UIButton!) {
        //        button.setBackgroundImage(nil, forState: .Normal)
        //        button.backgroundColor = UIColor.clearColor()
        button.layer.cornerRadius = 5
        //        button.layer.borderWidth = 1
        //        button.layer.borderColor = UIColor.whiteColor().CGColor
    }
}