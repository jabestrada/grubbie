//
//  AppPFSignUpViewController.swift
//  Grubbie
//
//  Created by JABE on 11/27/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

class AppPFSignupViewController : PFSignUpViewController, PFSignUpViewControllerDelegate {
    
    var backgroundImage : UIImageView!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        backgroundImage = UIFactory.createWelcomeBackgroundImageView()
        signUpView!.insertSubview(backgroundImage, atIndex: 0)
        

        UIFactory.configureLoginTextField(signUpView?.usernameField)
        UIFactory.configureLoginTextField(signUpView?.passwordField)
        UIFactory.configureLoginButton(signUpView?.signUpButton)
        
        signUpView?.dismissButton!.setTitle("No, thanks", forState: .Normal)
        signUpView?.dismissButton!.setImage(nil, forState: .Normal)
        
        // remove the parse Logo
        let logo = UIFactory.createWelcomeLabel()
        signUpView?.logo = logo
        self.delegate = self
        self.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal   
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
        Util.showMessage(self, msg: "Successfully signed up!", title: "") { (alertAction) -> Void in
            Util.closeController(self)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // stretch background image to fill screen
        backgroundImage.frame = CGRectMake( 0,  0,  signUpView!.frame.width,  signUpView!.frame.height)
        
        // position logo at top with larger frame
        signUpView!.logo!.sizeToFit()
        let logoFrame = signUpView!.logo!.frame
        signUpView!.logo!.frame = CGRectMake(logoFrame.origin.x, signUpView!.usernameField!.frame.origin.y - logoFrame.height - 16, signUpView!.frame.width,  logoFrame.height)
        
        let dismissButtonFrame = signUpView!.dismissButton!.frame
        signUpView?.dismissButton!.frame = CGRectMake(0, signUpView!.signUpButton!.frame.origin.y + signUpView!.signUpButton!.frame.height + 16.0,  signUpView!.frame.width,  dismissButtonFrame.height)
    }
    
}