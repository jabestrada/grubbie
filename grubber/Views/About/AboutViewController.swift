//
//  AboutViewController.swift
//  myEats
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

 class AboutViewController: UIViewController {

    @IBOutlet weak var appVersionLabel: UILabel!

    
    override func viewDidAppear(animated: Bool) {
        let version = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String
        let build = NSBundle.mainBundle().infoDictionary!["CFBundleVersion"] as! String
        appVersionLabel.text = "Version \(version) build \(build)"
        appVersionLabel.hidden = false
    }
    
    @IBAction func urlButtonPressed(sender: AnyObject) {
        if let button = sender as? UIButton {
            if let url = NSURL(string: "http://\((button.titleLabel?.text)!)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
  }

