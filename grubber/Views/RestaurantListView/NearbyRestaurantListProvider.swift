//
//  NearbyRestaurantListProvider.swift
//  Grubbie
//
//  Created by JABE on 11/30/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

class NearbyRestaurantListProvider : RestaurantListProvider {
    var configProvider = Factory.createConfigProvider()
    
    func getRestaurants(nameFilter: String, completion: (restaurants: [Restaurant]) -> Void) {
        AppGlobals.sharedLocation { (location) -> Void in
            Restaurant.getNearbyRestaurants(location, nameFilter: nameFilter, completionBlock: { (restaurants) -> Void in
                completion(restaurants: restaurants)
            })
        }
    }
    
    func getInitialSearchTerm() -> String {
        return configProvider.nearbyEatsSearchTermFilter
    }
    
    func onSearchTermChanged(newTerm: String) {
        configProvider.nearbyEatsSearchTermFilter = newTerm
    }
}

class RestaurantsOfUserListProvider : RestaurantListProvider {
    var configProvider = Factory.createConfigProvider()
    
    func getRestaurants(nameFilter: String, completion: (restaurants: [Restaurant]) -> Void) {
            // TODO: Need to add starting restaurant name filter here...
        Restaurant.getRestaurantsOfUser(Util.getCurrentUserId(), contains: nameFilter, locationPredicateValue: nil,
                completionBlock: {(restaurants) in
                    completion(restaurants: restaurants)
            })

    }
    
    func getInitialSearchTerm() -> String {
        return configProvider.nearbyEatsSearchTermFilter
    }
    
    func onSearchTermChanged(newTerm: String) {
        configProvider.nearbyEatsSearchTermFilter = newTerm
    }
}