//
//  RestaurantListViewController.swift
//  Grubbie
//
//  Created by JABE on 11/30/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation


class RestaurantListViewController : UIViewController{

    @IBOutlet var tableView: UITableView!
    @IBOutlet var filterTextField: UITextField!
    
    @IBOutlet var noResultsView: UIView!
    var restaurantListProvider : RestaurantListProvider!
    var restaurants = [Restaurant]()
    var delegate : RestaurantListViewControllerDelegate?
    var location : CLLocation?
    var activityIndicator : JLActivityIndicator!

    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    override func viewDidLoad() {
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: tableView)
        tableView.dataSource = self
        tableView.delegate = self
        AppGlobals.sharedLocation { (location) -> Void in
            self.filterTextField.text = self.restaurantListProvider.getInitialSearchTerm()
            self.location = location
            self.loadData()
        }
      Util.setNoResultsViewMessage(self.noResultsView, message: "There are no restaurants\nmatching the given filter.")
    }
    
    func loadData(){
        if let listProvider = self.restaurantListProvider {
            activityIndicator.startAnimation()
            listProvider.getRestaurants(self.filterTextField.text!, completion: { (restaurants) -> Void in
                self.restaurants = restaurants
                if restaurants.count == 0 {
                    self.noResultsView.hidden = false
                    self.tableView.hidden = true
                } else {
                    self.noResultsView.hidden = true
                    self.tableView.hidden = false
                    self.restaurants.sortInPlace({ (x, y) -> Bool in
                        Restaurant.sortByDistance(self.location!, restaurant1: x, restaurant2: y)
                    })
                    self.tableView.reloadData()
                }
                self.activityIndicator.stopAnimation()
            })
        }
    }
    
    @IBAction func searchPressed(sender: AnyObject) {
        self.hideKeyboard(sender)
        restaurantListProvider.onSearchTermChanged(filterTextField.text!)
        loadData()
    }

    
}

extension RestaurantListViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        let restaurant = self.restaurants[indexPath.row]
        cell.textLabel?.text = restaurant.name
        if let location = self.location {
            cell.detailTextLabel?.text = location.textForDistanceFrom(CLLocation(latitude: restaurant.lat, longitude: restaurant.lng))
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let delegate = self.delegate {
            let restaurant = self.restaurants[indexPath.row]
            Util.closeController(self)
            delegate.didSelectRestaurant(restaurant, userData: nil)
        }
    }
}