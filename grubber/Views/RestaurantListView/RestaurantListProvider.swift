//
//  RestaurantListProvider.swift
//  Grubbie
//
//  Created by JABE on 11/30/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

protocol RestaurantListProvider {
    func getRestaurants(nameFilter: String, completion: (restaurants: [Restaurant]) -> Void)
    func getInitialSearchTerm() -> String
    func onSearchTermChanged(newTerm: String)
}