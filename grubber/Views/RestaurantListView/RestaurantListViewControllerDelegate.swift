//
//  RestaurantListViewControllerDelegate.swift
//  Grubbie
//
//  Created by JABE on 11/30/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

protocol RestaurantListViewControllerDelegate {
    func didSelectRestaurant(restaurant: Restaurant, userData: AnyObject?)
}