//
//  SettingsViewController.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit


enum CellCommandType {
        case Normal
        case Caution
}

struct JLTableCellCommand{
    var mainText : String
    var detailText : String?
    var cellAccessoryType : UITableViewCellAccessoryType?
    var commandType : CellCommandType
    var invokeCommand : (UIViewController) -> Void
}


class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    var activityIndicator : JLActivityIndicator!
    
    var commands = [
        JLTableCellCommand(mainText: "About", detailText: "General app info and artwork acknowledgements", cellAccessoryType: .DisclosureIndicator, commandType: .Normal, invokeCommand: { (settingsVc) -> Void in
            if let view = settingsVc as? SettingsViewController {
                UIFactory.showStoryboard(settingsVc, storyboardId: StoryboardId.About, configureBlock: nil)
            }
        })
        ,
        JLTableCellCommand(mainText: "Account", detailText: "Displays details of your Grubbie login", cellAccessoryType: .DisclosureIndicator, commandType: .Normal) { (settingsVc) -> Void in
        UIFactory.showStoryboard(settingsVc, storyboardId: StoryboardId.AccountSettings, configureBlock: { (uiViewController) -> Void in
         })
    }
        ,
        JLTableCellCommand(mainText: "Log out", detailText: "Logs out your current account from Grubbie", cellAccessoryType: UITableViewCellAccessoryType?.None, commandType: .Normal, invokeCommand: { (settingsVc) -> Void in
            if let view = settingsVc as? SettingsViewController {
                Util.getConfirmation(view, message: "Are you sure you want to log out?", title: "", yesCaption: "Yup, log me out", noCaption: "Nope, I changed my mind", onYesDelegate: { (alertAction) -> Void in
                    view.performSegueWithIdentifier("toLogoutSegue", sender: nil)
                })
            }
        })
        ,
        JLTableCellCommand(mainText: "Delete this account",
                            detailText: "Removes this account and associated data from Grubbie",
                            cellAccessoryType: UITableViewCellAccessoryType?.None,
                            commandType: .Caution,
            invokeCommand: { (settingsVc) -> Void in
            if let view = settingsVc as? SettingsViewController {
                Util.getConfirmation(view, message: "Are you sure you want to delete this account? All data created through this account (including, but not limited to, reviews and photos) will be erased as well. This action cannot be undone.", title: "", yesCaption: "Yup, delete this account. Thanks for everything.", noCaption: "Nope, I want Grubbie back.", onYesDelegate: { (alertAction) -> Void in
                    view.activityIndicator.startAnimation()
                    AppUser.remove(Util.getCurrentUserId(), completion: { (success, error) -> Void in
                        view.activityIndicator.stopAnimation()
                        if success {
                            Util.showMessage(settingsVc, msg: "Successfully deleted this user account. Thank you for using Grubbie.", title: "", completionBlock: { (alertAction) -> Void in
                                view.performSegueWithIdentifier("toLogoutSegue", sender: nil)
                            })
                        } else {
                            Util.showMessage(settingsVc, msg: "There seemed to be a problem deleting this user. Please try again later.", title: UserDialogTitle.Error.rawValue, completionBlock: nil)
                        }
                        
                    })
                })
            }
        })
        
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: false)
        tableView.dataSource = self
        tableView.delegate = self
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commands.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let command = commands[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        cell.textLabel?.text = command.mainText
        if command.commandType == .Caution {
            cell.textLabel?.textColor = UIColor.redColor()
        }
        if let detailText = command.detailText {
            cell.detailTextLabel?.text = detailText
        }
        if let cellAccessoryType = command.cellAccessoryType {
            cell.accessoryType = cellAccessoryType
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let command = commands[indexPath.row]
        command.invokeCommand(self)

    }
}

