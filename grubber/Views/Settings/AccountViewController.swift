//
//  AccountViewController.swift
//  Grubbie
//
//  Created by JABE on 12/2/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {


    @IBOutlet var profPic: UIImageView!
    @IBOutlet var accountNameLabel: UILabel!
    @IBOutlet var loggedFromLabel: UILabel!
    @IBOutlet var syncProfPicButton: UIButton!
    
    @IBOutlet weak var grubbieProfPic: UIImageView!
    @IBOutlet weak var grubbieProfPicLabel: UILabel!
    
    var activityIndicator : JLActivityIndicator!
    var fbAdapter = Factory.createFBAdapter()
    
    override func viewDidLoad() {
        JLThemes.apply(profPic, grubbieProfPic)
        self.loggedFromLabel.hidden = true
        self.accountNameLabel.hidden = true
        
        super.viewDidLoad()
        self.activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: true)
        var fbUser = false
        ThreadingUtil.inBackground({
                fbUser = self.fbAdapter.isUserLoggedThruFacebook()
            }) {
                self.loggedFromLabel.hidden = false
                if fbUser {
                    self.loggedFromLabel.text = "You are logged in via Facebook"
                    self.profPic.hidden = false
                    self.grubbieProfPic.hidden = false
                    self.grubbieProfPicLabel.hidden = false
                    self.fbAdapter.getFbUserData(true, completion: { (userData, error) -> Void in
                        self.accountNameLabel.hidden = false
                        if let user = userData {
                            self.accountNameLabel.text = user.fullName
                            let fbProfPicUrl = NSURL(string: user.profilePicUrl)!
                            if let data = NSData(contentsOfURL: fbProfPicUrl) {
                                self.profPic.image = UIImage(data: data)
                                self.syncProfPicButton.hidden = false
                            }
                            if let currentGrubbieProfPic = AppUser.getProfPicImage(user.fbId) {
                                self.grubbieProfPic.image = currentGrubbieProfPic
                            }
                            else {
                                self.grubbieProfPic.hidden = true
                                self.grubbieProfPicLabel.text = "No current profile pic in Grubbie"
                            }
                        } else {
                            self.accountNameLabel.text = "Failed to get name from Facebook"
                        }
                        self.activityIndicator.stopAnimation()
                    })
                
                }
                else {
                    self.loggedFromLabel.text = "You are logged in via Grubbie account"
                    self.profPic.hidden = true
                    self.accountNameLabel.hidden = false
                    self.grubbieProfPic.hidden = true
                    self.grubbieProfPicLabel.hidden = true
                    self.accountNameLabel.text = PFUser.currentUser()!.email
                    self.activityIndicator.stopAnimation()
                }
        }
    }
    
    @IBAction func syncProfilePicPressed(sender: AnyObject) {
        self.activityIndicator.startAnimation()
        if let profPic = self.profPic.image {
            AppUser.updateProfPic(profPic) { (success, error) -> Void in
                self.activityIndicator.stopAnimation()
                if success {
                    self.grubbieProfPic.hidden = false
                    self.grubbieProfPic.image = profPic
                    Util.showMessage(self, msg: "Your Grubbie profile pic has been updated with your latest Facebook profile pic.", title: "")
                }
                else {
                    Util.showMessage(self, msg: "Something went wrong while trying to update your Grubbie profile pic. Please try again later.", title: UserDialogTitle.Error.rawValue)
                    print(error)
                }
                
            }
        }
        
    }

    func updateFbProfPic(){
        guard let currentPfUser = PFUser.currentUser() else {
            return
        }
        if let lastProfPicPull = currentPfUser.objectForKey("fbProfilePictureLastPull"){
            print("lastProfPicPull: \(lastProfPicPull)")
        }
        
        ThreadingUtil.inBackground({ () -> Void in
            print("Starting profile pic update...")
            self.fbAdapter.getProfilePicUrl({ (picUrl, error) -> Void in
                let fbProfPicUrl = NSURL(string: picUrl)!
                if let data = NSData(contentsOfURL: fbProfPicUrl){
                    let photoFile = PFFile(data: data)!
                    currentPfUser.setObject(photoFile, forKey: "fbProfilePicture")
                    currentPfUser.setValue(NSDate(), forKey: "fbProfilePictureLastPull")
                    currentPfUser.saveInBackgroundWithBlock({ (success, error) -> Void in
                        print("Profile pic updated done")
                    })
                }
            })
            }, callBack: nil)
    }
    
    
//    @IBAction func logoutButtonPressed(sender: AnyObject) {
//        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: .ActionSheet)
//        self.activityIndicator.startAnimation()
//        let cancelAction = UIAlertAction(title: "Nope, I changed my mind", style: .Cancel, handler: {action -> Void in
//            self.activityIndicator.stopAnimation()
//        })
//        
//        actionSheetController.addAction(cancelAction)
//        
//        
//        let logoutAction = UIAlertAction(title: "Yup, log me out", style: .Default) { action -> Void in
//            self.performSegueWithIdentifier("toLogoutSegue", sender: nil)
//        }
//        
//        actionSheetController.addAction(logoutAction)
//
//        self.presentViewController(actionSheetController, animated: true, completion: nil)
//    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }

}
