//
//  SharingViewController.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



class SharingViewController: UIViewController {
    var shareSessionInfo : ShareSessionInfo!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var percentDoneLabel: UILabel!
    @IBOutlet var cancelButton: UIButton!
    
    var totalTasks = 0
    var sharer : Sharer!
    var cancelPressed = false
    

    
    override func viewDidAppear(animated: Bool){
        progressView.progress = 0.0
        activityIndicator.startAnimating()
        
        self.sharer = Sharer(shareSessionInfo: shareSessionInfo)
        totalTasks = self.sharer.totalTaskCount
        sharer.delegate = self
        sharer.start()
    }
    
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.cancelPressed = true
    }
}

extension SharingViewController : SharerDelegate {
    func didCancel(currentProcessed: Int) {
        ThreadingUtil.inMainQueue {
            self.activityIndicator.stopAnimating()
            self.cancelButton.hidden = true
            Util.showMessage(self, msg: "Share canceled after processing \(currentProcessed) share(s)", title: "", completionBlock: { (action) -> Void in
                Util.closeController(self)
            })
        }
    }
    
    func didFinish(totalProcessed: Int) {
        ThreadingUtil.inMainQueue {
            self.activityIndicator.stopAnimating()
            self.cancelButton.hidden = true
            Util.showMessage(self, msg: "Sharing completed successfully", title: "", completionBlock: { (action) -> Void in

                self.performSegueWithIdentifier("toHomeSegue", sender: nil)
            })
            EventPublisher.sharedAppEventPublisher().emit(.ShareSummaryAdded, userData: self.sharer.shareSummary.id)
        }
    }
    
    func didMakeProgress(currentProcessed: Int) {
        ThreadingUtil.inMainQueue {
            self.progressView.progress = Float(currentProcessed) / Float(self.totalTasks)
            self.percentDoneLabel.text = "\(Int(self.progressView.progress * 100)) %"
        }
    }
    
    func onQueryForCancel(currentProcessed: Int) -> Bool {
        return self.cancelPressed
    }

}
    

protocol SharerDelegate {
    func didMakeProgress(currentProcessed: Int)
    func didFinish(totalProcessed: Int)
    func didCancel(currentProcessed: Int)
    func onQueryForCancel(currentProcessed: Int) -> Bool
}


class Sharer {
    var totalTaskCount = 0
    var shareSessionInfo : ShareSessionInfo
    var delegate : SharerDelegate?
    var shareSummary : ShareSummary!
    var isAdHocShareSession : Bool
    
    init(shareSessionInfo: ShareSessionInfo){
        self.shareSessionInfo = shareSessionInfo
        let totalSharees = shareSessionInfo.fbRecipients.count + shareSessionInfo.grubbieRecipients.count
        self.isAdHocShareSession = (self.shareSessionInfo.shareType == ShareType.AdhocShare)
        // TODO: Set totalTaskCount to 100 is ad hoc share....
        totalTaskCount = totalSharees * shareSessionInfo.photos.count

    }
    
    func start() {
        ThreadingUtil.inBackground({
            var counter = 0
            var didCancel = false
            let allUsers = self.shareSessionInfo.grubbieRecipients + self.shareSessionInfo.fbRecipients
            let shareSummary = ShareSummary.insert({ (shareSummary) -> Void in
                shareSummary.sharedBy = Util.getCurrentUserId()
                shareSummary.photoCount = self.shareSessionInfo.photos.count
                shareSummary.recipientCount = allUsers.count
                shareSummary.startProcessDate = NSDate()
            })
            self.shareSummary = shareSummary
            var adhocShare : AdhocShare!
            for photoSource in self.shareSessionInfo.photos {
                if self.isAdHocShareSession {
                    adhocShare = AdhocShare.insert({ (newAdhocShare : AdhocShare) -> Void in
                        newAdhocShare.comments = photoSource.comments!
                        if let _  = photoSource.audio {
                            newAdhocShare.audio = photoSource.audio!
                        }
                        newAdhocShare.photo = photoSource.photo
                        newAdhocShare.shareSummaryId = shareSummary.id
                    })
                }
                
                if didCancel {
                    break
                }
                // TODO: If ad hoc share, somehow update progress to reflect correct task count.
                for user in allUsers {
                    ShareNotification.insert({ (shareNotif) -> Void in
                        shareNotif.sharedTo = user.id
                        shareNotif.shareSummaryRefId = shareSummary.id
                        shareNotif.sharedBy = Util.getCurrentUserId()
                        shareNotif.shareType = photoSource.shareType
                        
                        if self.isAdHocShareSession {
                            shareNotif.shareRefId = adhocShare.id
                        } else {
                            shareNotif.shareRefId = photoSource.refId
                        }
                    })
                    counter++
                    if let delegate = self.delegate {
                        delegate.didMakeProgress(counter)
                        if delegate.onQueryForCancel(counter) {
                            didCancel = true
                            delegate.didCancel(counter)
                            break
                        }
                    }
                }
            }
            ShareSummary.onFinish(shareSummary.id, completion: { (success, error) -> Void in
                print("ShareSummary.onFinish error: \(error)")
            })
            if let delegate = self.delegate {
                delegate.didFinish(counter)
            }
        })
        {
                // Thread completion block: do nothing
        }
    }
}






