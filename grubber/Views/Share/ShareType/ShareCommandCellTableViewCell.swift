//
//  ShareCommandCellTableViewCell.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

protocol ShareCommandCellTableViewCellDelegate {
    func didPressNextButton(indexPath: NSIndexPath)
}


class ShareCommandCellTableViewCell: UITableViewCell {


    @IBOutlet var commandMainTextLabel: UILabel!

    @IBOutlet var commandDetailTextView: UITextView!
    
    var delegate : ShareCommandCellTableViewCellDelegate?
    var indexPath: NSIndexPath?
    
    @IBAction func didPressNextButton(sender: AnyObject) {
        guard let indexPath = self.indexPath else {
            assertionFailure("ShareCommandCellTableViewCell indexPath not set")
            return
        }
        if let delegate = self.delegate {
            delegate.didPressNextButton(indexPath)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
