//
//  ShareHomeViewController.swift
//  Grubbie
//
//  Created by JABE on 12/4/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

// Implementation here just for conformance with requirements of PhotoEditorViewController.delegate; else onSave won't trigger.
extension ShareTypeViewController : AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {

    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {

    }
}


// MARK: PhotoEditorDelegate
extension ShareTypeViewController : PhotoEditorDelegate {
    func onSave(photoEditorData: PhotoEditorData, completionBlock: ((success: Bool, isNewObject: Bool, savedObject: AnyObject?) -> Void)?) {
        print("ShareHomeViewController : PhotoEditorDelegate -> onSave")
        self.readyToShare = true
//        self.photoToShare = photoEditorData
        self.shareSessionInfo.photos.append(SharePhoto(photoEditorData: photoEditorData))
        if let completion = completionBlock {
            completion(success: true, isNewObject: false, savedObject: nil)
        }
    }
    func onDeletePhoto(photoSource: PhotoSource, completionBlock: (deleted: Bool, postDeleteUserMessage: String?, error: NSError?) -> Void) {
        assertionFailure("Photos cannnot be deleted through this module.")
    }
    func queryCanDeletePhotoSource(photoSource: PhotoSource, completionBlock: (canDelete: Bool, userReason: String, userData: AnyObject?) -> Void) {
        completionBlock(canDelete: false, userReason: "Photos cannnot be deleted through this module.", userData: nil)
    }
}

extension ShareTypeViewController : ShareCommandCellTableViewCellDelegate {
    func didPressNextButton(indexPath: NSIndexPath) {
        commands[indexPath.row].invokeCommand(self)
    }
}


class ShareTypeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var shareSessionInfo : ShareSessionInfo!
//    var photoToShare : PhotoEditorData?
    var readyToShare = false
    
    var commands = [
        JLTableCellCommand(mainText: UserMessage.GrubbieShare.rawValue, detailText: UserMessage.GrubbieShareDetailText.rawValue, cellAccessoryType: UITableViewCellAccessoryType?.None, commandType: .Normal) { (settingsVc) -> Void in
            UIFactory.showStoryboard(settingsVc, storyboardId: StoryboardId.GrubbiePhotoSearchView, configureBlock: { (uiViewController) -> Void in
                guard let view = uiViewController as? PhotoSearchViewController,
                    shareTypeVc = settingsVc as? ShareTypeViewController else {
                    return
                }
                shareTypeVc.shareSessionInfo.shareType = .GrubbieShare
                view.shareSessionInfo = shareTypeVc.shareSessionInfo
            })
        }
        ,
        JLTableCellCommand(mainText: UserMessage.AdhocShare.rawValue, detailText: UserMessage.AdhocShareDetailText.rawValue, cellAccessoryType: UITableViewCellAccessoryType?.None, commandType: .Normal) { (uiShareTypeViewController) -> Void in
            UIFactory.showStoryboard(uiShareTypeViewController, storyboardId: StoryboardId.PhotoEditor) { (uiViewController) -> Void in
                guard let photoEditorVc = uiViewController as? PhotoEditorViewController,
                    shareTypeVc = uiShareTypeViewController as? ShareTypeViewController else {
                        return
                }
                shareTypeVc.shareSessionInfo.shareType = .AdhocShare

                photoEditorVc.photoEditorDelegate = shareTypeVc
                photoEditorVc.delegate = shareTypeVc
            }
        }
    ]

    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        if self.readyToShare {
            self.readyToShare = false
            UIFactory.showStoryboard(self, storyboardId: .SharingViewController) { (uiViewController) -> Void in
                if let view = uiViewController as? SharingViewController {
                    view.shareSessionInfo = self.shareSessionInfo
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if shareSessionInfo == nil {
            shareSessionInfo = ShareSessionInfo()
        }
        
        tableView.dataSource = self
        tableView.delegate = self
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commands.count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let command = commands[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! ShareCommandCellTableViewCell
        cell.commandMainTextLabel.text = command.mainText
        cell.commandDetailTextView.text = command.detailText!
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let command = commands[indexPath.row]
        command.invokeCommand(self)
    }
    
}
