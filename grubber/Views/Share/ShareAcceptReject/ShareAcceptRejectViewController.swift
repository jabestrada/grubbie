//
//  ShareAcceptRejectViewController.swift
//  Grubbie
//
//  Created by JABE on 12/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


class ShareAcceptRejectViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var sharedByLabel: UILabel!
    @IBOutlet var sharedOnLabel: UILabel!
    @IBOutlet var audioButton: UIButton!
    @IBOutlet var existingRestaurantLabel: UILabel!
    @IBOutlet var discardButton: UIButton!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var shareNotification : ShareNotification!
    var shareNotificationViewModel : ShareNotificationViewModel!
    var audioData: NSData?
    var audioPlayer = JLAudioPlayer()
    var activityIndicator : JLActivityIndicator!
    var isAdhocShare  = false {
        didSet {
            saveButton.enabled = !isAdhocShare
        }
    }
    
    let withAudioImage = UIImage(named: MediaName.PhotoEditorPlayButtonIcon.rawValue)
    let withoutAudioImage = UIImage(named: MediaName.NoAudio.rawValue)
    
    override func viewDidLoad() {
        self.activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view)
        JLThemes.apply(self.imageView)
        existingRestaurantLabel.hidden = true
        audioButton.hidden = true
        discardButton.hidden = true
        sharedByLabel.hidden = true
        sharedOnLabel.hidden = true
        
        self.isAdhocShare = self.shareNotification.shareType == SharePhotoType.AdHoc
            
        sharedOnLabel.text = Factory.createDateFormatterWithTime().stringFromDate( shareNotification.sentDate)
        if let _ = shareNotificationViewModel {
            configure(shareNotificationViewModel)
            queryIfRestaurantAlreadyExists()
        }
        else {
            ShareNotification.getShareNotifViewModel(shareNotification, completion: { (shareNotifViewModel, error) -> Void in
                if let shareNotifVm = shareNotifViewModel {
                    self.shareNotificationViewModel = shareNotifVm
                    self.configure(shareNotifVm)
                    if !self.isAdhocShare {
                        self.queryIfRestaurantAlreadyExists()
                    }
                }
                else {
                    Util.showMessage(self, msg: "Shared item cannot be resolved. It may have been deleted by the sharer.", title: "", completionBlock: { (alertAction) -> Void in
                        Util.closeController(self)
                        EventPublisher.sharedAppEventPublisher().emit(AppEventType.ShareNotificationNotFound, userData: self.shareNotification.id)
                    })
                }
            })
        }
    }
    
    func queryIfRestaurantAlreadyExists(){
        if let restaurantName = shareNotificationViewModel.photoSourceInfo?.restaurantName {
            if let _ = Restaurant.getByName(restaurantName) {
                existingRestaurantLabel.hidden = false
            }
        }
        audioButton.hidden = false
        discardButton.hidden = false
        sharedByLabel.hidden = false
        sharedOnLabel.hidden = false

        self.activityIndicator.stopAnimation()
    }
    
    
    func configure(shareNotifViewModel: ShareNotificationViewModel){
        sharedByLabel.text = "Shared by \(shareNotifViewModel.sharerFirstName) \(shareNotifViewModel.sharerLastName)"
        imageView.image = shareNotifViewModel.photoData
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        self.imageView.addGestureRecognizer(gestureRecognizer)

        if let audio = shareNotifViewModel.audioData {
            self.audioData = audio
            audioButton.enabled = true
            audioButton.setImage(withAudioImage, forState: .Normal)
        } else {
            audioButton.enabled = false
            audioButton.setImage(withoutAudioImage, forState: .Normal)
        }
        
        var caption = ""
        if self.isAdhocShare {
            descriptionTextView.text = "No restaurant and location info because this is an ad hoc share.\n\n\(shareNotifViewModel.comments)"
        }
        else {
            if let photoSourceInfo = shareNotifViewModel.photoSourceInfo {
                caption = "\(photoSourceInfo.restaurantName)\n"
                if !photoSourceInfo.townCity.isEmpty {
                    caption = caption + "\(photoSourceInfo.townCity)"
                }
                if !photoSourceInfo.country.isEmpty {
                    caption = caption + (!photoSourceInfo.townCity.isEmpty ? ", " : "" ) +  "\(photoSourceInfo.country)\n"
                }
            }
            descriptionTextView.text = "\(caption)\n\(shareNotifViewModel.comments)"
        }
    }
    
    
    func imageTapped(img: AnyObject){
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.FullPhotoViewer) { (uiViewController) -> Void in
            if let view = uiViewController as? FullScreenPhotoViewController {
                view.image = self.imageView.image
            }
        }
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        self.activityIndicator.startAnimation()
        ShareNotification.accept(self.shareNotification) { (success, newPhoto, error) -> Void in
            self.activityIndicator.stopAnimation()
            if success {
                EventPublisher.sharedAppEventPublisher().emit(.ShareNotificationAccepted, userData: self.shareNotification.id)
                Util.showMessage(self, msg: "Shared photo was successfully accepted", title: "",
                    completionBlock: { (action) -> Void in
                    Util.closeController(self)
                })
            }
            else {
                if let error = error {
                    if error.code == AppErrorCode.ObjectNotFound.rawValue {
                        Util.showMessage(self, msg: "Something went wrong while accepting the share. The sharer might have deleted the shared item.", title: UserDialogTitle.Error.rawValue, completionBlock: { (alertAction) -> Void in
                            Util.closeController(self)
                            EventPublisher.sharedAppEventPublisher().emit(AppEventType.ShareNotificationNotFound, userData: self.shareNotification.id)
                        })
                    } else {
                        self.showGenericErrorMessage()
                    }
                }
                else {
                    self.showGenericErrorMessage()
                }
            }

        }
    }
    
    func showGenericErrorMessage(){
        Util.showMessage(self, msg: "Something went wrong while accepting the share. Please try again later.", title: UserDialogTitle.Error.rawValue)
    }
    
    @IBAction func discardButtonPressed(sender: AnyObject) {
        Util.getConfirmation(self, message: "Are you sure you want to discard this share?", title: "", yesCaption: "Yes. Totally.", noCaption: "Nope, I changed my mind.") { (alertAction) -> Void in
            self.activityIndicator.startAnimation()
            ShareNotification.reject(self.shareNotification) { (success, error) -> Void in
                self.activityIndicator.stopAnimation()
                if success {
                    Util.showMessage(self, msg: "Shared photo was successfully discarded.", title: "", completionBlock: { (alertAction) -> Void in
                        Util.closeController(self)
                        EventPublisher.sharedAppEventPublisher().emit(AppEventType.ShareNotificationRejected, userData: self.shareNotification.id)
                    })
                } else {
                    Util.showMessage(self, msg: "There was a problem discarding the notification. Please try again later.", title: UserDialogTitle.Error.rawValue)
                }
            }
        }
    }
    

    @IBAction func audioButtonPressed(sender: AnyObject) {
        if let audioData = self.audioData {
            audioButton.enabled = false
            self.audioPlayer.play(audioData, completionBlock: { (player, successfully) -> Void in
                self.audioButton.enabled = true
            })
        }
    }
    
}



























