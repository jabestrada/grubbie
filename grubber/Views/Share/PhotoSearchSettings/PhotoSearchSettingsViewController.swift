//
//  PhotoSearchSettingsViewController.swift
//  Grubbie
//
//  Created by JABE on 12/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

struct PhotoSearchSettings {
    var restaurantName : String
    var photoCaption : String
    var townCity : String
    var country : String
}

protocol PhotoSearchSettingsViewControllerDelegate {
    func didFinishSettings(settings: PhotoSearchSettings)
}

class PhotoSearchSettingsViewController : UIViewController {
    
    @IBOutlet var restaurantName: UITextField!
    @IBOutlet var photoCaption: UITextField!
    @IBOutlet var townCity: UITextField!
    @IBOutlet var country: UITextField!
    
    
    let tagMappingFactor = 10
    
    var delegate : PhotoSearchSettingsViewControllerDelegate?
    var settings : PhotoSearchSettings?
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadSettings()
    }

//    override func viewDidAppear(animated: Bool) {
//        loadSettings()
//    }
    
    func loadSettings(){
        if let settings = self.settings {
            restaurantName.text = settings.restaurantName
            photoCaption.text = settings.photoCaption
            townCity.text = settings.townCity
            country.text = settings.country
            evalTextFields(restaurantName, photoCaption, townCity, country)
        }
    }
    
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    func gatherSettings() {
        self.settings = PhotoSearchSettings(restaurantName: restaurantName.text!, photoCaption: photoCaption.text!, townCity: townCity.text!, country: country.text!)
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            self.gatherSettings()
            delegate.didFinishSettings(self.settings!)
            Util.closeController(self)
        }
    }
    
    @IBAction func filterTextChanged(sender: AnyObject) {
        if let textField = sender as? UITextField {
            evalTextFields(textField)
        }
        
    }
    
    func evalTextFields(textFields : UITextField...){
        for textField in textFields {
            if let clearButton = self.view.viewWithTag(textField.tag * tagMappingFactor) as? UIButton {
                clearButton.hidden = (textField.text?.isEmpty)!
            }
        }
    }
    
    @IBAction func clearTextFieldButtonPressed(sender: AnyObject) {
        if let deleteButton = sender as? UIButton {
            if let textField = self.view.viewWithTag(deleteButton.tag / tagMappingFactor) as? UITextField {
                textField.text = ""
                deleteButton.hidden = true
            }
        }
    }
    
}