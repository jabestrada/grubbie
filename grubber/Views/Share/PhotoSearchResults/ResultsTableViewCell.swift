//
//  ResultsTableViewCell.swift
//  Grubbie
//
//  Created by JABE on 12/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation


protocol ResultsTableViewCellDelegate {
    func didPressCheckboxButton(indexPath: NSIndexPath, isChecked: Bool)
    func didTapImage(image:UIImage)
    func didPressPlayButton(indexPath: NSIndexPath, completion: () -> Void)
}

class ResultsTableViewCell : UITableViewCell {

    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var photoCaption: UITextView!
    @IBOutlet var checkboxButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var commentsFetchActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var playButton: UIButton!
    
    var delegate : ResultsTableViewCellDelegate!
    var indexPath : NSIndexPath!
    var photoSourceId = ""
    var buttonIsChecked : Bool! {
        didSet {
            checkboxButton.setImage(buttonIsChecked! ? checkedImage : uncheckedImage,   forState: .Normal)
        }
    }
    let checkedImage = UIImage(named: MediaName.TableViewCellCheckedImage.rawValue)
    let uncheckedImage = UIImage(named: MediaName.TableViewCellUncheckedImage.rawValue)
    
    @IBAction func playButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            playButton.enabled = false
            delegate.didPressPlayButton(self.indexPath, completion: {
                self.playButton.enabled = true
            })
        }
    }
    
    var playImage = UIImage(named: MediaName.PhotoEditorPlayButtonIcon.rawValue)
    var noPlayImage = UIImage(named: MediaName.NoAudio.rawValue)
    
    
    func configureWithPhotoSourceInfo(searchResult: SearchResultViewModel, photoSourceInfo: PhotoSourceInfo){
        var caption = "\(photoSourceInfo.restaurantName)\n"
        if !photoSourceInfo.townCity.isEmpty {
            caption = caption + "\(photoSourceInfo.townCity)"
        }
        if !photoSourceInfo.country.isEmpty {
            caption = caption + (!photoSourceInfo.townCity.isEmpty ? ", " : "" ) +  "\(photoSourceInfo.country)\n"
        }

        self.photoCaption.text = "\(caption)\n\(searchResult.photoSource.getComments())"
    }
    
    func configure(indexPath: NSIndexPath, searchResult: SearchResultViewModel, photoSourceInfo: PhotoSourceInfo?, delegate: ResultsTableViewCellDelegate, completion: (photoSourceInfo: PhotoSourceInfo, searchResult: SearchResultViewModel) -> Void){
        self.indexPath = indexPath
        self.buttonIsChecked = searchResult.isSelected
        
        self.delegate = delegate

        if let photoSourceInfo = photoSourceInfo {
            configureWithPhotoSourceInfo(searchResult, photoSourceInfo: photoSourceInfo)
        } else {
            self.commentsFetchActivityIndicator.startAnimating()
            searchResult.photoSource.getPhotoSourceInfoInBackground { (photoSourceInfo, error) -> Void in
                self.commentsFetchActivityIndicator.stopAnimating()
                if let photoSourceInfo = photoSourceInfo {
                    self.configureWithPhotoSourceInfo(searchResult, photoSourceInfo: photoSourceInfo)
                    completion(photoSourceInfo: photoSourceInfo, searchResult: searchResult)
                }
                else {
                    self.photoCaption.text = "No info available\n\n\(searchResult.photoSource.getComments())"
                }

            }
        }

        if self.photoImageView.image == nil || searchResult.photoSource.getId() != self.photoSourceId {
            self.photoSourceId = searchResult.photoSource.getId()

            let hasAudio = searchResult.photoSource.getAudio() != NSData?.None
            self.playButton.setImage(hasAudio ? playImage : noPlayImage, forState: .Normal)
            self.playButton.enabled = hasAudio
            
            activityIndicator.startAnimating()
            self.photoImageView.hidden = true
            
            JLThemes.apply(self.photoImageView)
            searchResult.photoSource.getPhotoInBackground { (image, error) -> Void in
                if error == nil {
                    self.photoImageView.image = image!
                    let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
                    self.photoImageView.addGestureRecognizer(gestureRecognizer)
                }

                self.photoImageView.hidden = false
                self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    func imageTapped(img: AnyObject){
        if let delegate = self.delegate {
            delegate.didTapImage(photoImageView.image!)
        }
    }
    
    @IBAction func checkboxButtonPressed(sender: AnyObject) {
        self.buttonIsChecked = !self.buttonIsChecked
        self.delegate.didPressCheckboxButton(self.indexPath, isChecked: self.buttonIsChecked)
    }
}