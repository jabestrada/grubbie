//
//  PhotoSearchViewController.swift
//  Grubbie
//
//  Created by JABE on 12/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

extension PhotoSearchViewController : PhotoSearchSettingsViewControllerDelegate {
    func didFinishSettings(settings: PhotoSearchSettings) {

        self.settings = settings
        config.restaurantNameFilter = settings.restaurantName
        config.photoCaptionFilter = settings.photoCaption
        config.townCityFilter = settings.townCity
        config.countryFilter = settings.country
        self.hasSearched = true
        
        self.shareButton.hidden = false
        self.searchResultsContainer.hidden = true
        self.userMessageContainer.hidden = false
        self.userMessageLabel.text = "Searching in progress ..."
        self.doSearch()
    }
}


extension PhotoSearchViewController : ResultsTableViewCellDelegate {
    func didPressCheckboxButton(indexPath: NSIndexPath, isChecked: Bool) {
        let searchResult = self.searchResults[indexPath.row]
        searchResult.isSelected = isChecked
        self.selectedCount =  self.selectedCount + (isChecked ? 1 : -1)
    }
    
    func didTapImage(image: UIImage) {
            UIFactory.showStoryboard(self, storyboardId: .FullPhotoViewer) { (uiViewController) -> Void in
                if let view = uiViewController as? FullScreenPhotoViewController {
                    view.image = image
                }
        }
    }
    func didPressPlayButton(indexPath: NSIndexPath, completion: () -> Void) {
        let photo = self.searchResults[indexPath.row].photoSource
        if let audioData = photo.getAudio() {
            self.audioPlayer.play(audioData, completionBlock: { (player, successfully) -> Void in
                completion()
            })
            
        }
        else {
            print("No audio for this photo")
        }
    }

}


extension PhotoSearchViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! ResultsTableViewCell
        let photoSourceInfo = self.photoSourceInfos[searchResults[indexPath.row].photoSource.getId()]
        cell.configure(indexPath, searchResult: searchResults[indexPath.row],  photoSourceInfo: photoSourceInfo, delegate: self) { (photoSourceInfo, searchResultViewModel) -> Void in
            self.photoSourceInfos[searchResultViewModel.photoSource.getId()] = photoSourceInfo
        }
        
        return cell
    }
}

class SearchResultViewModel {
    var isSelected : Bool
    var photoSource : PhotoSource
    var photoSourceInfo: PhotoSourceInfo?
    
    init(isSelected: Bool, photoSource: PhotoSource){
        self.isSelected = isSelected
        self.photoSource = photoSource
    }
}

class PhotoSearchViewController : UIViewController {
    @IBOutlet var userMessageLabel: UILabel!
    @IBOutlet var resultsTableView: UITableView!
    @IBOutlet var searchResultsContainer: UIView!
    @IBOutlet var userMessageContainer: UIView!
    @IBOutlet var checkAllButton: UIButton!
    @IBOutlet var selectedLabel: UILabel!
    @IBOutlet var shareButton: UIButton!
    
    var photoSourceInfos = [String: PhotoSourceInfo]()
    
    var shareSessionInfo : ShareSessionInfo!
    var activityIndicator : JLActivityIndicator!
    var audioPlayer = JLAudioPlayer()
    var settings : PhotoSearchSettings?
    var config = Factory.createConfigProvider()
    var hasSearched = false
    var searchResults = [SearchResultViewModel]() {
        didSet{
            self.totalResultsCount = searchResults.count
            let hasResults = self.totalResultsCount > 0
            if hasSearched && !hasResults {
                userMessageLabel.text = "There are no photos\nthat matched the search criteria."
            }
            searchResultsContainer.hidden = !hasResults
            userMessageContainer.hidden = hasResults
        }
    }
    
    var checkedImage = UIImage(named: MediaName.TableViewCellCheckedImage.rawValue)
    var uncheckedImage = UIImage(named: MediaName.TableViewCellUncheckedImage.rawValue)
    var totalResultsCount = 0
    var selectedCount = 0 {
        didSet{
            self.selectedLabel.text = "\(selectedCount) selected out of \(totalResultsCount)"
        }
    }
    
    var allChecked = true {
        didSet {
            selectedCount = allChecked ? searchResults.count : 0
            checkAllButton.setImage(allChecked ? checkedImage : uncheckedImage, forState: .Normal)
        }
    }
    
    override func viewDidLoad() {
        if shareSessionInfo == nil {
            shareSessionInfo = ShareSessionInfo()
        }
        
        self.activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: false)
        self.settings =  PhotoSearchSettings(restaurantName: config.restaurantNameFilter, photoCaption: config.photoCaptionFilter, townCity: config.townCityFilter, country: config.countryFilter)
        resultsTableView.dataSource = self
        resultsTableView.reloadData()
    }
    
    @IBAction func checkAllButtonPressed(sender: AnyObject) {
        self.allChecked = !self.allChecked
        for searchResult in self.searchResults {
            searchResult.isSelected = self.allChecked
        }
        self.resultsTableView.reloadData()
    }
    
    @IBAction func shareButtonPressed(sender: AnyObject) {
        var sharedPhotos = [PhotoSource]()
        for result in searchResults {
            if result.isSelected {
                sharedPhotos.append(result.photoSource)
            }
        }
       print("\(sharedPhotos.count) photo(s) will be shared.")
        if sharedPhotos.count == 0 {
            Util.showMessage(self, msg: "Please select at least 1 photo to share.", title: "")
            return
        }
        self.shareSessionInfo.photos = sharedPhotos.map({p in SharePhoto(photoSource: p)})
        UIFactory.showStoryboard(self, storyboardId: .SharingViewController) { (uiViewController) -> Void in
            if let view = uiViewController as? SharingViewController {
                view.shareSessionInfo = self.shareSessionInfo
            }
        }
    }
    
    
    @IBAction func settingsButtonPressed(sender: AnyObject) {
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.PhotoSearchSettingsView) { (uiViewController) -> Void in
            if let view = uiViewController as? PhotoSearchSettingsViewController {
                view.delegate = self
                view.settings = self.settings
            }
        }
    }
    
    func doSearch(){
        self.activityIndicator.startAnimation()
        let search = GrubbieSearch()
        
        search.findPhotos(settings!) { (photos, error) -> Void in
            if let photos = photos {
                self.searchResults = photos.map({p in SearchResultViewModel(isSelected: true, photoSource: p)})
            }
            self.allChecked = true
            self.activityIndicator.stopAnimation()
            self.resultsTableView.reloadData()
        }
    }
    
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
}