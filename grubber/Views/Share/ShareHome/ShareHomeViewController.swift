//
//  ShareHomeViewController.swift
//  Grubbie
//
//  Created by JABE on 12/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



class SharedToYouDataSource : NSObject, UITableViewDataSource {
    var shareNotifications : [ShareNotification]
    var shareNotifViewModels = [String : ShareNotificationViewModel]()
    var sharedHomeViewController : ShareHomeViewController!
    
    var dateFormatter : NSDateFormatter!
    var audioPlayer : JLAudioPlayer!
    
    init(shareNotifications: [ShareNotification], shareHomeVc: ShareHomeViewController){
        self.shareNotifications = shareNotifications
        self.dateFormatter = shareHomeVc.dateFormatter
        self.audioPlayer = shareHomeVc.audioPlayer
        self.sharedHomeViewController = shareHomeVc
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shareNotifications.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let shareNotif = self.shareNotifications[indexPath.row]
        let shareNotifViewModel = self.shareNotifViewModels[shareNotif.id]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! SharedToYouTableViewCell
        cell.configure(indexPath, shareNotif: shareNotif, shareNotifViewModel: shareNotifViewModel, dateFormatter: self.dateFormatter, audioPlayer: self.audioPlayer, delegate: self.sharedHomeViewController) {
            (shareNotif, shareNotifViewModel) -> Void in
            self.shareNotifViewModels[shareNotif.id] = shareNotifViewModel
        }
        return cell
    }
}

extension ShareHomeViewController : SharedToYouTableViewCellDelegate {
    func didPressMoreDetailsButton(indexPath: NSIndexPath) {
        let shareNotif = self.sharedToYouDataSource.shareNotifications[indexPath.row]
        let shareNotifViewModel = self.sharedToYouDataSource.shareNotifViewModels[shareNotif.id]
    
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.ShareAcceptRejectViewController) { (uiViewController) -> Void in
            if let view = uiViewController as? ShareAcceptRejectViewController {
                view.shareNotification = shareNotif
                view.shareNotificationViewModel = shareNotifViewModel
            }
        }
    }
}

extension ShareHomeViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sharedByYouSummaries.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
 
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let shareSummary = sharedByYouSummaries[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        cell.textLabel?.text = "\(self.dateFormatter.stringFromDate(shareSummary.startProcessDate!))"
        
        let photoText = shareSummary.photoCount > 1 ? "s" : ""
        let personText = shareSummary.recipientCount > 1 ? "s" : ""
        cell.detailTextLabel?.text = "You shared \(shareSummary.photoCount) photo\(photoText) to \(shareSummary.recipientCount) person\(personText)"
        return cell
    }
}


extension ShareHomeViewController : AppEventSubscriber {
    func onEvent(eventType: AppEventType, eventTimeStamp: NSDate, userData: AnyObject?) {
        switch eventType {
        case .ShareSummaryAdded, .ShareNotificationAccepted, .ShareNotificationRejected, .ShareNotificationNotFound:
            self.onSharedToByChanged()
            break;
        default:
            // Ignore.
            break;
        }
    }
}


class ShareHomeViewController: UIViewController {
    @IBOutlet var sharedToBy: UISegmentedControl!
    @IBOutlet var sharedToYouContainer: UIView!
    @IBOutlet var sharedByYouContainer: UIView!
    @IBOutlet var sharedByYouTableView: UITableView!
    @IBOutlet var youHaveNotSharedAnyLabel: UILabel!
    
    @IBOutlet var youHaveNoItemsSharedToYouLabel: UILabel!
    @IBOutlet var sharedToYouTableView: UITableView!
    
    var sharedByYouSummaries = [ShareSummary]()
    var activityIndicator : JLActivityIndicator!
    var sharedToYouDataSource : SharedToYouDataSource!
    var audioPlayer = JLAudioPlayer()

    var dateFormatter = Factory.createDateFormatterWithTime()
    
    override func viewDidLoad() {
        EventPublisher.sharedAppEventPublisher().addSubscriber(self, eventTypes: .ShareSummaryAdded, .ShareNotificationAccepted, .ShareNotificationRejected, .ShareNotificationNotFound)
        
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: false)
        sharedToBy.selectedSegmentIndex = 0
        
        sharedByYouTableView.dataSource = self
        sharedByYouTableView.hidden = true
        youHaveNotSharedAnyLabel.hidden = true
        

        self.sharedToYouTableView.hidden = true
        self.youHaveNoItemsSharedToYouLabel.hidden = true
        
        onSharedToByChanged()
    }
    
    
    func onSharedToByChanged(){
        sharedToYouContainer.hidden = sharedToBy.selectedSegmentIndex != 0
        sharedByYouContainer.hidden = sharedToBy.selectedSegmentIndex != 1
        if sharedByYouContainer.hidden == false {
            activityIndicator.startAnimation()
            ShareSummary.getShareSummariesOfUser(Util.getCurrentUserId()) { (shareSummaries, error) -> Void in
                if let summaries = shareSummaries {
                    self.sharedByYouSummaries = summaries
                    self.sharedByYouTableView.hidden = summaries.count == 0
                } else {
                    self.sharedByYouTableView.hidden = true
                }
                self.youHaveNotSharedAnyLabel.hidden = !self.sharedByYouTableView.hidden
                self.sharedByYouTableView.reloadData()
                self.activityIndicator.stopAnimation()
                self.sharedToBy.setTitle("Shared by you (\(self.sharedByYouSummaries.count))", forSegmentAtIndex: 1)
            }
        } else if sharedToYouContainer.hidden == false {
            activityIndicator.startAnimation()
            ShareNotification.getShareNotificationsForUser(Util.getCurrentUserId()) { (notifications, error) -> Void in
                self.sharedToYouTableView.hidden = notifications.count == 0
                self.youHaveNoItemsSharedToYouLabel.hidden = !self.sharedToYouTableView.hidden
                self.sharedToYouDataSource = SharedToYouDataSource(shareNotifications: notifications, shareHomeVc: self)
                self.sharedToYouTableView.dataSource = self.sharedToYouDataSource
                self.sharedToYouTableView.reloadData()
                self.sharedToBy.setTitle("Shared to you (\(notifications.count))", forSegmentAtIndex: 0)
                self.activityIndicator.stopAnimation()
            }
        }
    }
    
    
    
    @IBAction func sharedToByValueChanged(sender: AnyObject) {
        onSharedToByChanged()
    }
    
}
