//
//  SharedToYouTableViewCell.swift
//  Grubbie
//
//  Created by JABE on 12/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

protocol SharedToYouTableViewCellDelegate {
    func didPressMoreDetailsButton(indexPath: NSIndexPath)
}

class SharedToYouTableViewCell: UITableViewCell {

    @IBOutlet var shareImageView: UIImageView!
    @IBOutlet var shareDateLabel: UILabel!
    @IBOutlet var sharedByLabel: UILabel!
    @IBOutlet var audioButton: UIButton!
    @IBOutlet var moreDetailsButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var delegate : SharedToYouTableViewCellDelegate?
    var indexPath: NSIndexPath!
    var audioPlayer : JLAudioPlayer!
    var audioData : NSData?
    
    let withAudioImage = UIImage(named: MediaName.PhotoEditorPlayButtonIcon.rawValue)
    let withoutAudioImage = UIImage(named: MediaName.NoAudio.rawValue)
    
    func configure(indexPath: NSIndexPath, shareNotif: ShareNotification, shareNotifViewModel: ShareNotificationViewModel?, dateFormatter: NSDateFormatter, audioPlayer: JLAudioPlayer, delegate: SharedToYouTableViewCellDelegate,
        completion: (shareNotif: ShareNotification, shareNotifViewModel: ShareNotificationViewModel?) -> Void) {
        
        JLThemes.apply(shareImageView)
        self.delegate = delegate
        self.indexPath = indexPath
        self.audioPlayer = audioPlayer
        shareDateLabel.text = dateFormatter.stringFromDate(shareNotif.sentDate)
        if let shareNotifViewModel = shareNotifViewModel {
            print("Not fetching shareNotifViewModel ...")
            configWithViewModel(shareNotifViewModel)
        } else {
            self.activityIndicator.startAnimating()
            self.shareImageView.hidden = true
            ShareNotification.getShareNotifViewModel(shareNotif) { (shareNotifViewModel, error) -> Void in
                ThreadingUtil.inMainQueue({ () -> Void in
                    self.activityIndicator.stopAnimating()
                })
                if let shareNotifViewModel = shareNotifViewModel {
                    self.shareImageView.hidden = false
                    self.configWithViewModel(shareNotifViewModel)
                    completion(shareNotif: shareNotif, shareNotifViewModel: shareNotifViewModel)
                } else {
                    self.shareImageView.hidden = true
                    completion(shareNotif: shareNotif, shareNotifViewModel: ShareNotificationViewModel?.None)
                }
            }
        }
    }
    
    private func configWithViewModel(shareNotifViewModel: ShareNotificationViewModel) {
        self.audioData = shareNotifViewModel.audioData
        self.shareImageView.image = shareNotifViewModel.photoData
        if let audio = shareNotifViewModel.audioData {
            self.audioData = audio
            self.audioButton.enabled = true
            self.audioButton.setImage(self.withAudioImage, forState: .Normal)
        }
        else {
            self.audioButton.enabled = false
            self.audioButton.setImage(self.withoutAudioImage, forState: .Normal)
        }
        self.sharedByLabel.text = "from \(shareNotifViewModel.sharerFirstName) \(shareNotifViewModel.sharerLastName)"
    }
    
    @IBAction func audioButtonPressed(sender: AnyObject) {
        if let audioData = self.audioData {
            self.audioButton.enabled = false
            self.audioPlayer.play(audioData, completionBlock: { (player, successfully) -> Void in
                self.audioButton.enabled = true
            })
        }
    }
    
    
    @IBAction func moreDetailsButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.didPressMoreDetailsButton(self.indexPath)
        }
    }
}

