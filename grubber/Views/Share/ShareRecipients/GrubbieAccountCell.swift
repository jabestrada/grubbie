//
//  GrubbieAccountCell.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

protocol GrubbieAccountCellDelegate {
    func didPressDelete(indexPath: NSIndexPath)
}

class GrubbieAccountCell: UITableViewCell {
    
    @IBOutlet var userNameLabel: UILabel!
    
    var indexPath : NSIndexPath!
    var delegate : GrubbieAccountCellDelegate?
//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    func configure(indexPath : NSIndexPath, delegate : GrubbieAccountCellDelegate){
        self.indexPath = indexPath
        self.delegate = delegate
    }
    
    @IBAction func deleteButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.didPressDelete(self.indexPath)
        }
    }
}
