//
//  ShareReciepientsViewController.swift
//  Grubbie
//
//  Created by JABE on 12/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

extension ShareRecipientsViewController : GrubbieAccountCellDelegate{
    func didPressDelete(indexPath: NSIndexPath) {
        grubbieAccounts.removeAtIndex(indexPath.row)
        onGrubbieAccountsChanged()

    }
}

extension ShareRecipientsViewController : UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.grubbieAccounts.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! GrubbieAccountCell
        cell.configure(indexPath, delegate: self)
        cell.userNameLabel.text = grubbieAccounts[indexPath.row].username
        return cell
    }
}




class FBAccountsDataSource : NSObject, UITableViewDataSource, UITableViewDelegate {
    var fbAccounts = [FBAccountViewModel]()
    private var rawFbAccounts = [FBAccountViewModel]()
    var shareRecipientsVc : ShareRecipientsViewController
    
    init(shareRecipientsVc: ShareRecipientsViewController){
        self.shareRecipientsVc = shareRecipientsVc
    }
    
    func setFbUsers(fbAccounts: [AppUser]){
        self.fbAccounts = fbAccounts.map({fbAccount in FBAccountViewModel(fbAccount: fbAccount)})
//        let user1 = AppUser()
//        user1.fbFirstName = "Julius"
//        user1.fbLastName = "Estrada"
//    
//        let user2 = AppUser()
//        user2.fbFirstName = "Adam"
//        user2.fbLastName = "Smith"
//        
//        self.fbAccounts.append(FbAccountViewModel(fbAccount: user1))
//        self.fbAccounts.append(FbAccountViewModel(fbAccount: user2))
        
        self.fbAccounts.sortInPlace({(x, y) -> Bool in
            x.fbAccount.fbFirstName < y.fbAccount.fbFirstName
        })
        // Use self.rawFbAccounts for filtering/sorting purposes in future enhancements.
        self.rawFbAccounts = self.fbAccounts
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fbAccounts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! FBAccountCell
        cell.configure(indexPath, appUser: self.fbAccounts[indexPath.row], isChecked: self.fbAccounts[indexPath.row].isSelected, delegate: self.shareRecipientsVc)
        return cell
    }
}

extension ShareRecipientsViewController : FBAccountCellDelegate {
    func didPressSelect(indexPath: NSIndexPath, isSelected: Bool) {
        self.fbAccountsDataSource.fbAccounts[indexPath.row].isSelected = isSelected
        self.selectedFbAccounts = selectedFbAccounts + (isSelected ? 1 : -1)
    }
}


class ShareRecipientsViewController: UIViewController {

    @IBOutlet var grubbieUserNameTextField: UITextField!
    @IBOutlet var grubbieAccountTableView: UITableView!
    @IBOutlet var accountTypeSegmentedView: UISegmentedControl!
    @IBOutlet var grubbieAccountsContainer: UIView!
    @IBOutlet var fbAccountsContainer: UIView!
    @IBOutlet var noFbGrubbieFriendsContainer: UIView!
    @IBOutlet var fbAccountsTableView: UITableView!
    @IBOutlet var selectAccountsShareInstructionContainer: UIView!
    
    
    var shareSessionInfo : ShareSessionInfo!
    
    var selectedFbAccounts = 0 {
        didSet{
            accountTypeSegmentedView.setTitle("Facebook Accounts (\(self.selectedFbAccounts))", forSegmentAtIndex: 1)
        }
    }
    
    var fbAccountsActivityIndicator : JLActivityIndicator!
    var grubbieAccountsActivityIndicator : JLActivityIndicator!
    var grubbieAccounts = [AppUser]()
    var fbAdapter = Factory.createFBAdapter()
    var fbAccountsDataSource : FBAccountsDataSource!
    
    var hasFbFriendsOnGrubbie : Bool? {
        didSet{
            if let hasFriendsOnGrubbie = self.hasFbFriendsOnGrubbie {
                noFbGrubbieFriendsContainer.hidden = hasFriendsOnGrubbie
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if shareSessionInfo == nil {
            shareSessionInfo = ShareSessionInfo()
        }
        
        self.fbAccountsDataSource = FBAccountsDataSource(shareRecipientsVc: self)
        
        self.fbAccountsActivityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: fbAccountsContainer, startAnimating: false)
        self.grubbieAccountsActivityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: grubbieAccountsContainer, startAnimating: false)
        
        fbAccountsTableView.dataSource = self.fbAccountsDataSource
        grubbieAccountTableView.dataSource = self
        accountTypeSegmentedView.selectedSegmentIndex = -1
        onGrubbieAccountTypeChanged()
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func grubbieAccountTypeChanged(sender: AnyObject) {
        onGrubbieAccountTypeChanged()
    }
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        if grubbieAccounts.count == 0 && self.selectedFbAccounts  == 0 {
            Util.showMessage(self, msg: "Please select at least 1 Grubbie to share with.", title: "")
        }
        else {
            self.shareSessionInfo.grubbieRecipients = grubbieAccounts
            self.shareSessionInfo.fbRecipients = self.getSelectedAccounts()

            UIFactory.showStoryboard(self, storyboardId: StoryboardId.ShareType) { (uiViewController) -> Void in
                if let view = uiViewController as? ShareTypeViewController {
                    view.shareSessionInfo = self.shareSessionInfo
                }
            }
        }
    }

    
    func getSelectedAccounts() -> [AppUser]{
        var selectedFbAccounts = [AppUser]()
        for user in self.fbAccountsDataSource.fbAccounts {
            if user.isSelected {
                selectedFbAccounts.append(user.fbAccount)
            }
        }
        return selectedFbAccounts
    }
    
    func onGrubbieAccountTypeChanged(){
        self.grubbieUserNameTextField.endEditing(true)
        
        grubbieAccountsContainer.hidden = accountTypeSegmentedView.selectedSegmentIndex != 0
        fbAccountsContainer.hidden = accountTypeSegmentedView.selectedSegmentIndex != 1
        selectAccountsShareInstructionContainer.hidden = accountTypeSegmentedView.selectedSegmentIndex > -1
        
        if !fbAccountsContainer.hidden {
            if let _ = self.hasFbFriendsOnGrubbie {
                print("No longer retrieving FB friends")
                // No-op
            }
            else {
                print("Retrieving FB friends...")
                fbAccountsActivityIndicator.startAnimation()
                self.fbAdapter.getFriendsUsingApp({ (fbUsers, error) -> Void in
                    self.fbAccountsActivityIndicator.stopAnimation()
                    if error == nil {
                        var fbAccounts = [AppUser]()
                        self.fbAccountsActivityIndicator.startAnimation()
                        for fbUser in fbUsers {
                            if let appUser = AppUser.getUserByFbId(fbUser.fbId) {
                                fbAccounts.append(appUser)
                            }
                        }
                        
                        self.hasFbFriendsOnGrubbie = fbAccounts.count > 0
                        self.fbAccountsDataSource.setFbUsers(fbAccounts)
                        self.selectedFbAccounts = self.fbAccountsDataSource.fbAccounts.filter { $0.isSelected}.count
                        self.fbAccountsTableView.reloadData()
                        self.fbAccountsActivityIndicator.stopAnimation()
                        print("Done retrieving FB friends")
                    } else {
                        self.hasFbFriendsOnGrubbie = Bool?.None
                        Util.showMessage(self, msg: "There seemed to be a problem reaching out to Facebook. Please try again later.", title: UserDialogTitle.Error.rawValue)
                        print("onGrubbieAccountTypeChanged.hasFbFriendsOnGrubbie error: \(error)")
                    }
                })
            }
        }
    }
    
        
    @IBAction func addGrubbieUserButtonPressed(sender: AnyObject) {
        guard let userName = grubbieUserNameTextField.text?.lowercaseString else {
            return
        }
        guard !userName.isEmpty else {
            Util.showMessage(self, msg: "Please enter Grubbie account to add.", title: "")
            return
        }
        

        if grubbieAccounts.contains({user in user.username == userName}){
            Util.showMessage(self, msg: "User \(userName) is already in the list.", title: "")
        }
        else {
            var appAccount : AppUser?
            grubbieAccountsActivityIndicator.startAnimation()
            ThreadingUtil.inBackground({ () -> Void in
                appAccount = AppUser.getAppAccount(userName)
                }, callBack: { () -> Void in
                    self.grubbieAccountsActivityIndicator.stopAnimation()
                    if let account = appAccount{
                        self.grubbieUserNameTextField.text = ""
                        self.grubbieAccounts.append(account)
                        self.onGrubbieAccountsChanged()
                    }
                    else {
                        Util.showMessage(self, msg: "\(userName) is not a registered Grubbie user.", title: "")
                    }
                    
            })
        }
    }
    
    func onGrubbieAccountsChanged(){
        grubbieAccountTableView.reloadData()
        accountTypeSegmentedView.setTitle("Grubbie Accounts (\(self.grubbieAccounts.count))", forSegmentAtIndex: 0)
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
}
