//
//  FBAccountCell.swift
//  Grubbie
//
//  Created by JABE on 12/7/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

protocol FBAccountCellDelegate {
    func didPressSelect(indexPath: NSIndexPath, isSelected: Bool)
}


class FBAccountCell: UITableViewCell {
    
    @IBOutlet var checkedButton: UIButton!
    @IBOutlet var fbFirstName: UILabel!
    @IBOutlet var fbLastName: UILabel!
    
    
    @IBOutlet var profPicImageView: UIImageView!
    @IBOutlet var profPicActivityIndicator: UIActivityIndicatorView!
    
    var indexPath : NSIndexPath!
    var delegate : FBAccountCellDelegate?
    var isChecked : Bool = false {
        didSet{
            checkedButton.setImage(isChecked ? checkedImage : uncheckedImage, forState: .Normal)
        }
    }

    let checkedImage = UIImage(named: MediaName.TableViewCellCheckedImage.rawValue)
    let uncheckedImage = UIImage(named: MediaName.TableViewCellUncheckedImage.rawValue)
    
    func configure(indexPath : NSIndexPath, appUser: FBAccountViewModel, isChecked: Bool, delegate : FBAccountCellDelegate){
        self.profPicActivityIndicator.startAnimating()
        var profPicPhoto : UIImage?
        ThreadingUtil.inBackground({ () -> Void in
                profPicPhoto = AppUser.getProfPicImage(appUser.fbAccount.fbId)
            }) { () -> Void in
                self.profPicActivityIndicator.stopAnimating()
                if let photo = profPicPhoto {
                    self.profPicImageView.image = photo
                }
        }
        
        self.indexPath = indexPath
        self.isChecked = isChecked
        self.fbFirstName.text = appUser.fbAccount.fbFirstName
        self.fbLastName.text = appUser.fbAccount.fbLastName
        self.delegate = delegate
    }
  
    
    @IBAction func checkedButtonPressed(sender: AnyObject) {
        self.isChecked = !self.isChecked
        if let delegate = self.delegate {
            delegate.didPressSelect(self.indexPath, isSelected: self.isChecked)
        }
    }
}