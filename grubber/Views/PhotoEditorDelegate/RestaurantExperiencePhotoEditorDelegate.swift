//
//  RestaurantExperiencePhotoEditorDelegate.swift
//  grubber
//
//  Created by JABE on 11/20/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class RestaurantExperiencePhotoEditorDelegate : PhotoEditorDelegate {
    func queryCanDeletePhotoSource(photoSource: PhotoSource, completionBlock: (canDelete: Bool, userReason: String, userData: AnyObject?) -> Void) {
        completionBlock(canDelete: true, userReason: "", userData: nil)
    }
    
    
    func onDeletePhoto(photoSource: PhotoSource, completionBlock: (deleted: Bool, postDeleteUserMessage: String?, error: NSError?) -> Void) {
        guard let reviewPhoto = photoSource as? ExperiencePhoto else {
            return
        }
        ExperiencePhoto.remove(reviewPhoto.id, completionBlock: { (success, error) -> Void in
            if error == nil {
                EventPublisher.sharedAppEventPublisher().emit(.RestaurantPhotoDeleted, userData: reviewPhoto.id)
            }
            completionBlock(deleted: error == nil, postDeleteUserMessage: "", error: error)
        })
    }
    
    
    func onSave(photoEditorData : PhotoEditorData, completionBlock: ((success: Bool, isNewObject: Bool, savedObject: AnyObject?) -> Void)?) {

        var updatedReviewPhoto : ExperiencePhoto?
        var isNew = true
        if let photoSource = photoEditorData.photoSource {
            isNew = photoSource.isNew()
        }
        var success = false
        var savedObject : AnyObject!
        if isNew {
            guard let review = photoEditorData.objectContext as? RestaurantExperience  else {
                return
            }
            updatedReviewPhoto = ExperiencePhoto.insert({(reviewPhoto: ExperiencePhoto)
                in
                reviewPhoto.comments = photoEditorData.caption!
                reviewPhoto.experienceId = review.id
                reviewPhoto.photo = photoEditorData.image
                if let audio = photoEditorData.audio {
                    reviewPhoto.audio = audio
                }
            })
            success = true
            savedObject = updatedReviewPhoto!
            if let newPhoto = updatedReviewPhoto {
                EventPublisher.sharedAppEventPublisher().emit(.ExperiencePhotoAdded, userData: newPhoto.id)
            }
            
        } else {
            let photoChanged = photoEditorData.photoSource?.getPhoto() != photoEditorData.image!
            var updateCount = 0
            if let reviewPhoto = photoEditorData.objectContext as? ExperiencePhoto {
                updateCount = ExperiencePhoto.update(reviewPhoto.id, updatePhoto: photoChanged, updateValuesAssignmentBlock: { (experiencePhoto) -> Void in
                    experiencePhoto.experienceId = reviewPhoto.experienceId
                    if let caption  = photoEditorData.caption {
                        experiencePhoto.comments = caption
                    }
                    if photoChanged {
                        if let image = photoEditorData.image {
                            experiencePhoto.photo = image
                        }
                    }
                    if let audio = photoEditorData.audio {
                        experiencePhoto.audio = audio
                    }
                })
                success = updateCount == 1
                savedObject = reviewPhoto
                EventPublisher.sharedAppEventPublisher().emit(.ExperiencePhotoModified, userData: reviewPhoto.id)
            }
        }
        if let completion = completionBlock {
            completion(success: success, isNewObject: isNew, savedObject: savedObject)
        }
    }
}