//
//  RestaurantPhotoEditorDelegate.swift
//  grubber
//
//  Created by JABE on 11/20/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

class RestaurantPhotoEditorDelegate : PhotoEditorDelegate {
    
    func queryCanDeletePhotoSource(photoSource: PhotoSource, completionBlock: (canDelete: Bool, userReason: String, userData: AnyObject?) -> Void) {
        completionBlock(canDelete: true, userReason: "", userData: nil)
    }
    
    func onDeletePhoto(photoSource: PhotoSource, completionBlock: (deleted: Bool, postDeleteUserMessage: String?, error: NSError?) -> Void) {
        guard let restoPhoto = photoSource as? RestaurantPhoto else {
            return
        }

        RestaurantPhoto.remove(restoPhoto.id, completionBlock: { (success, error) -> Void in
            if error == nil {
                EventPublisher.sharedAppEventPublisher().emit(.RestaurantPhotoDeleted, userData: restoPhoto.id)
            }
            completionBlock(deleted: error == nil, postDeleteUserMessage: "", error: error)
        })
    }
    
    func onSave(photoEditorData : PhotoEditorData, completionBlock: ((success: Bool, isNewObject: Bool, savedObject: AnyObject?) -> Void)?) {
        
        var updatedPhoto : RestaurantPhoto?
        var isNew = true
        if let photoSource = photoEditorData.photoSource {
            isNew = photoSource.isNew()
        }
        if isNew {
            guard let restaurant = photoEditorData.objectContext as? Restaurant  else {
                return
            }
            updatedPhoto = RestaurantPhoto.insert({(restaurantPhoto: RestaurantPhoto)
                in
                restaurantPhoto.comments = photoEditorData.caption!
                restaurantPhoto.restaurantId = restaurant.id
                restaurantPhoto.photo = photoEditorData.image
                if let audio = photoEditorData.audio {
                    restaurantPhoto.audio = audio
                }
            })
            if let completionBlock = completionBlock {
                completionBlock(success: true, isNewObject: isNew,  savedObject: updatedPhoto)
            }
            if let _ = updatedPhoto {
                EventPublisher.sharedAppEventPublisher().emit(.RestaurantPhotoAdded, userData: updatedPhoto!.id)
            }
        } else {
            var updateCount = 0
            if let galleryPhoto  = photoEditorData.objectContext as? RestaurantPhoto {
                let updatePhoto = galleryPhoto.photo != photoEditorData.image!
                updateCount = RestaurantPhoto.update(galleryPhoto.id, updatePhoto: updatePhoto,updateValuesAssignmentBlock: { (restoPhoto) -> Void in
                    restoPhoto.restaurantId = galleryPhoto.restaurantId
                    if let caption  = photoEditorData.caption {
                        restoPhoto.comments = caption
                    }
                    if let image = photoEditorData.image {
                        restoPhoto.photo = image
                    }
                    if let audio = photoEditorData.audio {
                        restoPhoto.audio = audio
                    }
                })
                if let completion = completionBlock {
                    completion(success: updateCount == 1, isNewObject: false, savedObject: galleryPhoto)
                }
                if let _ = updatedPhoto {
                    EventPublisher.sharedAppEventPublisher().emit(.RestaurantPhotoModified, userData: updatedPhoto!.id)
                }
            }
        }
    }
}
