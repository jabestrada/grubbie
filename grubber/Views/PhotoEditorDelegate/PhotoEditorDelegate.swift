//
//  PhotoEditorDelegate.swift
//  grubber
//
//  Created by JABE on 11/20/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


struct PhotoEditorData {
    var caption : String?
    var image : UIImage?
    var audio : NSData?
    var objectContext : AnyObject?
    var photoSource: PhotoSource?

}

protocol PhotoEditorDelegate {
    func onSave(photoEditorData : PhotoEditorData, completionBlock: ((success: Bool, isNewObject: Bool, savedObject: AnyObject?) -> Void)?)
    func queryCanDeletePhotoSource(photoSource: PhotoSource, completionBlock: (canDelete: Bool, userReason: String, userData: AnyObject?) -> Void)
    func onDeletePhoto(photoSource: PhotoSource, completionBlock: (deleted: Bool, postDeleteUserMessage: String?, error: NSError?) -> Void)
    
}

