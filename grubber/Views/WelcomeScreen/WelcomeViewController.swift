//
//  WelcomeViewController.swift
//  Grubbie
//
//  Created by JABE on 11/28/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import Foundation

class WelcomeViewController : UIViewController {

    @IBOutlet var textContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        JLThemes.formatContainerView(textContainer)
        self.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
    }
    
    
    @IBAction func getStartedPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("segueToHome", sender: nil)
//        Util.closeController(self)
    }
}

