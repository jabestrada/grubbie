//
//  FullScreenPhotoViewController.swift
//  myEats
//
//  Created by JABE on 11/11/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


class FullScreenPhotoViewController: UIViewController {
    var image : UIImage?
    
    @IBOutlet var backgroundView: UIView!
    var imageView : UIImageView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    // www.raywenderlich.com/76436/use-uiscrollview-scroll-zoom-content-swift
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = self.image {
            scrollView.delegate = self
            
            self.imageView = UIImageView(image: image)
            self.imageView.userInteractionEnabled = true
            imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: image.size)
            scrollView.addSubview(imageView)
            scrollView.contentSize = image.size
            
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewDoubleTapped:")
            doubleTapRecognizer.numberOfTapsRequired = 2
            doubleTapRecognizer.numberOfTouchesRequired = 1
            scrollView.addGestureRecognizer(doubleTapRecognizer)
            
            let scrollViewFrame = scrollView.frame
            let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
            let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
            let minScale = min(scaleWidth, scaleHeight)
            scrollView.minimumZoomScale = minScale
            scrollView.maximumZoomScale = 1.0
            scrollView.zoomScale = minScale
            
            centerScrollViewContents()
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: "backgroundTapped:")
            backgroundView.addGestureRecognizer(tapRecognizer)
            
            let imageTapRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped:")
            self.imageView.addGestureRecognizer(imageTapRecognizer)

        }

    }
    
    func imageTapped(recognizer: UITapGestureRecognizer) {
        // Do nothing; intercept tap on images to prevent tap event from bubbling up to backgroundView.
    }
    
    func backgroundTapped(recognizer: UITapGestureRecognizer){
        if recognizer.view == backgroundView {
            print("backgroundTapped")
            Util.closeController(self)
        }
    }
    
    func scrollViewDoubleTapped(recognizer: UITapGestureRecognizer) {
        let pointInView = recognizer.locationInView(imageView)
        var newZoomScale = scrollView.zoomScale * 1.5
        newZoomScale = min(newZoomScale, scrollView.maximumZoomScale)
        let scrollViewSize = scrollView.bounds.size
        let w = scrollViewSize.width / newZoomScale
        let h = scrollViewSize.height / newZoomScale
        let x = pointInView.x - (w / 2.0)
        let y = pointInView.y - (h / 2.0)
        
        let rectToZoomTo = CGRectMake(x, y, w, h)
        scrollView.zoomToRect(rectToZoomTo, animated: true)
    }
    
    
    func centerScrollViewContents(){
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        imageView.frame = contentsFrame
        
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
}


// UIScrollViewDelegate
extension FullScreenPhotoViewController : UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
//    func scrollViewDidZoom(scrollView: UIScrollView) {
//        centerScrollViewContents()
//    }
}