//
//  PhotoEditorViewController.swift
//  Grubber
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


extension PhotoEditorViewController : AudioRecordingDelegate {
    func doneRecording(audioData: NSData?) {
        self.audioData = audioData
    }
}

class PhotoEditorViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var captionTextView: UITextView!
//    @IBOutlet var photoCaptionLabel: UILabel!
    @IBOutlet var photoSourceOption: UISegmentedControl!
    @IBOutlet var addEditAudioButton: UIButton!
    @IBOutlet var playStopButton: UIButton!
    
    var delegate : AppModelEditorDelegate?
    var photoEditorDelegate : PhotoEditorDelegate?
    
    var activityIndicator : JLActivityIndicator!
    
    var onSaveObjectContext : AnyObject?

    var photoSource : PhotoSource?
    var audioPlayer = JLAudioPlayer()
    
    var isPlayingAudio  = false {
        didSet {
            playStopButton.setImage(UIImage(named:  isPlayingAudio ? MediaName.PhotoEditorStopButtonIcon.rawValue : MediaName.PhotoEditorPlayButtonIcon.rawValue), forState: .Normal)
        }
    }
    
    var validators = [JLControlValidator]()
    var audioData : NSData? {
        didSet{
            onAudioDataChanged()
        }
    }
    
    
    override func viewDidLoad() {
        playStopButton.setTitle("", forState: .Normal)
        self.hideKeyboardOnTap()
        validators.append(RequiredFieldValidator(controlToValidate: imageView, errorMessage: "Please choose a photo"))
        JLThemes.apply(captionTextView)
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: false)
        photoSourceOption.selectedSegmentIndex = -1
        
        if let photoSource = self.photoSource {
            self.captionTextView.hidden = false
            self.captionTextView.text = photoSource.getComments()
            self.imageView.image = photoSource.getPhoto()
            self.audioData = photoSource.getAudio()
        }
        
        onAudioDataChanged()
    }
    
    func onAudioDataChanged(){
        if let _ = self.audioData {
            addEditAudioButton.setTitle("Edit Audio", forState: .Normal)
            playStopButton.hidden = false
        }
        else {
            addEditAudioButton.setTitle("Add Audio", forState: .Normal)
            playStopButton.hidden = true
        }
        isPlayingAudio = false
        addEditAudioButton.hidden = self.imageView.image == nil
        
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func addEditButtonPressed(sender: AnyObject) {
        UIFactory.createAudioRecordingController(self, audioData: self.audioData, delegate: self)
        
    }
    
    
    @IBAction func playStopButtonPressed(sender: AnyObject) {
        if isPlayingAudio {
            self.audioPlayer.stop()
            self.isPlayingAudio = false
        }
        else {
            if let audioData = self.audioData {
                isPlayingAudio = true
                self.audioPlayer.play(audioData) { (player, successfully) -> Void in
                    self.isPlayingAudio = false
                }
            }
        }
    }
    
    @IBAction func photoSourceOptionChanged(sender: AnyObject) {
        let cameraViewController = UIImagePickerController()
        cameraViewController.delegate = self
        
        if photoSourceOption.selectedSegmentIndex == 0 {
            cameraViewController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        else {
            cameraViewController.sourceType = UIImagePickerControllerSourceType.Camera
        }
        self.presentViewController(cameraViewController, animated: true, completion: { self.photoSourceOption.selectedSegmentIndex = -1})
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        guard let delegate = self.delegate, photoEditorDelegate = self.photoEditorDelegate  else {
            return
        }

        if !self.isValid(){
            return
        }
        
        self.activityIndicator.startAnimation()
        ThreadingUtil.inBackground({
            photoEditorDelegate.onSave(PhotoEditorData(caption: self.captionTextView.text, image: self.imageView.image,  audio: self.audioData, objectContext: self.onSaveObjectContext, photoSource: self.photoSource), completionBlock: { (success, isNewInstance, savedObject) -> Void in
                self.activityIndicator.stopAnimation()
                if isNewInstance {
                    delegate.didAddInstance(self, instance: savedObject)
                }
                else {
                    delegate.didUpdateInstance(self, instance: savedObject)
                }
                Util.closeController(self)
                })
        }, callBack: nil)
    }
    
    func isValid() -> Bool {
        var result = true
        for validator in self.validators {
            if !validator.isValid() {
                Util.showMessage(self, msg: validator.errorMessage, title: UserDialogTitle.Info.rawValue)
                result = false
                break
            }
        }
        return result
    }
}


extension PhotoEditorViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imageView.image = image
        self.captionTextView.hidden = false
//        self.photoCaptionLabel.hidden = false
        onAudioDataChanged()
        Util.closeController(picker)
    }
}
