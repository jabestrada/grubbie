//
//  PhotoListCell.swift
//  Grubber
//
//  Created by JABE on 11/10/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

protocol PhotoCellDelegate {
    func imageTapped(image: UIImage)
    func postToFBTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?)
    func audioButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?)
    func deleteButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?)
    func editButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?)
}

class PhotoListCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var commentsLabel: UITextView!
    @IBOutlet var postToFbButton: UIButton!
    @IBOutlet var audioButton: UIButton!
    
    @IBOutlet var photoFetchActivityIndicator: UIActivityIndicatorView!
    
    var delegate : PhotoCellDelegate?
    var indexPath : NSIndexPath!
    
    func configure(photoSource: PhotoSource, indexPath: NSIndexPath,  delegate : PhotoCellDelegate){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.cornerRadius = 8
        
        
//        self.imageView.image = photoSource.getPhoto()
        photoFetchActivityIndicator.startAnimating()
        photoSource.getPhotoInBackground { (image, error) -> Void in
            self.imageView.image = image
            self.photoFetchActivityIndicator.stopAnimating()
        }
        
//        self.imageView.layer.borderWidth = 1.0
//        self.imageView.layer.cornerRadius = CGFloat(15.0)
        self.commentsLabel.text = photoSource.getComments()
        
        imageView.userInteractionEnabled = true
        
        self.indexPath = indexPath
        self.delegate = delegate
        
        audioButton.setTitle("", forState: .Normal)
        if let audio = photoSource.getAudio(){
//            audioButton.enabled = audio.length > 0
            if audio.length > 0 {
                audioButton.setImage( UIImage(named: MediaName.PhotoEditorPlayButtonIcon.rawValue), forState: .Normal)
                audioButton.enabled = true
            } else {
                audioButton.setImage( UIImage(named: MediaName.NoAudio.rawValue), forState: .Normal)
                audioButton.enabled = false
            }
        }
        else {
            audioButton.setImage( UIImage(named: MediaName.NoAudio.rawValue), forState: .Normal)
            audioButton.enabled = false
        }
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        imageView.addGestureRecognizer(gestureRecognizer)
    }

    @IBAction func editButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.editButtonTapped(self.indexPath, completionCallback: nil)
        }
    }
    
    @IBAction func deleteButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.deleteButtonTapped(self.indexPath, completionCallback: nil)
        }
    }
    
    @IBAction func audioButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            self.audioButton.enabled = false
            delegate.audioButtonTapped(self.indexPath, completionCallback: {
                self.audioButton.enabled = true
            })
        }
    }

    @IBAction func postToFBTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.postToFBTapped(self.indexPath, completionCallback: nil)
        }
    }
    
    func imageTapped(img: AnyObject){
        if let delegate = self.delegate {
            delegate.imageTapped(imageView.image!)
        }
    }

}
