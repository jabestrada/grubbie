//
//  PhotoListDataSource.swift
//  grubber
//
//  Created by JABE on 11/20/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


protocol PhotoListDataSource {
    var noPhotoMessage : String { get set }
    var listTitle : String { get set }
    func getPhotos() -> [PhotoSource]
    
    func getPhotoEditorDelegate() -> PhotoEditorDelegate?
    func getOnNewObjectContext() -> AnyObject?
}
