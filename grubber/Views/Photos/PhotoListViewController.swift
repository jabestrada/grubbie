//
//  PhotoListViewController.swift
//  Grubber
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


class PhotoListViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noResultsView: UIView!
    @IBOutlet var restaurantNameLabel: UILabel!
    
    var activityIndicator : JLActivityIndicator!
    var photoSourceDelegate : PhotoListDataSource!
    var onNewObjectContext : AnyObject?
    
    var photos = [PhotoSource]()
    
    var fbAdapter = Factory.createFBAdapter()
    
    var audioPlayer = JLAudioPlayer()
    
    override func viewDidLoad() {
        JLThemes.apply(restaurantNameLabel)
        // Save newObjectContext and pass it when New button is activated.
        self.onNewObjectContext = self.photoSourceDelegate?.getOnNewObjectContext()
        
        Util.setNoResultsViewMessage(noResultsView, message: (photoSourceDelegate?.noPhotoMessage)!)
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.collectionView)
        collectionView.dataSource = self

        restaurantNameLabel.text = photoSourceDelegate.listTitle
        loadPhotos()
    }
    

    
    func loadPhotos(){
        guard let photoSourceDelegate = self.photoSourceDelegate else {
            return
        }
        self.activityIndicator.startAnimation()
        ThreadingUtil.inBackground({
                self.photos = photoSourceDelegate.getPhotos()
            }) {
                    self.collectionView.reloadData()
                    if self.photos.count == 0 {
                        self.collectionView.hidden = true
                        self.noResultsView.hidden = false
                    } else {
                        self.collectionView.hidden = false
                        self.noResultsView.hidden = true
                    }
                    self.activityIndicator.stopAnimation()
                }
    }
    
    
    @IBAction func newPhotoButtonPressed(sender: AnyObject) {
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.PhotoEditor) { (uiViewController) -> Void in
            if let photoEditorVc = uiViewController as? PhotoEditorViewController {
                photoEditorVc.photoEditorDelegate = self.photoSourceDelegate.getPhotoEditorDelegate()
                photoEditorVc.onSaveObjectContext = self.onNewObjectContext
                photoEditorVc.delegate = self
            }
        }
    }
    
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
}


// MARK: PhotoCellDelegate
extension PhotoListViewController : PhotoCellDelegate {
    func imageTapped(image: UIImage) {
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.FullPhotoViewer) { (uiViewController) -> Void in
            if let view = uiViewController as? FullScreenPhotoViewController {
                view.image = image
            }
        }
//        UIFactory.createFullScreenPhotoViewController(self, image: image)
    }
    
    func editButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?) {
        let photoSource = self.photos[indexPath.row]
        UIFactory.showStoryboard(self, storyboardId: StoryboardId.PhotoEditor) { (uiViewController) -> Void in
            if let photoEditorVc = uiViewController as? PhotoEditorViewController {
                photoEditorVc.photoEditorDelegate = self.photoSourceDelegate.getPhotoEditorDelegate()
                photoEditorVc.photoSource = photoSource
                photoEditorVc.delegate = self
                photoEditorVc.onSaveObjectContext = photoSource
            }
        }
    }
    
    
    func deleteButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?) {
        guard let photoEditorDelegate = self.photoSourceDelegate.getPhotoEditorDelegate() else {
            print("deleteButtonTapped: No photoEditorDelegate")
            return
        }
        let photoSource = self.photos[indexPath.row]
        photoEditorDelegate.queryCanDeletePhotoSource(photoSource) { (canDelete, userReason, userData) -> Void in
            if !canDelete {
                Util.showMessage(self, msg: userReason.isEmpty ? "Sorry, cannot delete this photo at the moment." : userReason, title: "")
            } else {
                let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to delete this photo?", preferredStyle: .ActionSheet)
                let cancelAction = UIAlertAction(title: "Nope, I changed my mind", style: .Cancel, handler: nil)
                actionSheetController.addAction(cancelAction)
                let deleteReviewAction = UIAlertAction(title: "Yup, go ahead and delete", style: UIAlertActionStyle.Destructive) { action -> Void in
                    photoEditorDelegate.onDeletePhoto(photoSource, completionBlock: { (deleted, postDeleteUserMessage, error) -> Void in
                        var userMsg =  error == nil ? "Photo was successfully deleted" : "Error deleting photo. Please try again later."
                        if let postDeleteMsg = postDeleteUserMessage {
                            if !postDeleteMsg.isEmpty {
                                userMsg = postDeleteMsg
                            }
                        }
                        if error == nil {
                            Util.showMessage(self, msg: userMsg, title: "", completionBlock: {(alertAction) in
                                self.loadPhotos()
                            })
                        }
                        else {
                            print(error)
                            Util.showMessage(self, msg: userMsg, title: UserDialogTitle.Error.rawValue, completionBlock: nil)
                        }
                        
                    })
                }
                actionSheetController.addAction(deleteReviewAction)
                self.presentViewController(actionSheetController, animated: true, completion: nil)
            }
        
        }
    }
    
    func postToFBTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)? )  {
        let reviewPhoto = self.photos[indexPath.row]
        var comments = reviewPhoto.getComments()
        if comments.isEmpty {
            comments = reviewPhoto.getRestaurantName()
        }
        else {
            comments = comments + ". " + reviewPhoto.getRestaurantName()
        }
        
        var photoType = ""
        switch reviewPhoto.getPhotoType() {
        case PhotoSourceType.ExperiencePhoto:
            photoType = ExperiencePhoto.dataSourceEntityName
            break;
        case PhotoSourceType.RestaurantPhoto:
            photoType = RestaurantPhoto.dataSourceEntityName
            break;
        default:
            photoType = "Unknown"
            break;
        }
        
        let post = SocialPost(refId: reviewPhoto.getId(), objectType: photoType, photo: reviewPhoto.getPhoto(), caption: "\(reviewPhoto.getComments()). \(reviewPhoto.getRestaurantName()).")

        UIFactory.showStoryboard(self, storyboardId: .PostToSocial, configureBlock: { (uiViewController) -> Void in
            if let viewController = uiViewController as? PostToSocialViewController {
                viewController.post = post
            }
        })
    }
    
    func audioButtonTapped(indexPath: NSIndexPath, completionCallback: (() -> Void)?) {
        let photo = self.photos[indexPath.row]
        if let audioData = photo.getAudio() {
            let outsideCompletionBlock = completionCallback
            self.audioPlayer.play(audioData, completionBlock: { (player, successfully) -> Void in
                if let outsideCompletionBlock = outsideCompletionBlock {
                    outsideCompletionBlock()
                }
            })
            
        }
        else {
            print("No audio for this photo")
        }
    }
}


// MARK: UICollectionViewDataSource
extension PhotoListViewController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! PhotoListCell
        let reviewPhoto = photos[indexPath.row]
        
        cell.configure(reviewPhoto, indexPath: indexPath, delegate: self)
        return cell
    }
}


// MARK: AppModelEditorDelegate
extension PhotoListViewController : AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {
        loadPhotos()
    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {
        loadPhotos()
    }
}

