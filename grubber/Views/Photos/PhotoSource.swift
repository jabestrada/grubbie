//
//  PhotoSource.swift
//  Grubbie
//
//  Created by JABE on 12/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


@objc class PhotoSourceInfo : NSObject {
    var restaurantName = ""
    var townCity = ""
    var country = ""
    
    init(restaurantName: String, townCity: String, country: String){
        self.restaurantName = restaurantName
        self.townCity = townCity
        self.country = country
    }
}

@objc enum PhotoSourceType : Int {
    case RestaurantPhoto = 1
    case ExperiencePhoto = 2
    case Adhoc = 3
}

@objc protocol PhotoSource {
    func isNew() -> Bool
    func getRestaurantName() -> String
    func getId() -> String
    func getPhotoType() -> PhotoSourceType
    func getPhoto() -> UIImage
    func getPhotoPfFile() -> PFFile?
    func getPhotoInBackground(completion: (image: UIImage?, error: NSError?) -> Void)
    func getComments() -> String
    func getAudio() -> NSData?
    func getPhotoSourceInfoInBackground(completion: (photoSourceInfo: PhotoSourceInfo?, error: NSError?) -> Void)
}