//
//  PostToSocialViewController.swift
//  grubber
//
//  Created by JABE on 11/16/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//





public class PostToSocialViewController: UIViewController {
    @IBOutlet var captionTextView: UITextView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var postPrivacy: UISegmentedControl!

    @IBOutlet var backgroundView: UIView!
    @IBOutlet var removePhotoButton: UIButton!
    
    var activityIndicator : JLActivityIndicator!
    var post : SocialPost!
    var fbAdapter = Factory.createFBAdapter()
    
    
    public override func viewDidLoad() {
        postPrivacy.selectedSegmentIndex = 1
        JLThemes.apply(captionTextView)
        self.hideKeyboardOnTap()
        
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.imageView, startAnimating: false)
        
        
        imageView.userInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        imageView.addGestureRecognizer(gestureRecognizer)
        
        loadPost()
        ThreadingUtil.inBackgroundAfter({ () -> Void in
            if !self.fbAdapter.isUserLoggedThruFacebook(){
                Util.showMessage(self, msg: "Sorry, you cannot post to Facebook because you didn't log in to Grubbie via Facebook.", title: "", completionBlock: { (alertAction) -> Void in
                    Util.closeController(self)
                })
            }
            }, afterDelay: 0.5)
        
        print("post.refId: \(post.refId)")
        print("post.objectType: \(post.objectType)")
    }
    

    func imageTapped(img: AnyObject){
        if let image = imageView.image {
            UIFactory.showStoryboard(self, storyboardId: StoryboardId.FullPhotoViewer, configureBlock: { (uiViewController) -> Void in
                if let view = uiViewController as? FullScreenPhotoViewController {
                    view.image = image
                }
            })
        }
    }
    
    
    func loadPost(){
        captionTextView.text = ""
        imageView.image = nil

        captionTextView.text = post.caption
        if let photo = post.photo {
            imageView.image = photo
            removePhotoButton.hidden = false
        } else {
            removePhotoButton.hidden = true
        }
    }
    
    @IBAction func removePhotoPressed(sender: AnyObject) {
        imageView.image = nil
        removePhotoButton.hidden = true
    }
    
    @IBAction func resetPressed(sender: AnyObject) {
        loadPost()
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        Util.closeController(self)
        
    }
    
    func getPrivacySetting() -> PostPrivacy {
        switch postPrivacy.selectedSegmentIndex{
        case 0:
            return PostPrivacy.OnlyMe
        case 1:
            return PostPrivacy.Friends
        case 2:
            return PostPrivacy.Everyone
        default:
            return PostPrivacy.OnlyMe
        }
    }
    
    @IBAction func postPressed(sender: AnyObject) {
        let caption = self.captionTextView?.text
        let image = self.imageView?.image
        
        if (caption == nil || caption!.isEmpty) && image == nil {
            Util.showMessage(self, msg: "Caption and photo cannot be both empty.", title: UserDialogTitle.Info.rawValue)
            return
        }
        
        self.activityIndicator.startAnimation()
        self.fbAdapter.queryFbPublishPermission(self) { (hasPublishPermissions, error) -> Void in
                if hasPublishPermissions {
                    ThreadingUtil.inBackground({
                        }) {
                            if image == nil{
                                self.fbAdapter.postStatus(FBParams(message: caption!, photo: nil, privacy: self.getPrivacySetting()),
                                    completionBlock: { (result, error) -> Void in
                                        self.onPostToFb(result, error: error)
                                })
                            }
                            else {
                                let photoData = UIImageJPEGRepresentation(image!, 1.0)!
                                self.fbAdapter.postPhoto(FBParams(message: caption ?? "", photo: photoData, privacy: self.getPrivacySetting()),
                                    completionBlock: { (result, error) -> Void in
                                        self.onPostToFb(result, error: error)
                                })
                            }
                  }
                }
                else {
                    self.activityIndicator.stopAnimation()
                    Util.showMessage(self, msg: UserMessage.ErrorInsufficientPublishToFbPermission.rawValue, title: UserDialogTitle.Error.rawValue)
                }
            }
    }
    
    
    func onPostToFb(result: AnyObject?, error: NSError?){
        self.activityIndicator.stopAnimation()
        if error == nil {
            if let resultDictionary = result as? NSDictionary {
                if let postId = resultDictionary["id"] as? String {
                    FbPostAudit.insert({ (fbPostAudit) -> Void in
                        fbPostAudit.fbRefId = postId
                        fbPostAudit.objectType = self.post.objectType
                        fbPostAudit.appRefId = self.post.refId
                        fbPostAudit.postedBy  = Util.getCurrentUserId()
                        fbPostAudit.postedOn = NSDate()
                    })
                }
            }
            Util.showMessage(self, msg: UserMessage.GenericSuccessfullyPostedToFb.rawValue, title: UserDialogTitle.Success.rawValue)
                { (UIAlertAction) in
                    Util.closeController(self)
                }

        } else {
            print("PostToFb error: \(error)")
            Util.showMessage(self, msg: UserMessage.ErrorPostingToFacebook.rawValue, title: UserDialogTitle.Error.rawValue)
        }
    
    }
}
