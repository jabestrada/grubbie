//
//  MyNearbyEatsListViewController.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//



extension MyNearbyEatsListViewController : AppEventSubscriber {
    func onEvent(eventType: AppEventType, eventTimeStamp: NSDate, userData: AnyObject?) {
        switch eventType {
            case .RestaurantAdded, .RestaurantModified, .RestaurantDeleted:
                self.onRefreshList()
            break;
            default:
            print("MyNearbyEatsListViewController + AppEventSubscriber: Unhandled event \(eventType)")
        }
    }
}
class MyNearbyEatsListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var kilometersFilterTextField: UITextField!
    
    @IBOutlet var noResultsView: UIView!
    
    var configProvider = Factory.createConfigProvider()
    
    var restaurants = [Restaurant]() {
        didSet{
            onRestaurantsChanged()
        }
    }
    

    
    func onRestaurantsChanged(){
        if self.restaurants.count > 0 {
            self.tableView.hidden = false
            self.noResultsView.hidden = true
        }
        else {
            self.tableView.hidden = true
            self.noResultsView.hidden = false
        }
    }
    
    
    var currentLocation : CLLocation?
    
    var jlActivityIndicator : JLActivityIndicator!
    
    override func viewDidLoad() {
        print("MyNearbyEatsListViewController.viewDidLoad")
        EventPublisher.sharedAppEventPublisher().addSubscriber(self, eventTypes: .RestaurantAdded, .RestaurantModified, .RestaurantDeleted)

        self.jlActivityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.tableView)
        
        let formatter = NSNumberFormatter()
        formatter.maximumFractionDigits = 0
        kilometersFilterTextField.text = formatter.stringFromNumber(configProvider.nearbyEatsDistanceFromFilter)
        
        tableView.delegate = self
        tableView.dataSource = self
        onRefreshList()
    }
    
    
    
    func onRefreshList() {
        self.jlActivityIndicator.startAnimation()
        var userHasRestaurant = false
        print("startOnRefreshList")
        ThreadingUtil.inBackground({ () -> Void in
                userHasRestaurant = Restaurant.doesUserHaveRestaurant()
            }) {
                if userHasRestaurant{
                    let milesFilter = self.configProvider.nearbyEatsDistanceFromFilter / 1.609
                    AppGlobals.sharedLocation { (location) -> Void in
                        let locationPredicate = JLLocationPredicateValue(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, withinMiles: milesFilter)
                        Restaurant.getRestaurantsOfUser(nil, contains: String?.None, locationPredicateValue: locationPredicate) { (restaurants: [Restaurant]) -> Void in
                            self.restaurants = restaurants
                            self.currentLocation = location
                            self.tableView.reloadData()
                            if restaurants.count == 0 {
                                Util.setNoResultsViewMessage(self.noResultsView, message: UserMessage.NoNearbyRestaurantSearchResult.rawValue)
                            }
                            self.jlActivityIndicator.stopAnimation()
                        }
                    }
                }
                else {
                    Util.setNoResultsViewMessage(self.noResultsView, message: UserMessage.NoRestaurantAtAll.rawValue)
                    self.jlActivityIndicator.stopAnimation()
                    self.onRestaurantsChanged()
                }
        }
    }
    
    
    @IBAction func onRefreshPressed(sender: AnyObject) {
        self.view.endEditing(true)
        if let kmFilter = Double(kilometersFilterTextField.text!) {
            configProvider.nearbyEatsDistanceFromFilter = kmFilter
        }
        onRefreshList()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let restaurantHomeVc = segue.destinationViewController as? RestaurantTabBarController {
            restaurantHomeVc.restaurant = sender as? Restaurant
        }
    }
    
    func segueForRestaurant(restaurant: Restaurant) {
        self.performSegueWithIdentifier("segueToRestaurantHome", sender: restaurant)
    }
}


// MARK: UITableViewDataSource and UITableViewDelegate
extension MyNearbyEatsListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCellWithIdentifier("cell") as? RestaurantListingCell)!
        let restaurant = self.restaurants[indexPath.row]
        cell.configureCell(restaurant, location: self.currentLocation, delegate: self)
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       segueForRestaurant(restaurants[indexPath.row])
    }
    
}

// MARK: RestaurantListingCellDelegate
extension MyNearbyEatsListViewController : RestaurantListingCellDelegate {
    func didPressPhoneNumberButton(sender: AnyObject, rawPhoneNumber: String) {
        Util.dial(self, rawNumber: rawPhoneNumber)
    }
}

