//
//  RestaurantMainDetailEditorController.swift
//  myEats
//
//  Created by JABE on 11/5/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit


// MARK: AppModelEditorDelegate
extension RestaurantMainDetailEditorController : AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {
        
    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {
        if let geoData = instance as? JLGeoData {
            self.location = geoData.location
            if let address = restaurantAddressTextField.text {
                if address.isEmpty {
                    restaurantAddressTextField.text = geoData.street
                }
            }
            townCityTextField.text = geoData.townCity
            restaurantCountryTextField.text = geoData.country
        }
        else {
            ThreadingUtil.inMainQueue({ () -> Void in
                Util.showMessage(self, msg: "Geo data was not available. Please enter the restaurant's location details manually.", title: "")
            })
        }
    }
}

class RestaurantMainDetailEditorController: UIViewController {
    @IBOutlet var restaurantNameTextField: UITextField!
    @IBOutlet var restaurantAddressTextField: UITextField!
    @IBOutlet var restaurantPhoneTextField: UITextField!
    @IBOutlet var restaurantEmailTextField: UITextField!
    @IBOutlet var restaurantCountryTextField: UITextField!
    @IBOutlet var currentGeoLabel: UILabel!
    @IBOutlet var townCityTextField: UITextField!

    @IBOutlet weak var deleteButton: UIButton!
    
    var delegate : AppModelEditorDelegate?
    var restaurant : Restaurant!
    var location : CLLocation?
    var activityIndicator : JLActivityIndicator!
    var validators = [JLControlValidator]()
    
    @IBAction func deleteButtonPressed(sender: AnyObject) {
        Util.getConfirmation(self, message: "Are you sure you want to delete this restaurant record? All reviews/photos associated with this restaurant will be deleted as well, and this action cannot be undone.", title: "", yesCaption: "Yup, go ahead and delete", noCaption: "Nope, I changed my mind") { (alertAction) -> Void in
            self.activityIndicator.startAnimation()
            Restaurant.remove(self.restaurant.id) { (success, error) -> Void in
                self.activityIndicator.stopAnimation()
                if success {
                    EventPublisher.sharedAppEventPublisher().emit(.RestaurantDeleted, userData: self.restaurant.id)
                    Util.showMessage(self, msg: "Restaurant was successfully deleted", title: "", completionBlock: { (alertAction) -> Void in
                        self.performSegueWithIdentifier("goHomeSegue", sender: nil)
                    })
                }
                else {
                    Util.showMessage(self, msg: "There was a problem while deleting the restaurant record. Please try again later.", title: "", completionBlock: { (alertAction) -> Void in
                    })
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        self.hideKeyboardOnTap()
        validators.append(RequiredFieldValidator(controlToValidate: restaurantNameTextField!, errorMessage: UserMessage.RestaurantNameRequired.rawValue))
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: true)
        
        if let restaurant = self.restaurant {
            restaurantNameTextField.text = restaurant.name
            restaurantAddressTextField.text = restaurant.address
            restaurantPhoneTextField.text = restaurant.phone
            restaurantEmailTextField.text = restaurant.email
            townCityTextField.text = restaurant.townCity
            restaurantCountryTextField.text = restaurant.country
            currentGeoLabel.hidden = true
            deleteButton.hidden = false
            self.location = CLLocation(latitude: restaurant.lat, longitude: restaurant.lng)
            self.activityIndicator.stopAnimation()
        } else {
            currentGeoLabel.hidden = false
            deleteButton.hidden = true
            AppGlobals.sharedPlacemark { (placemarks, error) -> Void in
                if placemarks != nil {
                    let geoData = JLGeoData(placemarks: placemarks)
                    self.location = geoData.location
                    self.restaurantCountryTextField.text = geoData.country
                    self.restaurantAddressTextField.text = geoData.street
                    self.townCityTextField.text = geoData.townCity
                } else {
                    AppGlobals.sharedLocation({ (location) -> Void in
                        self.location = location
                    })
                }
                self.activityIndicator.stopAnimation()
            }
        }
    }
    
    
    @IBAction func onCancelPressed(sender: AnyObject) {
            Util.closeController(self)
    }
    
    @IBAction func onSavedPressed(sender: AnyObject) {
        onSave()
    }

    func onSave(){
        if let _ = self.delegate {
            if !self.isValid(){
                return
            } else {
                let restaurantName = self.restaurantNameTextField.text!
                var existingRestaurant : Restaurant?
                self.activityIndicator.startAnimation()
                if self.isNewRestaurant() {
                    ThreadingUtil.inBackground({
                            existingRestaurant = Restaurant.getByName(restaurantName)
                        }, callBack: {
                            self.activityIndicator.stopAnimation()
                            if let existingRestaurant = existingRestaurant {
                                self.showExistingRestaurantAlert(existingRestaurant)
                                
                            } else {
                                self.doSave()
                            }
                    })
                } else {
                    ThreadingUtil.inBackground({ () -> Void in
                            existingRestaurant = Restaurant.getByName(restaurantName)
                        }, callBack: {
                            self.activityIndicator.stopAnimation()
                            if let existingRestaurant = existingRestaurant {
                                if existingRestaurant.id != self.restaurant.id {
                                    Util.showMessage(self, msg: "\(restaurantName) already exists in your Grubs list.", title: "", completionBlock: nil)
                                } else {
                                    self.doSave()
                                }
                            } else {
                                self.doSave()
                            }
                    })
                }
            }
        }
    }
    
    
    func showExistingRestaurantAlert(restaurant: Restaurant) {
        guard let delegate = self.delegate else {
            return
        }
        
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "\(restaurant.name) already exists in your Grubs list.", preferredStyle: .ActionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Change the name of this entry", style: .Default, handler: nil)
        actionSheetController.addAction(cancelAction)

        let useExistingRestaurantAction: UIAlertAction = UIAlertAction(title: "Use your existing restaurant entry", style: .Default) { action -> Void in
            delegate.didAddInstance(self, instance: restaurant)
            Util.closeController(self)
        }
        actionSheetController.addAction(useExistingRestaurantAction)
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)

    }
    
    func isNewRestaurant() -> Bool {
        return self.restaurant == nil || self.restaurant.isNew()
    }
    
    func doSave(){
        guard let delegate = self.delegate else {
            return
        }
        
        self.activityIndicator.startAnimation()
        if self.isNewRestaurant() {
            var newRestaurant : Restaurant!
            ThreadingUtil.inBackground({
                newRestaurant = Restaurant.insert({(restaurant) in
                    self.gatherRestaurant(restaurant)
                })
                }, callBack: {
                    self.activityIndicator.stopAnimation()
                    EventPublisher.sharedAppEventPublisher().emit(.RestaurantAdded, userData: newRestaurant.id)
                    delegate.didAddInstance(self, instance: newRestaurant)
                    Util.closeController(self)
            })
        }
        else {
            var updateCount = -1
            ThreadingUtil.inBackground({
                updateCount = Restaurant.update(self.restaurant.id, updateValuesAssignmentBlock: { (restaurant) -> Void in
                    self.gatherRestaurant(restaurant)
                })
                }, callBack: {
                    self.activityIndicator.stopAnimation()
                    if updateCount > 0 {
                        EventPublisher.sharedAppEventPublisher().emit(.RestaurantModified, userData: self.restaurant.id)
                        Util.showMessage(self, msg: "Restaurant was successfully updated", title: UserDialogTitle.Success.rawValue, completionBlock: { (alertAction) -> Void in
                            delegate.didUpdateInstance(self, instance: self.restaurant)
                            Util.closeController(self)
                        })
                    } else {
                        Util.showMessage(self, msg: "Restaurant was not updated. Please try again later.", title: UserDialogTitle.Error.rawValue)
                    }
            })

        }
    }
    
    
    func gatherRestaurant(restaurant: Restaurant) {
        restaurant.name = self.restaurantNameTextField.text!
        restaurant.address = self.restaurantAddressTextField.text!
        restaurant.phone = self.restaurantPhoneTextField.text!
        restaurant.email = self.restaurantEmailTextField.text!
        restaurant.townCity = self.townCityTextField.text!
        restaurant.country = self.restaurantCountryTextField.text!
        if let location = self.location {
            restaurant.lat = location.coordinate.latitude
            restaurant.lng = location.coordinate.longitude
            
        }
        else {
            print("Location not available. Defaulting to 0.")
        }
    }
    
    @IBAction func geoCoordinatesPressed(sender: AnyObject) {
        guard let location = self.location  else {
            return
        }
        UIFactory.createGeoEditorController(self, location: location, delegate: self)
    }
    
    
    func isValid() -> Bool {
        for validator in self.validators {
            if !validator.isValid() {
                Util.showMessage(self, msg: validator.errorMessage, title: UserDialogTitle.Info.rawValue)
                return false
            }
        }
        return true
    }
}












