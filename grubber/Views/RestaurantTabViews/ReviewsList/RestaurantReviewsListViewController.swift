//
//  RestaurantReviewsViewController.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


class RestaurantReviewsListViewController: UIViewController, RestaurantTabBarViewProtocol, PhotoListDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var restaurantNameLabel: UILabel!
    @IBOutlet var noResultsView: UIView!
    
    var appModelEventsDelegate : AppModelEditorDelegate?
    var fbAdapter = Factory.createFBAdapter()
    var location : CLLocation!
    var activityIndicator : JLActivityIndicator!
    var dateFormatter = Factory.createDateFormatter()
    var restaurant : Restaurant! {
        didSet {
            loadRestaurantReviews()
        }
    }
    
    var reviews = [RestaurantExperience]() {
        didSet{
            if reviews.count == 0 {
                tableView.hidden = true
                noResultsView.hidden = false
            }
            else {
                tableView.hidden = false
                noResultsView.hidden = true
            }
        }
    }
    func onReady() {
        // TODO: ...
    }
    override func viewDidLoad() {
        restaurantNameLabel.text = self.restaurant.name
        JLThemes.apply(restaurantNameLabel)
        
        Util.setNoResultsViewMessage(self.noResultsView, message: UserMessage.NoReviewForThisRestaurant.rawValue)
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: tableView)
        
        tableView.dataSource = self
       loadRestaurantReviews()
    }
    
    @IBAction func addReviewPressed(sender: AnyObject) {
        UIFactory.createReviewEditorController(self, restaurant: self.restaurant, restaurantExperience: RestaurantExperience?.None, delegate: self)
    }
    
    
    @IBAction func backPressed(sender: AnyObject) {
        AppGlobals.appHomeTabViewController.activateGrubsList()
        Util.closeController(self)
    }
    
    
    func loadRestaurantReviews() {
        guard let restaurant = self.restaurant,
              let _ = self.tableView else {
            return
        }
        self.activityIndicator.startAnimation()
        var reviews : [RestaurantExperience]!
        ThreadingUtil.inBackground({
            reviews = RestaurantExperience.getAllReviews(restaurant.id)
            }) {
                self.reviews = reviews
                self.tableView.reloadData()
                
                self.activityIndicator.stopAnimation()
        }
    }
    
    // MARK: PhotoListDataSource
    var noPhotoMessage = UserMessage.NoReviewPhoto.rawValue
    var listTitle = ""
    
    var getPhotosDelegate : (() -> [PhotoSource])?
    func getPhotos() -> [PhotoSource] {
        if let getPhotosDelegate = self.getPhotosDelegate {
            return getPhotosDelegate()
        }
        return [PhotoSource]()
    }
    
    var getOnNewObjectContextMethod : (() -> AnyObject?)?
    func getOnNewObjectContext() -> AnyObject? {
        if let getOnNewObjectContextMethod = self.getOnNewObjectContextMethod {
            return getOnNewObjectContextMethod()
        }
        return nil
    }
    
    func getPhotoEditorDelegate() -> PhotoEditorDelegate? {
        return RestaurantExperiencePhotoEditorDelegate()
    }

}

// MARK: prepareForSegue
extension RestaurantReviewsListViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let reviewPhotosVc = segue.destinationViewController as? PhotoListViewController {
            if let review =  sender as? RestaurantExperience {
                let date = Factory.createDateFormatter().stringFromDate(review.experienceDate)
                let rating = RateScale(rawValue: review.rating)!
                self.listTitle = "\(self.restaurant.name)\n\(date) - \(rating.shortDescription)"
                self.getPhotosDelegate = {
                    return ExperiencePhoto.getReviewPhotos(review.id)
                }
                self.getOnNewObjectContextMethod = {
                    return review
                }
            }

            reviewPhotosVc.photoSourceDelegate = self
        }
        else if let newReviewPhoto = segue.destinationViewController as? PhotoEditorViewController {
            newReviewPhoto.delegate = self
        }
    }
}


// MARK: AppModelEditorDelegate
extension RestaurantReviewsListViewController : AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {
        self.loadRestaurantReviews()
    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {
        self.loadRestaurantReviews()
    }
}


// MARK: ReviewCellDelegate
extension RestaurantReviewsListViewController : ReviewCellDelegate {
    func didPressPhotosButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)?) {
        self.performSegueWithIdentifier("segueToReviewPhotos", sender: review)
    }
    
    
    func didPressDeleteButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)?) {
        let reviewDate = self.dateFormatter.stringFromDate(review.experienceDate)
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to delete this review dated \(reviewDate)? All photos associated with this review will be deleted as well.", preferredStyle: .ActionSheet)
        let cancelAction = UIAlertAction(title: "Nope, I changed my mind", style: .Cancel, handler: nil)
        actionSheetController.addAction(cancelAction)
        
        let deleteReviewAction = UIAlertAction(title: "Yup, go ahead and delete", style: .Destructive) { action -> Void in
              RestaurantExperience.remove(review.id, completionBlock: { (success, error) -> Void in
                if error == nil {
                    Util.showMessage(self, msg: "Review dated \(reviewDate) was successfully deleted", title: "", completionBlock: {(alertAction) in
                        self.loadRestaurantReviews()
                    })
                    EventPublisher.sharedAppEventPublisher().emit(.RestaurantExperienceDeleted, userData: review.id)
                }
                else {
                    Util.showMessage(self, msg: "Error deleting review. Please try again later.", title: UserDialogTitle.Error.rawValue, completionBlock: nil)
                }
              })

        }
        actionSheetController.addAction(deleteReviewAction)
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    
    }
    
    func didPressPostToFBButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)?) {
        var photos = [ExperiencePhoto]()
        self.activityIndicator.startAnimation()
        
        var restaurantName = ""
        var comments = ""
        ThreadingUtil.inBackground({
            if let restaurant = Restaurant.getById(review.restaurantId) {
                restaurantName = restaurant.name
            }

            // TODO: Rethink this behavior of automatically including the first review's photo; might not be a good idea.
            photos = ExperiencePhoto.getReviewPhotos(review.id)
            }) {
                self.activityIndicator.stopAnimation()
                var post : SocialPost!
                if photos.count > 0 {
                    photos[0].getPhotoInBackground({ (image, error) -> Void in
                        let photoData = UIImageJPEGRepresentation(image!, 1.0)!
                        if review.comments.isEmpty{
                            comments = restaurantName
                        }
                        else {
                            comments = "\(review.comments). \(restaurantName)"
                        }
                        post = SocialPost(refId: review.id, objectType: RestaurantExperience.dataSourceEntityName, photo:  UIImage(data: photoData), caption: comments)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            UIFactory.showStoryboard(self, storyboardId: .PostToSocial, configureBlock: { (uiViewController) -> Void in
                                if let viewController = uiViewController as? PostToSocialViewController {
                                    viewController.post = post
                                }
                            })
                        })
                    })
//                    let photoData = UIImageJPEGRepresentation(photos[0].photo, 1.0)!
//                    post = SocialPost(photo:  UIImage(data: photoData), caption: review.comments)
                } else {
                    if review.comments.isEmpty{
                        comments = restaurantName
                    }
                    else {
                        comments = "\(review.comments). \(restaurantName)"
                    }
                    post = SocialPost(refId: review.id, objectType: RestaurantExperience.dataSourceEntityName, photo: nil, caption: comments)
                    UIFactory.showStoryboard(self, storyboardId: .PostToSocial, configureBlock: { (uiViewController) -> Void in
                        if let viewController = uiViewController as? PostToSocialViewController {
                            viewController.post = post
                        }
                    })
                }
      
        }
    }
    
    func didPressEditButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)?) {
        UIFactory.createReviewEditorController(self, restaurant: self.restaurant, restaurantExperience: review, delegate: self)
    }
}


// MARK: UITableViewDataSource
extension RestaurantReviewsListViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let review = reviews[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")! as! ReviewCell
        cell.delegate = self
        
        cell.restaurantExperience = review
        
        cell.dateLabel.text = self.dateFormatter.stringFromDate(review.experienceDate)
        cell.ratingLabel.text = RateScale(rawValue: review.rating)!.shortDescription
        cell.commentsLabel.text = review.comments
        JLThemes.apply(cell.commentsLabel!)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviews.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
}