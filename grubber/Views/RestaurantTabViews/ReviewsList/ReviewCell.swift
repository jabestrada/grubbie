//
//  ReviewCell.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


protocol ReviewCellDelegate {
    func didPressPhotosButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)?) -> Void
    func didPressPostToFBButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)? ) -> Void
    func didPressEditButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)? ) -> Void
    func didPressDeleteButton(review: RestaurantExperience, completionCallback: ((result: [AnyObject]?) -> Void)? ) -> Void
}


class ReviewCell: UITableViewCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var commentsLabel: UITextView!
    @IBOutlet var postToFbButton: UIButton!
    @IBOutlet var viewPhotos: UIButton!
    
    var delegate : ReviewCellDelegate?
    var restaurantExperience : RestaurantExperience!
    
    @IBAction func photosButtonPressed(sender: AnyObject) {
        guard let delegate = self.delegate, let restaurantExperience = self.restaurantExperience else {
            return
        }
        delegate.didPressPhotosButton(restaurantExperience, completionCallback: nil)
    }
    
    @IBAction func editReviewPressed(sender: AnyObject) {
        guard let delegate = self.delegate, let restaurantExperience = self.restaurantExperience  else {
            return
        }
        delegate.didPressEditButton(restaurantExperience, completionCallback: nil)
    }
    
    @IBAction func deleteButtonPressed(sender: AnyObject) {
        guard let delegate = self.delegate, let restaurantExperience = self.restaurantExperience  else {
            return
        }
        delegate.didPressDeleteButton(restaurantExperience, completionCallback: nil)
    }
    
    
    @IBAction func postToFBPressed(sender: AnyObject) {
        guard let delegate = self.delegate, let restaurantExperience = self.restaurantExperience  else {
            return
        }

        delegate.didPressPostToFBButton(restaurantExperience, completionCallback: nil)
    }
    
}
