//
//  RestaurantTabBarViewProtocol.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

protocol RestaurantTabBarViewProtocol {
    var restaurant : Restaurant!	 { get set}
    var location : CLLocation! { get set}
    var appModelEventsDelegate : AppModelEditorDelegate? {get set}
    func onReady()
}
