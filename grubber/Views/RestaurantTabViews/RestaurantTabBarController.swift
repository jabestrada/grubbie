//
//  RestaurantTabBarController.swift
//  myEats
//
//  Created by JABE on 11/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

//public enum EventType {
//
//}
//public class OriginatingEvent {
//
//}

class RestaurantTabBarController: UITabBarController {
    var restaurant : Restaurant!
    var locator = JLLocator()
    
    override func viewDidLoad() {
        locator.getCurrentLocation { (location) -> Void in
            if let restaurant = self.restaurant {
                for controller in self.viewControllers! {
                    if var restaurantTab = controller as? RestaurantTabBarViewProtocol {
                        restaurantTab.restaurant = restaurant
                        restaurantTab.location = location
                        restaurantTab.appModelEventsDelegate = self
                        restaurantTab.onReady()
                    }
                }
            }
        }
    }
}

extension RestaurantTabBarController : AppModelEditorDelegate {
    func getReviewListController() -> RestaurantReviewsListViewController? {
        for controller in self.viewControllers! {
            if let vc = controller as? RestaurantReviewsListViewController {
                return vc
            }
        }
        return RestaurantReviewsListViewController?.None
    }
    
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {
        if let _  = instance as? RestaurantExperience {
            if let reviewListVc = getReviewListController() {
                self.selectedViewController = reviewListVc
                reviewListVc.loadRestaurantReviews()
            }
        }
    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {
        
    }
}
