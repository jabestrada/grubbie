//
//  RestaurantHomeViewController.swift
//  myEats
//
//  Created by JABE on 11/6/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


import MapKit

extension RestaurantHomeViewController : AppEventSubscriber {
    func onEvent(eventType: AppEventType, eventTimeStamp: NSDate, userData: AnyObject?) {
        switch eventType {
        case .RestaurantModified:
            if let restaurantId = userData as? String {
                if restaurant.id == restaurantId {
                    self.restaurant = Restaurant.getById(restaurantId)
                    self.updateMainRestaurantInfo()
                }
            }
            break;
        case .RestaurantPhotoAdded, .ExperiencePhotoAdded:
            guard let _ = self.profPicSource else {
                self.updateProfPic()
                return
            }
            break;
        case .RestaurantPhotoDeleted, .RestaurantPhotoModified, .ExperiencePhotoDeleted, .ExperiencePhotoModified:
            guard let photoId = userData as? String,
                    currentProfPic = self.profPicSource else {
                return
            }
            if photoId == currentProfPic.getId() {
                self.updateProfPic()
            }
            break;
        case .RestaurantExperienceAdded:
            self.updateLatestReview()
            break;
        case .RestaurantExperienceDeleted, .RestaurantExperienceModified:
            guard let reviewId = userData as? String,
                  latestReview = self.latestReview else {
                return
            }
            if reviewId == latestReview.id {
                self.updateLatestReview()
                if eventType == .RestaurantExperienceDeleted {
                    self.updateProfPic()
                }
            }
            break;
        default:
            print("RestaurantHomeViewController.onEvent: Didn't handle event \(eventType)")
            break;
        }

    }
}



class RestaurantHomeViewController: UIViewController, RestaurantTabBarViewProtocol, PhotoListDataSource {

    @IBOutlet var restaurantNameTextField: UILabel!
    @IBOutlet var lastVisitLabel: UILabel!
    @IBOutlet var distanceFromLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var imageView: UIImageView!

    @IBOutlet var noPhotoView: UIView!
    @IBOutlet var commentsTextView: UITextView!
    @IBOutlet var addressLabel: UILabel!

    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var photoViewCaption: UILabel!
    
    @IBOutlet var callButton: UIButton!
    @IBOutlet var withReviewContainer: UIView!
    @IBOutlet var withoutReviewContainer: UIView!
    
    var appModelEventsDelegate : AppModelEditorDelegate?
    var activityIndicator : JLActivityIndicator!
    var profPicSource : PhotoSource?
    var latestReview : RestaurantExperience?
    var restaurant : Restaurant!
    
    var location : CLLocation! {
        didSet {
            updateLocationInfo()
        }
    }
    @IBAction func callButtonPressed(sender: AnyObject) {
        if let number = phoneNumberLabel?.text {
            Util.dial(self, rawNumber: number)
        }
    }
    
    @IBAction func photosButtonPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("segueToPhotos", sender: nil)
    }

    
    func onReady() {
        updateRestaurantInfo()
    }
    

    override func viewDidLoad() {
        EventPublisher.sharedAppEventPublisher().addSubscriber(self, eventTypes: .RestaurantModified, .RestaurantPhotoDeleted,
                                                                                .RestaurantPhotoModified, .RestaurantPhotoAdded,
                                                                                .ExperiencePhotoModified, .ExperiencePhotoAdded,
                                                                                .RestaurantExperienceAdded, .RestaurantExperienceModified, .RestaurantExperienceDeleted)
        
        JLThemes.apply(restaurantNameTextField, commentsTextView, noPhotoView, imageView)

        Util.setNoResultsViewMessage(withoutReviewContainer, message: UserMessage.NoReviewForThisRestaurant.rawValue)
        self.activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        imageView.addGestureRecognizer(gestureRecognizer)
    }
    
    func imageTapped(img: AnyObject){
        if let image = imageView.image {
            UIFactory.showStoryboard(self, storyboardId: StoryboardId.FullPhotoViewer, configureBlock: { (uiViewController) -> Void in
                if let view = uiViewController as? FullScreenPhotoViewController {
                    view.image = image
                }
            })
        }
    }
    
    func updateMainRestaurantInfo(){
        guard let theRestaurant = self.restaurant else {
            return
        }
        self.activityIndicator.startAnimation()
        self.listTitle = "\(theRestaurant.name) Gallery"
        restaurantNameTextField?.text = theRestaurant.name
        addressLabel?.text = "Address: \(self.restaurant.address)"
        if self.restaurant.phone.isEmpty {
            callButton?.hidden = true
            phoneNumberLabel?.hidden = true
        } else {
            callButton?.hidden = false
            phoneNumberLabel?.text = self.restaurant.phone
            phoneNumberLabel?.hidden = false
        }
        self.activityIndicator.stopAnimation()
    }
    
    func updateProfPic(){
            print("updateProfPic called")
            self.photoViewCaption?.text = "Retrieving..."
            Restaurant.getProfilePic(self.restaurant.id, completion: { (photoSource, error) -> Void in
                if error == nil {
                    if let photoSource = photoSource {
                        self.profPicSource = photoSource
                        self.imageView?.hidden = false
                        self.imageView?.image = photoSource.getPhoto()
                    }
                    else {
                        self.profPicSource = nil
                        self.imageView?.hidden = true
                        self.photoViewCaption?.text = "No Photo"
                    }
                } else {
                    print("updateProfPic: \(error)")
                    self.profPicSource = nil
                    self.imageView?.hidden = true
                    self.photoViewCaption?.text = "No Photo"
                }
            })
        
    }
    
    
    func updateLatestReview() {
        guard let theRestaurant = self.restaurant else {
            return
        }
        self.activityIndicator?.startAnimation()
        ThreadingUtil.inBackground({
            self.latestReview = RestaurantExperience.getLatestReviewOfRestaurant(theRestaurant.id)
            }) {
                if let latestReview = self.latestReview {
                    self.withReviewContainer?.hidden = false
                    self.withoutReviewContainer?.hidden = true
                    
                    self.lastVisitLabel?.text = "Last visited on \(Factory.createDateFormatter().stringFromDate(latestReview.experienceDate))"
                    
                    if let rating = RateScale(rawValue: latestReview.rating) {
                        self.ratingLabel?.text = "Rating: \(rating.shortDescription)"
                    }
                    self.commentsTextView?.text = latestReview.comments.isEmpty ? "No comments given" : latestReview.comments
                }
                else {
                    self.withReviewContainer?.hidden = true
                    self.withoutReviewContainer?.hidden = false
                }
                self.activityIndicator?.stopAnimation()
        }
    }
    
    func updateRestaurantInfo() {
        updateProfPic()
        updateMainRestaurantInfo()
        updateLatestReview()
    }
    
    func updateLocationInfo() {
        guard let _ = self.location,
            let _ = self.distanceFromLabel else {
            return
        }
        let restoLocation = CLLocation(latitude: self.restaurant.lat, longitude: self.restaurant.lng)
        
        let distanceFromText = restoLocation.textForDistanceFrom(self.location)
        self.distanceFromLabel.text = "Distance: \(distanceFromText)"
    }
    
    @IBAction func addReviewPressed(sender: AnyObject) {
        UIFactory.createRestaurantEditorController(self, restaurant: self.restaurant, delegate: self)
    }
    
    @IBAction func backToMyEatsPressed(sender: AnyObject) {
        AppGlobals.appHomeTabViewController.activateGrubsList()
        Util.closeController(self)
    }
    
    // MARK: prepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let reviewPhotosVc = segue.destinationViewController as? PhotoListViewController {
            reviewPhotosVc.photoSourceDelegate = self
        }
    }
    // MARK: PhotoListDataSource
    var noPhotoMessage = UserMessage.NoGalleryPhotosForThisRestaurant.rawValue
    var listTitle = ""
    
    func getPhotos() -> [PhotoSource] {
        return RestaurantPhoto.getPhotosOfRestaurant(self.restaurant.id)
    }
    
    func getOnNewObjectContext() -> AnyObject? {
        return self.restaurant
    }
    
    func getPhotoEditorDelegate() -> PhotoEditorDelegate? {
        return RestaurantPhotoEditorDelegate()
    }
    
}


// MARK: AppModelEditorDelegate
extension RestaurantHomeViewController : AppModelEditorDelegate {
    func didAddInstance<T>(sourceViewController: UIViewController, instance: T) {
        if let delegate = self.appModelEventsDelegate {
            if let _  = instance as? RestaurantExperience {
               delegate.didAddInstance(self, instance: instance)
            }
        }
    }
    
    func didUpdateInstance<T>(sourceViewController: UIViewController, instance: T) {
        // Do nothing.
    }
}
