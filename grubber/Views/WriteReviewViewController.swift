//
//  WriteReviewViewController.swift
//  myEats
//
//  Created by JABE on 11/5/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

// MARK: RatingControllerDelegate
extension WriteReviewViewController : RatingControllerDelegate {
    func onRatingChanged(newValue: Double, oldValue: Double?) {
        onRatingChanged()
    }
}


class WriteReviewViewController: UIViewController {
    
    @IBOutlet var rate1: UIButton!
    @IBOutlet var rate2: UIButton!
    @IBOutlet var rate3: UIButton!
    @IBOutlet var rate4: UIButton!
    @IBOutlet var rate5: UIButton!
    
    
    @IBOutlet var restaurantNameTextField: UILabel!
    @IBOutlet var restaurantsCommentTextField: UITextView!
    @IBOutlet var ratingDescriptionTextField: UILabel!
    
    @IBOutlet var ratingShortDescription: UILabel!
    @IBOutlet var experienceDatePicker: UIDatePicker!
    
    let defaultRating = RateScale.ThreeStars
    
    var delegate : AppModelEditorDelegate?
    var restaurant : Restaurant!
    var restaurantExperience : RestaurantExperience!
    var activityIndicator : JLActivityIndicator!
    var validators = [JLControlValidator]()
    var ratingController : RatingController!
    
    @IBAction func ratingPressed(sender: AnyObject) {
        guard let ratingButton = sender as? UIButton else {
            return
        }

        self.ratingController.setValue(Double(ratingButton.tag))
    }

    
    override func viewDidLoad() {
        let ratingButtons = [RatingButton(button: rate1, ratingWeight: Double(rate1.tag)),
                            RatingButton(button: rate2, ratingWeight: Double(rate2.tag)),
                            RatingButton(button: rate3, ratingWeight: Double(rate3.tag)),
                            RatingButton(button: rate4, ratingWeight: Double(rate4.tag)),
                            RatingButton(button: rate5, ratingWeight: Double(rate5.tag))]
        for button in ratingButtons {
            button.onImage = UIImage(named: "StarFilled")
            button.offImage = UIImage(named: "Star")
        }
        self.ratingController = RatingController(ratingWidgets: ratingButtons)
        self.ratingController.delegate = self
        
        validators.append(DateCompareValidator(controlToValidate: self.experienceDatePicker, errorMessage: "Review date cannot be later than today", compareRules: [.OrderedSame, .OrderedAscending], compareToCurrentDate: true))
        self.hideKeyboardOnTap()
        JLThemes.apply(restaurantNameTextField, restaurantsCommentTextField)
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: self.view, startAnimating: false)

        if let restaurantExperience = self.restaurantExperience {
            experienceDatePicker.date = restaurantExperience.experienceDate
            self.ratingController.setValue(Double(restaurantExperience.rating))
            restaurantsCommentTextField.text = restaurantExperience.comments
        } else {
            experienceDatePicker.date = NSDate()
            self.ratingController.setValue(Double(defaultRating.rawValue))
            restaurantsCommentTextField.text = ""
        }
        
        
        if let restaurant = self.restaurant {
            restaurantNameTextField.text = restaurant.name
        }
        
        self.onRatingChanged()
    }

    
    func onRatingChanged(){
      let rating = RateScale(rawValue: Int(self.ratingController.getValue()!))
        ratingDescriptionTextField.text = rating?.longDescription
        ratingShortDescription.text = rating?.shortDescription
        
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func savePressed(sender: AnyObject) {
        if let delegate = self.delegate {
            if !self.isValid(){
                return
            }
            self.activityIndicator.startAnimation()
            if self.restaurantExperience == nil || self.restaurantExperience.isNew() {
                var newRestaurantExperience : RestaurantExperience!
                ThreadingUtil.inBackground({
                    newRestaurantExperience = RestaurantExperience.insert({(restaurantExperience : RestaurantExperience) in
                        self.gatherRestaurantExperience(restaurantExperience)
                    })

                    }, callBack: {
                        self.activityIndicator.stopAnimation()
                       Util.closeController(self)
                        if let _ = newRestaurantExperience {
                            delegate.didAddInstance(self, instance: newRestaurantExperience)
                        }
                        if let newExperience = newRestaurantExperience {
                            EventPublisher.sharedAppEventPublisher().emit(.RestaurantExperienceAdded, userData: newExperience.id)
                        }
                })


            }
            else {
                var updated = -1
                ThreadingUtil.inBackground({ () -> Void in
                        updated = RestaurantExperience.update(self.restaurantExperience.id,
                            updateValuesAssignmentBlock: { (restaurantExperience: RestaurantExperience) -> Void in
                                self.gatherRestaurantExperience(restaurantExperience)
                    })
                    }, callBack: {
                        self.activityIndicator.stopAnimation()
                        if updated > 0 {
                            Util.showMessage(self, msg: "Review was successfully updated", title: UserDialogTitle.Success.rawValue, completionBlock: { (alertAction) -> Void in
                                EventPublisher.sharedAppEventPublisher().emit(.RestaurantExperienceModified, userData: self.restaurantExperience.id)
                                delegate.didUpdateInstance(self, instance: self.restaurant)
                                Util.closeController(self)
                            })
                        } else {
                            Util.showMessage(self, msg: "Failed to update review. Please try again later.", title: UserDialogTitle.Error.rawValue, completionBlock: nil)
                        }
                })

            }
            
         
        }
    }
    
    func gatherRestaurantExperience(restaurantExperience: RestaurantExperience){
        restaurantExperience.comments = self.restaurantsCommentTextField.text
        restaurantExperience.experienceDate = self.experienceDatePicker.date
        restaurantExperience.restaurantId = self.restaurant.id
        restaurantExperience.rating = Int(self.ratingController.getValue()!)
    }
    
    
    func isValid() -> Bool {
        for validator in self.validators {
            if !validator.isValid() {
                Util.showMessage(self, msg: validator.errorMessage, title: UserDialogTitle.Info.rawValue)
                return false
            }
        }
        return true

    }
    
}
