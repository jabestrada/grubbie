//
//  AudioRecording.swift
//  grubber
//
//  Created by JABE on 11/21/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import AVFoundation

protocol AudioRecordingDelegate {
    func doneRecording(audioData: NSData?)
}


class AudioRecordingViewController: UIViewController {
    
    @IBOutlet var recordButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var recordInstructionLabel: UILabel!
    
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var delegate : AudioRecordingDelegate?
    
    var audioData : NSData? {
        didSet{
            onAudioDataChanged()
        }
    }
    
    var isRecording = false {
        didSet {
            playButton.enabled = !isRecording
            recordInstructionLabel.text = isRecording ? UserMessage.StopRecordingInstruction.rawValue : UserMessage.StartRecordingInstruction.rawValue
            recordButton.setImage(UIImage(named: isRecording ? MediaName.StopRecordingIcon.rawValue : MediaName.StartRecordingIcon.rawValue), forState: .Normal)
        }
    }
    
    var isPlaying = false {
        didSet {
            recordInstructionLabel.text = isPlaying ? UserMessage.PlayingRecording.rawValue : UserMessage.StartRecordingInstruction.rawValue
            recordButton.enabled = !isPlaying
            playButton.setImage(UIImage(named: isPlaying ?  MediaName.StopRecordingIcon.rawValue : MediaName.PlayRecordingIcon.rawValue ), forState: .Normal)
        }
    }

    func onAudioDataChanged(){
        playButton?.enabled = self.audioData != NSData?.None
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordInstructionLabel.text = UserMessage.StartRecordingInstruction.rawValue
        let soundFileUrl = getSoundFileUrl()
        let recordSettings = [AVEncoderAudioQualityKey: AVAudioQuality.Min.rawValue,
            AVEncoderBitRateKey: 16,
            AVNumberOfChannelsKey: 1,
            AVSampleRateKey: 5512.5]
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        }
        catch let err as NSError {
            print("audioSession error: \(err.localizedDescription)")
        }
        
        do {
            try audioRecorder = AVAudioRecorder(URL: soundFileUrl, settings: recordSettings as! [String : AnyObject])
            audioRecorder?.prepareToRecord()
        }
        catch let err as NSError {
            print("audioRecorder error: \(err.localizedDescription)")
        }
        onAudioDataChanged()
    }
    
    func getSoundFileUrl() -> NSURL {
        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let pathComponents = [dirPaths[0], MediaName.RecordingTempFileName.rawValue]
        return NSURL.fileURLWithPathComponents(pathComponents)!
    }
    


    @IBAction func doneRecordingPressed(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.doneRecording(self.audioData)
        }
        Util.closeController(self)
    }

    @IBAction func cancelButtonPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func playButtonPressed(sender: AnyObject) {
        guard let audioData = self.audioData else {
            return
        }
        
        if isPlaying {
            audioPlayer?.stop()
            isPlaying = false
        } else {
            do {
                do {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker)
                } catch let e as NSError {
                    print("Error #85: \(e.localizedDescription)")
                }

                try audioPlayer = AVAudioPlayer(data: audioData)
                audioPlayer?.delegate = self
                audioPlayer?.play()
                isPlaying = true                
            }
            catch let e as NSError {
                print("Play error: \(e.localizedDescription)")
            }
        }
    }
    
    
    @IBAction func recordButtonPressed(sender: AnyObject) {
        if isRecording {
            isRecording = false
            self.audioRecorder?.stop()
            self.audioData = NSData(contentsOfURL: getSoundFileUrl())
        }
        else {
            self.audioRecorder?.record()
            isRecording = true
        }
    }
    
}

extension AudioRecordingViewController : AVAudioPlayerDelegate  {
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        isPlaying = false
    }
}
