//
//  RestaurantMapViewController.swift
//  myEats
//
//  Created by JABE on 11/9/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import MapKit

class RestaurantMapViewController: UIViewController, RestaurantTabBarViewProtocol {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var restaurantNameLabel: UILabel!
    
    var appModelEventsDelegate : AppModelEditorDelegate?
    
    var restaurant : Restaurant! {
        didSet {
            redrawMap()
        }
    }
    var location : CLLocation! {
        didSet {
            redrawMap()
        }
    }

    var activityIndicator : JLActivityIndicator!
    
    override func viewDidLoad() {
        JLThemes.apply(restaurantNameLabel)
        
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: mapView)
        redrawMap()
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        Util.closeController(self)
    }

    func onReady() {
        // TODO: ....
    }
    func redrawMap() {
        guard let _ = self.restaurant,
                  _ = self.location,
                  _ = self.mapView else {
                    return
        }
        self.activityIndicator.startAnimation()
        
        let restoLocation = CLLocation(latitude: self.restaurant.lat, longitude: self.restaurant.lng)
        let subTitleText = "\(self.restaurant.address) (\(restoLocation.textForDistanceFrom(location)))"
        restaurantNameLabel.text = "\(self.restaurant.name)\n\(subTitleText)"
        
        let restaurantCoordinates = CLLocationCoordinate2DMake(self.restaurant.lat, self.restaurant.lng)
        let restaurantAnnotation = MKPointAnnotation.make(restaurantCoordinates, title: self.restaurant.name, subTitle: subTitleText)
        self.mapView.addAnnotation(restaurantAnnotation)
        
        AppGlobals.sharedPlacemark { (placemarks, error) -> Void in
            let geoData = JLGeoData(placemarks: placemarks)
            let youAreHereAnnotation = MKPointAnnotation.make(self.location.coordinate, title: UserMessage.YourCurrentLocation.rawValue, subTitle: geoData.street)

            self.mapView.addAnnotation(restaurantAnnotation)
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(self.location.coordinate, 500, 500)
            let adjustedRegion = self.mapView.regionThatFits(viewRegion)
            self.mapView.setRegion(adjustedRegion, animated: true)
            self.mapView.showsUserLocation = true
            
            self.mapView.showAnnotations([restaurantAnnotation, youAreHereAnnotation], animated: true)
            self.activityIndicator.stopAnimation()
        }

    }
}





