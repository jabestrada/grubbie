//
//  GeoLocationEditor.swift
//  Grubbie
//
//  Created by JABE on 11/26/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


import MapKit
import AssetsLibrary


class GeoLocationEditor: UIViewController {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var latTextField: UITextField!
    @IBOutlet var lngTextField: UITextField!
    
    var location : CLLocation?
    var currentLocation : CLLocation?
    var delegate : AppModelEditorDelegate?
    var activityIndicator : JLActivityIndicator!
    var locator : JLLocator!
    var geoData : JLGeoData?
    
    override func viewDidLoad() {
        locator = JLLocator()
        
        self.hideKeyboardOnTap()
        activityIndicator = JLActivityIndicator(parentView: self.view, onTopOfView: mapView)
        if let location = self.location {
            bindCurrentLocation(location)
        }
    }
    
    func bindCurrentLocation(newLocation : CLLocation) {
        self.currentLocation = newLocation
        latTextField.text = "\(newLocation.coordinate.latitude)"
        lngTextField.text = "\(newLocation.coordinate.longitude)"
        drawMap()
    }
    
    func drawMap(){
        guard let location = self.currentLocation,
            mapView = self.mapView else {
                return
        }
        mapView.removeAnnotations(mapView.annotations)
        locator.getGeoDataFromLocation(location) { (geoData, error) -> Void in
            if let geoData = geoData {
                self.geoData = geoData
                let viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
                let adjustedRegion = mapView.regionThatFits(viewRegion)
                mapView.setRegion(adjustedRegion, animated: true)
                
                var annotationTitle = ""
                if let _ = geoData.street {
                    annotationTitle = geoData.street!
                }
                let restaurantAnnotation = MKPointAnnotation.make(location.coordinate, title: annotationTitle, subTitle: String?.None)
                mapView.showAnnotations([restaurantAnnotation], animated: true)
            }
             else {
                ThreadingUtil.inMainQueue({ () -> Void in
                    Util.showMessage(self, msg: UserMessage.NoPlacemarksError.rawValue, title: "", completionBlock: { (alertAction) -> Void in
                        Util.closeController(self)
                    })
                })
            }
            self.activityIndicator.stopAnimation()
        }
    }
    
    @IBAction func donePressed(sender: AnyObject) {
        if let delegate = self.delegate {
            if let _ = self.geoData {
                delegate.didUpdateInstance(self, instance: self.geoData!)
            } else {
                // JABE, Dec. 17, 2015: For some reason, passing the unwrapped variable causes
                // the cast "self.geoData as? JLGeoData" to fail in the delegate's callback.
                delegate.didUpdateInstance(self, instance: self.geoData)
            }
        }
        Util.closeController(self)
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        Util.closeController(self)
    }
    
    @IBAction func getCoordsFromPhotoPressed(sender: AnyObject) {
        let cameraViewController = UIImagePickerController()
        cameraViewController.delegate = self
        cameraViewController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(cameraViewController, animated: true, completion: nil)
        
    }
}

extension GeoLocationEditor : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if picker.sourceType == UIImagePickerControllerSourceType.PhotoLibrary {
            guard let url = info[UIImagePickerControllerReferenceURL] as? NSURL
                else {
                    self.cannotGetGPSDataFromPhoto()
                    return
            }
            
            let library = ALAssetsLibrary()
            library.assetForURL(url, resultBlock: {
                (asset: ALAsset!) in
                    if asset.valueForProperty(ALAssetPropertyLocation) != nil {
                        if let location = asset.valueForProperty(ALAssetPropertyLocation) as? CLLocation {
                           self.bindCurrentLocation(CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
                            Util.showMessage(self, msg: "GPS coordinates from photo was successfully loaded to the map", title: UserDialogTitle.Info.rawValue)
                        }
                    }
                    else {
                        self.cannotGetGPSDataFromPhoto()
                    }
                }, failureBlock: {
                    (error: NSError!) in
                    self.cannotGetGPSDataFromPhoto()
            })
        } else {
            self.cannotGetGPSDataFromPhoto()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cannotGetGPSDataFromPhoto(){
        Util.showMessage(self, msg: "Sorry, failed to extract GPS data from selected photo.", title: UserDialogTitle.Info.rawValue)
    }
}
