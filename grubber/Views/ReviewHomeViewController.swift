//
//  ReviewHomeViewController.swift
//  myEats
//
//  Created by JABE on 11/3/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//


import Foundation
import UIKit
import CoreLocation
import MapKit


typealias JSONParameters = [String: AnyObject]


enum ReviewHomeViewControllerSegue : String {
    case toRestaurantBaseDetail = "segueToRestaurantBaseDetail"
    case toWriteReview = "segueToWriteReview"
    case toRestaurantHome = "segueToRestaurantHome"
}


extension ReviewHomeViewController : RestaurantListViewControllerDelegate {
    func didSelectRestaurant(restaurant: Restaurant, userData: AnyObject?) {
        self.doSegueForRestaurant(restaurant)
//        ThreadingUtil.inBackgroundAfter({ () -> Void in
//            self.doSegueForRestaurant(restaurant)
//            }, afterDelay: 0.5)
    }
}

struct ReviewHomeCommand{
    var text : String
    var invokeCommand : (ReviewHomeViewController) -> Void
}


class ReviewHomeViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!

    
    var currentLocation : CLLocation?
    var appModelDelegate : AppModelEditorDelegate?
    var segueTo : String?
    var segueToParam : AnyObject?
    
    var theView = self
    
    var commands = [ReviewHomeCommand(text: UserMessage.ManualRestaurantEntry.rawValue, invokeCommand: {(reviewHomeViewController: ReviewHomeViewController) -> Void in
                        reviewHomeViewController.performSegueWithIdentifier(ReviewHomeViewControllerSegue.toRestaurantBaseDetail.rawValue, sender: nil)
                    }),
                    ReviewHomeCommand(text: UserMessage.SelectNearbyRestaurants.rawValue, invokeCommand: {(reviewHomeViewController: ReviewHomeViewController) -> Void in
                        UIFactory.showStoryboard(reviewHomeViewController, storyboardId: StoryboardId.RestaurantListView, configureBlock: { (uiViewController) -> Void in
                            if let restoListVc = uiViewController as? RestaurantListViewController {
                                restoListVc.delegate = reviewHomeViewController
                                restoListVc.restaurantListProvider =  NearbyRestaurantListProvider()
                            }
                        })
                    }),
                    ReviewHomeCommand(text: UserMessage.SelectFromGrubsList.rawValue, invokeCommand: {(reviewHomeViewController: ReviewHomeViewController) -> Void in
                        UIFactory.showStoryboard(reviewHomeViewController, storyboardId: StoryboardId.RestaurantListView, configureBlock: { (uiViewController) -> Void in
                            if let restoListVc = uiViewController as? RestaurantListViewController {
                                restoListVc.delegate = reviewHomeViewController
                                restoListVc.restaurantListProvider =  RestaurantsOfUserListProvider()
                            }
                        })
                    })]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        JLThemes.apply(self.view)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let segue = self.segueTo {
            if !segue.isEmpty {
                self.performSegueWithIdentifier(segue, sender: self.segueToParam)
                self.segueTo = String?.None
                self.segueToParam = nil
            }
        }
    }

}




// MARK: prepareForSegue
extension ReviewHomeViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let restoMainEditorVc = segue.destinationViewController as? RestaurantMainDetailEditorController {
            restoMainEditorVc.delegate = self
            if let restaurant = sender as? Restaurant {
                restoMainEditorVc.restaurant = restaurant
            }
        } else if let writeReviewVc = segue.destinationViewController as? WriteReviewViewController {
            writeReviewVc.delegate = self
            if let restaurant = sender as? Restaurant {
                writeReviewVc.restaurant = restaurant
            }
            
        } else if let restaurantHomeVc = segue.destinationViewController as? RestaurantTabBarController {
            if let restaurantExperience = sender as? RestaurantExperience {
                if let restaurant = Restaurant.getById(restaurantExperience.restaurantId) {
                    restaurantHomeVc.restaurant = restaurant
                }
            }
        }
    }
}



// MARK: AppModelEditorDelegate
extension ReviewHomeViewController : AppModelEditorDelegate {
    
    func didAddInstance<Restaurant>(sourceViewController: UIViewController, instance: Restaurant) {
        if sourceViewController.isKindOfClass(RestaurantMainDetailEditorController) {
            self.segueTo = ReviewHomeViewControllerSegue.toWriteReview.rawValue
            self.segueToParam = instance as? AnyObject
        } else if sourceViewController.isKindOfClass(WriteReviewViewController) {
            self.segueTo = ReviewHomeViewControllerSegue.toRestaurantHome.rawValue
            self.segueToParam = instance as? AnyObject
        }
    }
    
    func didUpdateInstance<Restaurant>(sourceViewController: UIViewController, instance: Restaurant) {
        print("Updated: \(instance)")
    }
}


// MARK: UITableViewDelegate
extension ReviewHomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let command = self.commands[indexPath.row]
        command.invokeCommand(self)
    }
    
    func doSegueForRestaurant(restaurant: Restaurant){
        if restaurant.isNew() {
            // New restaurants need to be saved to Grubs list first before review can be made.
//            self.performSegueWithIdentifier(ReviewHomeViewControllerSegue.toRestaurantBaseDetail.rawValue, sender: restaurant as AnyObject)
            self.segueTo = ReviewHomeViewControllerSegue.toRestaurantBaseDetail.rawValue
            self.segueToParam = restaurant as? AnyObject
        }
        else {
            // Restaurant already in Grubs list, so proceed to write review.
//            self.performSegueWithIdentifier(ReviewHomeViewControllerSegue.toWriteReview.rawValue, sender: restaurant as AnyObject)
            self.segueTo = ReviewHomeViewControllerSegue.toWriteReview.rawValue
            self.segueToParam = restaurant as? AnyObject
        }
    }
}

// MARK: UITableViewDataSource
extension ReviewHomeViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        let command = commands[indexPath.row]
        cell.textLabel?.text = command.text
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commands.count
    }
}