//
//  RateScale.swift
//  myEats
//
//  Created by JABE on 11/8/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

public enum RateScale: Int {
    case OneStar = 1
    case TwoStars = 2
    case ThreeStars = 3
    case FourStars = 4
    case FiveStars = 5
    
    var shortDescription: String {
        get {
            switch self {
            case .OneStar:
                return "Awful"
            case .TwoStars:
                return "So-so"
            case .ThreeStars:
                return "Average"
            case .FourStars:
                return "Great"
            case .FiveStars:
                return "Perfect"
            }
        }
    }
    var longDescription : String {
        get {
            switch self {
            case .OneStar:
                return "Not. Returning. EVER."
            case .TwoStars:
                return "May return if no other choices are available."
            case .ThreeStars:
                return "Good enough but not great."
            case .FourStars:
                return "Nearly perfect except for minor flaws."
            case .FiveStars:
                return "Nothing more to ask for!"
            }
        }
    }
}