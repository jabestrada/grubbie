//
//  RatingButton.swift
//  Grubbie
//
//  Created by JABE on 12/4/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

class RatingButton : NSObject, RatingWidget {
    var ratingWeight : Double = 0
    var onImage : UIImage?
    var offImage: UIImage?
    var button : UIButton
    
    init(button: UIButton, ratingWeight: Double){
        self.button = button
        self.ratingWeight = ratingWeight
    }
    
    func setState(isOn: Bool) {
        self.button.setImage(isOn ? onImage : offImage, forState: .Normal)
    }
}