//
//  RatingController.swift
//  Grubbie
//
//  Created by JABE on 12/4/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit


protocol RatingControllerDelegate {
    func onRatingChanged(newValue: Double, oldValue: Double?)
}


class RatingController {
    var ratingWidgets : [RatingWidget]
    var delegate : RatingControllerDelegate?
    private var currentValue : Double?
    
    init(ratingWidgets: [RatingWidget]){
        self.ratingWidgets = ratingWidgets
    }
    
    func setValue(newValue : Double){
        let oldValue = currentValue
        currentValue = newValue
        for widget in self.ratingWidgets {
            widget.setState(newValue >= widget.ratingWeight)
        }
        if let delegate = self.delegate {
            delegate.onRatingChanged(newValue, oldValue: oldValue)
        }
    }
    
    func getValue() -> Double? {
        return currentValue
    }
    
}