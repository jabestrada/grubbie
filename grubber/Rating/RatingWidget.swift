//
//  RatingWidget.swift
//  Grubbie
//
//  Created by JABE on 12/4/15.
//  Copyright © 2015 JABELabs. All rights reserved.
//

import UIKit

@objc protocol RatingWidget {
    var ratingWeight : Double { get set }
    func setState(isOn: Bool)
}